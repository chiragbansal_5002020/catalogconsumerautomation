package com.paytm.network_sniffer;

import org.json.JSONArray;
import org.json.JSONObject;

import com.paytm.utils.FrameworkExceptionStack;

public class FrameworkJSONParser {
	
	public static String parse_add_to_cart_value(String jsonString) {
		FrameworkExceptionStack expStack = FrameworkExceptionStack.map.get(Thread.currentThread().getId());
		String result = null;
		if(jsonString.startsWith("[") && jsonString.endsWith("]"))
			jsonString = jsonString.substring(1,jsonString.length()-1);
			JSONObject obj = new JSONObject(jsonString);
			if(obj.has("actions")) {
				JSONObject actions = obj.getJSONObject("actions");
				if(actions.has("add_to_cart")) {
					result = actions.get("add_to_cart").toString();
				}else {
					expStack.add(new Exception(""
							+ "add_to_cart property not found in actions object in json string : "+jsonString));
				}
			}else {
				expStack.add(new Exception("actions object not found in json string: "+jsonString));
			}
		return result;
	}
	
	public static Object parseJsonString(String jsonString, String jsonQuery) {
		System.out.println("JSON String is : "+jsonString);
		System.out.println("JSON Query is : "+jsonQuery);
		Object result = null;
		String[] keys = jsonQuery.split("\\.");
		int counter = keys[0].isEmpty()?1:0;
		JSONObject obj = null;
		if(jsonString.startsWith("[") && jsonString.endsWith("]")) {
			
				obj = new JSONArray(jsonString).getJSONObject(Integer.parseInt(
						keys[counter].split("\\[")[1].replaceAll("]","").trim()));
				counter++;
		}else {
			obj = new JSONObject(jsonString);	
		}
		
		String key=null;
		
		for(; counter<keys.length; counter++) {
			key = keys[counter];
			
			if(key.endsWith("]")) { 
				String[] keyParts = key.split("\\[");
				int index = Integer.parseInt(keyParts[1].replaceAll("]", "").trim());
				key = keyParts[0].trim();
				if(obj.has(key)) {
					obj = obj.getJSONArray(key).getJSONObject(index);
				}else {
					System.out.println("Key -> "+key+" not found in json string !");
					break;
				}
			}else {
				if(obj.has(key)) {
					if(counter==keys.length-1) {
						result = obj.get(key);
					}else {
						obj=obj.getJSONObject(key);
					}
				}else {
					System.out.println("Key -> "+key+" not found in json string !");
				}
			}
		}
		return result;
	}
}
