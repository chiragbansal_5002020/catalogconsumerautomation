package com.paytm.reusable_methods;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

public class RequestHeaderUtils {
	
	@SuppressWarnings("finally")
	public String getRemoteAddress(String uri) {
		String remoteAddress=null;
		try {
			InetAddress address = InetAddress.getByName(new URL(uri).getHost());
			remoteAddress = address.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}finally {
			return remoteAddress;
		}
	}
	
	public static void main(String[] args) {
		String url = "https://catalog.paytm.com";
		System.out.println("Remote Address: "+new RequestHeaderUtils().getRemoteAddress(url));
	}
}
