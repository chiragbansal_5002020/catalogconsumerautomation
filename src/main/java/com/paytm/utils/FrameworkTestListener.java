package com.paytm.utils;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


import com.aventstack.extentreports.Status;
import com.paytm.reusable_methods.generic_methods;
import com.paytm.utils.Reporter;

public class FrameworkTestListener implements ITestListener {

	private Reporter fetchReporterFromTestInstance(ITestResult result) {
		return Reporter.reportsMap.get(result.getInstance().getClass());
	}
	
	@Override
	public synchronized void onTestStart(ITestResult result) {
		if(DriverManager.getWebDriver(Thread.currentThread().getId()) == null) {
			new DriverManager().setWebDriverFromGlobalWebDriverSettings();
		}else {
			new DriverManager().restartBrowser();
		}
		new FrameworkExceptionStack(Thread.currentThread().getId());
		String tcName = result.getName();
		Object[] params = result.getParameters();
		if(params.length>0) {
			if(params[0] instanceof String)
				tcName += "-"+params[0].toString();
		}
		fetchReporterFromTestInstance(result).createTest(tcName);
//		if(DriverManager.getWebDriver(Thread.currentThread().getId()) == null) {
//			new DriverManager().setWebDriverFromGlobalWebDriverSettings();
//		}
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("Test is passed. Calling finish on reporter");
		FrameworkExceptionStack expStack = FrameworkExceptionStack.map.get(Thread.currentThread().getId());
		if(expStack.getAllExceptionsList().size()==0)
			fetchReporterFromTestInstance(result).getCurrentTest().log(Status.PASS, result.getName()+" Passed!");
		else
			fetchReporterFromTestInstance(result).getCurrentTest().log(Status.FAIL, 
					"Exceptions : "+expStack.getAllExceptionsStackTraceAsString());
		
		fetchReporterFromTestInstance(result).finishCurrentTest();
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Test case failed. Capturing screenshot in report !");
		Reporter reporter = fetchReporterFromTestInstance(result);
		fetchReporterFromTestInstance(result).getCurrentTest().log(Status.FAIL, result.getThrowable());
		reporter.addScreenshotInTest(generic_methods.capture_screenshot(
				DriverManager.getCurrentWebDriver(),
				reporter.getReportDir(),
				"FAILURE_"+result.getName()));
		System.out.println("Getting framework exception stack on thread id : "+Thread.currentThread().getId());
		FrameworkExceptionStack expStack = FrameworkExceptionStack.map.get(Thread.currentThread().getId());
		if(expStack.getAllExceptionsList().size()>0)
			reporter.logFailWithoutCapturingScreenshot(
					"Failures : \n"+expStack.getAllExceptionsStackTraceAsString());
		reporter.finishCurrentTest();
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("Test is skipped and calling finish on reporter");
		//fetchReporterFromTestInstance(result).getCurrentTest().log(Status.SKIP, result.getName()+" skipped !");
		fetchReporterFromTestInstance(result).RemoveCurrentTestFromReport();
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
	}

	@Override
	public void onStart(ITestContext context) {
		
	}

	@Override
	public void onFinish(ITestContext context) {
		
	}
}
