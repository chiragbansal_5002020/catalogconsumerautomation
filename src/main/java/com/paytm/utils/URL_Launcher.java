package com.paytm.utils;

public class URL_Launcher {
	public static void launchGridPage(String glpid) {
		if(MiddlewareStatusManager.isMiddlewareStatusOn())
			glpid+="?use_mw=1";
		DriverManager.getWebDriver(Thread.currentThread().getId()).get(ConfigReader.getValue("baseURL")+"/"+glpid);
		WebdriverUtils.waitForPageToLoad();
		SleepUtils.waitForMidTime();
	}
	
	public static String getBaseURL() {
		return ConfigReader.getValue("baseURL");
	}
}
