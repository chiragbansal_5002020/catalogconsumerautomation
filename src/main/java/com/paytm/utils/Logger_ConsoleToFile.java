package com.paytm.utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class Logger_ConsoleToFile {

	public void startLoggerOnFile(String filePath) {

		File logFile = new File(filePath);
		try {
			if(logFile.createNewFile()) {
				System.out.println("Created logfile at path :  "+filePath);
				PrintStream logfilePrintStream = new PrintStream(logFile);
				System.out.println("Starting logging on file ... !");
				System.setOut(logfilePrintStream);
				System.out.println("Started logging on file at path : "+filePath);
			}else {
				System.out.println("Log File could not be createda at path : "+filePath);
			}
		} catch (IOException e) {
			System.out.println("Logging on file could not be stated! \n"
					+"File Path : "+filePath);
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

}
