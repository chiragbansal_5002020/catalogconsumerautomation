package com.paytm.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.paytm.network_sniffer.ActionTracker;
import com.paytm.network_sniffer.Product;
import com.paytm.network_sniffer.TrackerResponse;
import com.paytm.page_classes.BasePage;
import com.paytm.page_classes.ConsumerGridPage;
import com.paytm.page_classes.HomePage;
import com.paytm.reusable_methods.generic_methods;
import com.paytm.utils.DiscoverabilityMapping;
import com.paytm.utils.DriverManager;
import com.paytm.utils.MiddlewareStatusManager;
import com.paytm.utils.NoProductFoundException;
import com.paytm.utils.Reporter;
import com.paytm.utils.SleepUtils;
import com.paytm.utils.URL_Launcher;


public class Grid_TestCases extends Setup{
	BasePage bp;
	ConsumerGridPage cgp;
	HomePage hp;
	generic_methods gm;
	public Reporter htmlReporter = new Reporter().getInstance(this.getClass(),"Grid_validations.html");

	@BeforeTest(alwaysRun=true)
	public void startReport() {
		//htmlReporter.createReport("Grid_validations.html");
		bp = new BasePage();
		cgp = new ConsumerGridPage();
		hp = new HomePage();
		gm = new generic_methods();
	}
	
	@Test
	public void test1() {
		htmlReporter.logInfo("dummy test 1");
		SleepUtils.sleepForMilliseconds(2000);
		htmlReporter.logPass("test passed");
	}
	
	@Test
	public void test2() {
		htmlReporter.logInfo("dummy test 2");
		SleepUtils.sleepForMilliseconds(4000);
		htmlReporter.logPass("test passed");
	}
	
	@Test
	public void test4() {
		htmlReporter.logInfo("dummy test 4");
		SleepUtils.sleepForMilliseconds(5000);
		htmlReporter.logPass("test passed");
	}
	
	@Test
	public void test3() {
		htmlReporter.logInfo("dummy test 3");
		SleepUtils.sleepForMilliseconds(2300);
		htmlReporter.logFailWithoutCapturingScreenshot("test failed");
	}
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class,
			priority=1
		 )
	public void launchGridPage(String glpidORllpid) {
		htmlReporter.logInfo("Testing for id - "+glpidORllpid);
		URL_Launcher.launchGridPage(glpidORllpid);
		if(cgp.isNoProductFoundOnGrid())
			htmlReporter.logFailWithoutCapturingScreenshot("Couldn't load the product page!!");
		else
			htmlReporter.logPass(DriverManager.getCurrentWebDriver().getTitle());
		htmlReporter.captureScreenshot();
	}
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class,
			priority=2)
	public void brokenLinksTest(String glpidORllpid) throws IOException {
		htmlReporter.logInfo("Testing for id - "+glpidORllpid);
		URL_Launcher.launchGridPage(glpidORllpid);
		htmlReporter.logPass(DriverManager.getWebDriver(Thread.currentThread().getId()).getTitle());
		
		htmlReporter.logInfo("Getting Response Code For All Products Link on Grid Pgae");
		Map<String,Integer> map_productsLinkResponseCode = cgp.getResponseCodeForProductsLink(); 
		htmlReporter.logInfo(gm.getRepresentableStringFromMap(map_productsLinkResponseCode));
		Map<String,Integer> map_filteredResultForResponseCode = cgp.getFilteredResponseCodeExcluding(
				map_productsLinkResponseCode, Arrays.asList(200));
		if(map_filteredResultForResponseCode.keySet().size()==0) {
			htmlReporter.logPass("All product links are with response code : 200");
		}else {
			htmlReporter.logFail("Broken links found for some products : \n"
						+gm.getRepresentableStringFromMap(map_filteredResultForResponseCode));
		}
				
		/*htmlReporter.logInfo("Getting all links with response code other than 200 on page.");
		Map<String,Integer> map_allBrokenLinkResponseCode = cgp.getBrokenLinksOnPage(); 
		if(map_allBrokenLinkResponseCode.keySet().size()==0) {
			softAssertion.assertTrue(true,"All links are with response code : 200");
			htmlReporter.logPass("All links are with response code : 200");
		}else {
			softAssertion.assertTrue(false,"Not all links are with response code : 200");
			htmlReporter.logFail("Not all links on page are with response code : 200 \n"+"Below is the list \n"
						+gm.getRepresentableStringFromMap(map_allBrokenLinkResponseCode));
		}*/
	}

	@Test(dataProvider= "GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class,
			priority=3)
	public void sortingFunctionalTest(String glpidORllpid) throws NoProductFoundException {
		
		htmlReporter.logInfo("Testing for id - "+glpidORllpid);
		URL_Launcher.launchGridPage(glpidORllpid);
		htmlReporter.logPass(DriverManager.getCurrentWebDriver().getTitle());
		
		if(cgp.isSortByDropDownPresentOnPage()) {
			htmlReporter.logPass("Sort By drop down present on Grid page!");
		}else {
			htmlReporter.logFail("Sort By drop down not present on Grid page! ");
		}

		SleepUtils.waitForMidTime();
		List<String> sortingOptionsList = cgp.getSortingOptionsList();
		htmlReporter.logInfo("Sorting options : "+sortingOptionsList);
		if(sortingOptionsList.contains("New") && sortingOptionsList.contains("Popular") 
				&& sortingOptionsList.contains("Low to High Price") && sortingOptionsList.contains("High to Low Price")) {
			htmlReporter.logPass("All 4 sorting options present !");
		}else {
			htmlReporter.logFail("All 4 sorting options (New, Popular, High to Low Price, Low to High Price) not present!");
		}
		
		htmlReporter.logInfo("Prices order present on Grid before sorting: "+cgp.getListOfPricesOnGrid());

		htmlReporter.logInfo("Sorting products by High to Low Price");
		cgp.clickSortingOption(cgp.highToLowPrice);
		htmlReporter.logInfo("Prices order on Grid after sorting high to low : "+cgp.getListOfPricesOnGrid());
		if(cgp.isGridSorted(cgp.highToLowPrice)) {
			htmlReporter.logPass("High to Low Sort option is working fine");
		}else {
			htmlReporter.logFail("High to Low Sort option is not working."+"\n"+
					"Sorting failed for product : "+cgp.getProductNameWhereSortingFailed(cgp.highToLowPrice)
					+" -- "+cgp.getProductPriceWhereSortingFailed(cgp.highToLowPrice));
			htmlReporter.logInfo(gm.getRepresentableStringFromMap(cgp.getProductsNamePriceMap()));
		}

		htmlReporter.logInfo( "Sorting products by Low to High Price");
		cgp.clickSortingOption(cgp.lowToHighPrice);
		htmlReporter.logInfo( "Prices order on Grid after sorting low to high : "+cgp.getListOfPricesOnGrid());
		if(cgp.isGridSorted(cgp.lowToHighPrice)) {
			htmlReporter.logPass( "Low to High Sort option is working fine");
		}else {
			htmlReporter.logFail("Low to High Sort option is not working."+"\n"+
					"Sorting failed for product : "+cgp.getProductNameWhereSortingFailed(cgp.lowToHighPrice)
					+" -- "+cgp.getProductPriceWhereSortingFailed(cgp.lowToHighPrice));
			htmlReporter.logInfo(gm.getRepresentableStringFromMap(cgp.getProductsNamePriceMap()));
		}
	}
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class,
			priority=4,
			groups= {"Filtering"})
	public void priceFilteringFunctionalTest(String glpidORllpid) throws NoProductFoundException  {
		htmlReporter.logInfo("Testing for id - "+glpidORllpid);
		URL_Launcher.launchGridPage(glpidORllpid);
		htmlReporter.logPass(DriverManager.getWebDriver(Thread.currentThread().getId()).getTitle());
		
		if(cgp.isPriceRangeFilterDisplayedOnUI()) {
			Assert.assertTrue(true, "Price Range Filter Present on UI");
			htmlReporter.logPass("Price Range Filter present on Grid Page");
		}else {
			Assert.assertTrue(false,"Price Range Filter not Present on UI");
			htmlReporter.logFail("Price Range Filter not present on Grid Page");
		}
		
		int minPrice = cgp.getMinimumPriceFromPriceRangeFilter();
		htmlReporter.logInfo("Testing for Minimum Price Range");
		htmlReporter.logInfo("Setting Price Range : "+minPrice+" --- "+minPrice);
		cgp.setPriceRange(minPrice, minPrice);
		if(cgp.isPriceRangeSetInFilter(minPrice, minPrice)) {
			htmlReporter.logInfo("Products filtered should have price :  "+minPrice);
			htmlReporter.logInfo("Product prices on grid page : "+cgp.getListOfPricesOnGrid());
			if(cgp.isAllPricesOnGridInRange(minPrice, minPrice)) {
				htmlReporter.logPass("All products filtered on grid are with price : "+minPrice);
			}else {
				htmlReporter.logFail("All products filtered on grid are not with price : "+minPrice);
				htmlReporter.logInfo(gm.getRepresentableStringFromMap(cgp.getProductsNamePriceMap()));
			}
		}else {
			htmlReporter.logFail("Price range couldn't be set in filter for value -- "+minPrice+" , "+minPrice);
		} 
		
		
		//SleepUtils.waitForShortTime();
		
		int maxPrice = cgp.getMaximumPriceFromPriceRangeFilter();
		htmlReporter.logInfo("Testing for Maximum Price Range");
		htmlReporter.logInfo("Setting Price Range : "+maxPrice+" --- "+maxPrice);
		cgp.setPriceRange(maxPrice, maxPrice);
		if(cgp.isPriceRangeSetInFilter(maxPrice, maxPrice)) {
			htmlReporter.logInfo("Products filtered should have price :  "+maxPrice);
			htmlReporter.logInfo("Product prices on grid page : "+cgp.getListOfPricesOnGrid());
			if(cgp.isAllPricesOnGridInRange(maxPrice, maxPrice)) {
				htmlReporter.logPass("All products filtered on grid are with price : "+maxPrice);
			}else {
				htmlReporter.logFail("All products filtered on grid are not with price : "+maxPrice);
				htmlReporter.logInfo(gm.getRepresentableStringFromMap(cgp.getProductsNamePriceMap()));
			}
		}else {
			htmlReporter.logFail("Price range couldn't be set in filter for value -- "+maxPrice+" , "+maxPrice);
		}
		
		
		//SleepUtils.waitForShortTime();
		List<Integer> randomPricesList = cgp.setRandomPriceRange();
		htmlReporter.logInfo("Testing for Random Price Range");
		htmlReporter.logInfo( "Setting Price Range in filter :  "+randomPricesList.get(0)+" --- "+randomPricesList.get(1));
		
		if(cgp.isPriceRangeSetInFilter(randomPricesList.get(0), randomPricesList.get(1))) {
			htmlReporter.logInfo( "Products filtered should be in price range : "+randomPricesList.get(0)+" to "
					+randomPricesList.get(1));
			htmlReporter.logInfo( "Prices on page : "+cgp.getListOfPricesOnGrid());
			boolean isPricesInRange = cgp.isAllPricesOnGridInRange(randomPricesList.get(0), randomPricesList.get(1));
			if(isPricesInRange) {
				htmlReporter.logPass( "Prices of products listed is within specified range");
			}else {
				htmlReporter.logFail( "Prices of products listed is not within specified range");
				htmlReporter.logFail("Price filter failed at price : "+
				cgp.getPriceWherePriceFilterFailed(randomPricesList.get(0), randomPricesList.get(1)));
				htmlReporter.logInfo(gm.getRepresentableStringFromMap(cgp.getProductsNamePriceMap()));
			}
		}else {
			htmlReporter.logFail("Price range couldn't be set in filter for value -- "+randomPricesList.get(0)+" , "+randomPricesList.get(1));
		}
	}
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class,
			priority=5,
			groups= {"Filtering"})
	public void attributeFiltersFunctionalTest(String glpidORllpid) throws NoProductFoundException  {
		int productQuantityOnGrid;
		int productQuantityDisplayedWithinFilters;
		htmlReporter.logInfo("Testing for id - "+glpidORllpid);
		URL_Launcher.launchGridPage(glpidORllpid);
		htmlReporter.logPass(DriverManager.getWebDriver(Thread.currentThread().getId()).getTitle());
		
		List<String> filterList = cgp.getAttributeFiltersList();
		htmlReporter.logInfo("Available Attribute filters : "+filterList);
		
		htmlReporter.logInfo("Test with single attribute and single filter vlaue");
		htmlReporter.logInfo("Applying filter on : "+filterList.get(0) +" --> "+"\n"+
				cgp.selectRandomFiltersForAnAttribute(filterList.get(0),1));
		productQuantityDisplayedWithinFilters = cgp.getTotalProductQtyDisplyedRightSideInAFilter(filterList.get(0));
		productQuantityOnGrid = cgp.getProductQtyOnGridPage();
		htmlReporter.logInfo("Quantity Displayed along side filter : "+productQuantityDisplayedWithinFilters);
		htmlReporter.logInfo("Quantity displayed on grid : "+productQuantityOnGrid);
		if(productQuantityOnGrid == productQuantityDisplayedWithinFilters) {
			htmlReporter.logPass("Attribute filter with single value working fine.");
		}else {
			htmlReporter.logFail("Attribute filter with single value not working correctly!");
		}
		
		htmlReporter.logInfo("Clearing all filters!");
		cgp.clearAllAttributeFilters();
		SleepUtils.waitForShortTime();
		
		htmlReporter.logInfo("Test with single attribute and multiple filter vlaues.");
		htmlReporter.logInfo("Applying filter on : "+filterList.get(0) +" --> "+"\n"+
				cgp.selectRandomFiltersForAnAttribute(filterList.get(0),3));
		productQuantityDisplayedWithinFilters = cgp.getTotalProductQtyDisplyedRightSideInAFilter(filterList.get(0));
		productQuantityOnGrid = cgp.getProductQtyOnGridPage();
		htmlReporter.logInfo("Quantity Displayed along side filter : "+productQuantityDisplayedWithinFilters);
		htmlReporter.logInfo("Quantity displayed on grid : "+productQuantityOnGrid);
		if(productQuantityOnGrid == productQuantityDisplayedWithinFilters) {
			htmlReporter.logPass("Attribute filter with multiple values working fine.");
		}else {
			htmlReporter.logFail("Attribute filter with multiple values not working correctly!");
		}
		
		htmlReporter.logInfo("Clearing all filters!");
		cgp.clearAllAttributeFilters();
		SleepUtils.waitForShortTime();
		
		htmlReporter.logInfo("Test with 2 attributes and multiple filter vlaues.");
		htmlReporter.logInfo("Applying filter on : "+filterList.get(0) +" --> "+"\n"+
				cgp.selectRandomFiltersForAnAttribute(filterList.get(0),2));
		SleepUtils.waitForMidTime();
		filterList = cgp.getAttributeFiltersList();
		htmlReporter.logInfo("Applying filter on : "+filterList.get(1) +" ---> "+"\n"+
				cgp.selectRandomFiltersForAnAttribute(filterList.get(1), 2));
		SleepUtils.waitForMidTime();
		int productQtyForFirstFilter = cgp.getTotalProductQtyDisplyedRightSideInAFilter(filterList.get(0));
		int productQtyForSecondFilter = cgp.getTotalProductQtyDisplyedRightSideInAFilter(filterList.get(1));
		productQuantityOnGrid = cgp.getProductQtyOnGridPage();
		htmlReporter.logInfo("Quantity Displayed along side filter1 : "+productQtyForFirstFilter);
		htmlReporter.logInfo("Quantity Displayed along side filter2 : "+productQtyForSecondFilter);
		htmlReporter.logInfo("Quantity displayed on grid : "+productQuantityOnGrid);
		if(productQuantityOnGrid == cgp.get_AND_ResultOfPrices(productQtyForFirstFilter,productQtyForSecondFilter)) {
			htmlReporter.logPass("Attribute filter with multiple values working fine.");
		}else {
			htmlReporter.logFail("Attribute filter with multiple values not working correctly!");
		}

	}
		
	
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class,
			priority=6)
	public void filterValuesFormatTest(String glpidORllpid) throws IOException {
		htmlReporter.logInfo("Testing for id - "+glpidORllpid);
		URL_Launcher.launchGridPage(glpidORllpid);
		htmlReporter.logPass(DriverManager.getWebDriver(Thread.currentThread().getId()).getTitle());
		
		htmlReporter.logInfo("Checking format of filter values ... ");
		Map<String,String> map_filterValueFormat = cgp.getFilterValueFormatStatus();
		htmlReporter.logInfo(gm.getRepresentableStringFromMap(map_filterValueFormat));
		Map<String,String> incorrectFormatMap = cgp.getIncorrectFilterValueFormatMap();
		if(incorrectFormatMap.size()==0) {
			htmlReporter.logPass("All Filter values are in correct format");
		}else {
			htmlReporter.logFail("All Filter values are not in correct format ");
			htmlReporter.logFail("Incorrect Format filter values : \n"+gm.getRepresentableStringFromMap(incorrectFormatMap));
		}
	}
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class,
			priority=7)
	public void verifyProductsOnSecondPageOfGrid(String glpidORllpid) throws IOException, NoProductFoundException {
		htmlReporter.logInfo("Testing for id - "+glpidORllpid);
		URL_Launcher.launchGridPage(glpidORllpid);
		htmlReporter.logPass(DriverManager.getWebDriver(Thread.currentThread().getId()).getTitle());
		
		htmlReporter.logInfo("Testing for product visibility on 2nd page ...!");
		if(cgp.isNextPageAvailable()) {
			htmlReporter.logPass("Next Page is available for the products on grid !");
			List<String> productNamesListCurrentPage = cgp.getProductNamesListOnGrid();
			htmlReporter.logInfo("Current page product list - : "+"\n"+
					gm.getRepresentableStringFromList(productNamesListCurrentPage));
			htmlReporter.logInfo("Navigating to next page on grid");
			cgp.goToNextPage();
			if(cgp.isProductPresentOnGrid()) {
				htmlReporter.logPass("Products visible on next page !");
				List<String> productNamesListNextPage = cgp.getProductNamesListOnGrid();
				htmlReporter.logInfo("Next page product list - :"+"\n"+
						gm.getRepresentableStringFromList(productNamesListNextPage));
				if(!cgp.isTwoListContainsSameValue(productNamesListCurrentPage,productNamesListNextPage )) {
					htmlReporter.logPass("Products verfication on next page successful!");
				}else {
					htmlReporter.logFail("Products on previous page and next page are same. So there may be chances "
							+"that click on next page didn't work !");
					htmlReporter.logFail("Products verfication on next page failed !");
				}
			}else {
				htmlReporter.logFail("Some products are not visible on next page!");
			}	
		} else {
			htmlReporter.logFail("Next page is not available for the products on grid!");
		}
	}
	
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class
			)
	public void addToCartTest(String testID) throws Exception {
		//new DriverManager().restartBrowser();
		String filterValue = MiddlewareStatusManager.isMiddlewareStatusOn()?"middleware.paytmmall":"catalog.paytm";
		htmlReporter.logInfo("Testing for ID - "+testID);
		htmlReporter.logInfo("Testing for Add to Cart by plus icon on the product !");
		String categoryID = testID.split("-")[testID.split("-").length-1];
		
		/*String dbQuery = "Select info from catalog_category where id='"+categoryID+"'";
		DBFunctions dbfs = new DBFunctions();
		String info = dbfs.SelectQuery(1, dbQuery, "mktplace_catalog");
		
		int add_to_cart_valueInDB = (int) FrameworkJSONParser.parseJsonString(info, ".actions.add_to_cart");
		Reporter.logInfo("add_to_cart value from db for category id "+categoryID+"  -> "+add_to_cart_valueInDB);*/
		
		int add_to_cart_valueInDB=1;
		if(add_to_cart_valueInDB!=1) {
			htmlReporter.logFailWithoutCapturingScreenshot("add_to_cart vlaue in DB is not 1");
		}else {
			ActionTracker tracker = new ActionTracker(filterValue);
			TrackerResponse response = tracker.trackUrlLaunch(
					URL_Launcher.getBaseURL()+"/"+testID+(MiddlewareStatusManager.isMiddlewareStatusOn()?"?use_mw=1":""), 10);
			
			double cartValueBeforeAddingProduct = Double.MIN_VALUE;
			double cartValueAfterAddingProduct = Double.MIN_VALUE;
		
			htmlReporter.logInfo("Applying filter on API requests with - "+ filterValue);
			int add_to_cart_for_category = response.getAddToCartValueForCategory(filterValue);
			htmlReporter.logInfo("add_to_cart value for category - "+add_to_cart_for_category);
			if(add_to_cart_for_category == 1) {
				htmlReporter.logPass("add_to_cart value is 1 for category ");
			}else {
				htmlReporter.logFailWithoutCapturingScreenshot("add_to_cart value is not 1 for category");
			}
			List<Product> products = response.getProductsFromGridLayoutJSON(filterValue);
			if(products.size()==0) {
				htmlReporter.logFail("No product extracted from JSON response !");
			}else {
				if(!cgp.isCartEmpty()) {
					htmlReporter.logInfo("Cart is not empty. Before testing the 'Add to Cart' feature, "
							+ "removing all the items from cart");
					if(cgp.removeAllProductsFromCart()) {
						htmlReporter.logInfo("All the products removed from cart. Starting the test now ...");
					}else {
						htmlReporter.logInfo("All products couldn't be removed from cart. "
								+"it may fail the testcase in subsequent steps");
						htmlReporter.captureScreenshot();
					}
				}
				
				for(Product product : products) {
					htmlReporter.logInfo("Product Name : "+product.getName());
					htmlReporter.logInfo("add_to_cart value in json for the product = "+product.getAdd_to_cart());
						if(cgp.isAddToCartPlusIconAvailableForProduct(product.getName())) {
							htmlReporter.logPass("Add to Cart Plus Icon present on UI");
						}else {
							htmlReporter.logFailWithoutCapturingScreenshot("Add to Cart Plus Icon not present on UI");
						}
				}
				
				Product productToAdd = products.get(0);
				cartValueBeforeAddingProduct = cgp.getCartValue();
				htmlReporter.logInfo("Cart value before adding product : "+ cartValueBeforeAddingProduct);
				cgp.addProductToCartByPlusIcon(productToAdd.getName());
				SleepUtils.sleepForMilliseconds(1500);
				cartValueAfterAddingProduct = cgp.getCartValue();
				htmlReporter.logInfo("Adding product to cart : "+productToAdd.getName());
				htmlReporter.logInfo("Product price : "+productToAdd.getEffective_price());
				htmlReporter.logInfo("Cart value after adding product : "+cartValueAfterAddingProduct);
				if(cartValueBeforeAddingProduct+productToAdd.getEffective_price() == cartValueAfterAddingProduct) {
					htmlReporter.logPass("Add to Cart working fine");
					htmlReporter.captureScreenshot();
				}else {
					htmlReporter.logFailWithoutCapturingScreenshot("Add to Cart not working fine");
				}
				SleepUtils.sleepForMilliseconds(5000);
				htmlReporter.logInfo("Removing product from cart");
				if(cgp.removeAllProductsFromCart()) {
					htmlReporter.logPass("Product removed from cart successfully");
				}else {
					htmlReporter.logFailWithoutCapturingScreenshot("Product not removed from cart");
				}
				
			}
		}
		
		htmlReporter.captureScreenshot();
	}
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class
			)
	public void serviceabilityTest(String testID) throws Exception {
		//new DriverManager().restartBrowser();
		String filterValue = MiddlewareStatusManager.isMiddlewareStatusOn()?"middleware.paytmmall":"catalog.paytm";
		String pincodeServiceable = "201301";
		String pincodeNotServiceable = "125048";
		ConsumerGridPage cgp = new ConsumerGridPage();
		generic_methods gm = new generic_methods();
		URL_Launcher.launchGridPage(testID);
		SleepUtils.waitForMidTime();
		TrackerResponse response = null;
		List<Product> productList = null;
		
		htmlReporter.logInfo("Testing for serviceability on page!");
		htmlReporter.logInfo("Testing with pincode where product should be serviceable ..!");
		htmlReporter.logInfo("Pincode - "+pincodeServiceable);
		response = cgp.enterPincodeAndGetTrackerResponse(pincodeServiceable,filterValue);
		productList = response.getProductsFromGridLayoutJSON(filterValue);
		htmlReporter.logInfo(gm.getRepresentableStringFromList(productList));
		if(cgp.isAllProductsServiceable(productList)) {
			htmlReporter.logPass("All the products are serviceable");
		}else {
			htmlReporter.logFail("Not all products are serviceable");
		}
		
		htmlReporter.logInfo("Checking for serviceability order .. ");
		if(cgp.isServiceabilityOrderCorrect(productList)) {
			htmlReporter.logPass("Serviceability order is correct!");
		}else {
			htmlReporter.logFail("Serviceability order is not correct!");
		}
		
		htmlReporter.logInfo("Testing with pincode where product should not be serviceable ..!");
		htmlReporter.logInfo("Testing with pincode - "+pincodeNotServiceable);
		response = cgp.changePincodeAndGetTrackerResponse(pincodeNotServiceable,filterValue);
		productList = response.getProductsFromGridLayoutJSON(filterValue);
		htmlReporter.logInfo(gm.getRepresentableStringFromList(productList));
		if(!cgp.isAllProductsServiceable(productList)) {
			htmlReporter.logPass("Products are not serviceable");
		}else {
			htmlReporter.logFail("Products are serviceable");
		}
		cgp.changePincode(pincodeServiceable);
		
	}
	
	
	@Test(dataProvider="GridTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.Grid_DataProvider.class
			)
	public void discoverabilityTest(String testID) throws Exception {
		String filterValue = MiddlewareStatusManager.isMiddlewareStatusOn()?"middleware.paytmmall":"catalog.paytm";
		int expectedDiscoverabilityValue = 0;
		if(testID.toLowerCase().contains("discoverability=offline")) {
			expectedDiscoverabilityValue = DiscoverabilityMapping.OFFLINE_VAL;
		}else if(testID.toLowerCase().contains("discoverability=online")) {
			expectedDiscoverabilityValue = DiscoverabilityMapping.ONLINE_VAL;
		}else if(testID.toLowerCase().contains("discoverability=b2b")) {
			expectedDiscoverabilityValue = DiscoverabilityMapping.B2B_VAL;
		}else if(testID.toLowerCase().contains("discoverability=storefront")) {
			expectedDiscoverabilityValue = DiscoverabilityMapping.STOREFRONT_VAL;
		}
		
		htmlReporter.logInfo("Testing for discoverability on page - "+testID);
		htmlReporter.logInfo("Expected discoverability value for products - "+expectedDiscoverabilityValue);
		ActionTracker tracker = new ActionTracker(filterValue);
		TrackerResponse response = tracker.trackUrlLaunch(
				URL_Launcher.getBaseURL()+"/"+testID+(MiddlewareStatusManager.isMiddlewareStatusOn()?"&use_mw=1":""), 10);
		htmlReporter.logInfo("Applying filter on API requests with - "+ filterValue);
		List<Product> products = response.getProductsFromGridLayoutJSON(filterValue);
		if(products.size()==0) {
			htmlReporter.logFail("No product extracted from JSON response !");
		}else {
			for(Product product : products) {
				htmlReporter.logInfo(product.toString());
				if(product.getDiscoverability()==expectedDiscoverabilityValue) {
					htmlReporter.logPass("Discoverability Value for product is as expected");
				}else {
					htmlReporter.logFailWithoutCapturingScreenshot("Expected Discoverability value is "+expectedDiscoverabilityValue
							+"\nBut actual vallue is "+product.getDiscoverability());
				}
			}
		}
	
		htmlReporter.captureScreenshot();
	}
	
	@Test 
	public void dummyTestGrid(){
		htmlReporter.logInfo("This is a dummy grid test case");
		DriverManager.getCurrentWebDriver().get("https://www.paytmmall.com");
		htmlReporter.captureScreenshot();
		htmlReporter.logPass("Test Passed!!!");
	}
	
	@AfterTest(alwaysRun=true)
	public void endReport() {
		//htmlReporter.finishCurrentTestReport();
	}
}
