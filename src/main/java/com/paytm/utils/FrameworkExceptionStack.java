package com.paytm.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FrameworkExceptionStack {
	public static Map<Long, FrameworkExceptionStack> map = new HashMap<>();
	private  List<Exception> exceptionsList;
	
	public FrameworkExceptionStack(Long threadId) {
		System.out.println("Associating framework stack with thread id : "+threadId);
		if(map.get(threadId)!=null) {
			System.out.println("Framework stack already initialized with thread id, associating new stack");
			map.remove(threadId);
			map.put(threadId, this);
		}else {
			map.put(threadId, this);
		}
		
		exceptionsList = new ArrayList<>();
	}
	
	public  void add(Exception e) {
		exceptionsList.add(e);
	}
	
	public static FrameworkExceptionStack getCurrentStack() {
		return map.get(Thread.currentThread().getId());
	}
	/*public void initializeExceptionStack() {
		exceptionsList = new ArrayList<Exception>();
	}
	*/
	public List<Exception> getAllExceptionsList() {
		return exceptionsList;
	}
	
	public String getAllExceptionsStackTraceAsString() {
		String longString="";
		for(Exception e: exceptionsList) {
			StackTraceElement[] trace = e.getStackTrace();
			longString+=e.getMessage()+"\n";
			longString+=e.getClass().getName()+"\n";
			for(int i=0; i<trace.length; i++) {
				StackTraceElement ele = trace[i];
				if(ele.getClassName().startsWith("com.paytm")) {
					//longString+=ele.getFileName()+", Class: "+ele.getClassName()+" , Method: "+ele.getMethodName()+"("
					//		+"line- "+ele.getLineNumber()+")"+"\n";
					longString+="at "+ele.getClassName()+"."+ele.getMethodName()+"("+ele.getFileName()+":"+ele.getLineNumber()+")\n";
				}
			}
			longString+="\n================================================================================= \n\n";
		}
		return longString;
	}
	
	/*public static void main(String[] args) {
		initializeExceptionStack();
		System.out.println("Adding arithmetic exception in stack");
		try {
			int a = 10/0;
		}catch(ArithmeticException e) {
			FrameworkExceptionStack.add(e);
			//e.printStackTrace();
		}
		System.out.println("Adding some other exception ");
		try {
			new FileReader(new File("./hello.java"));
		}catch(Exception e) {
			//e.printStackTrace();
			FrameworkExceptionStack.add(e);
		}
		System.out.println("Printing all exceptoins");
		System.out.println(FrameworkExceptionStack.getAllExceptionsStackTraceAsString());
	}*/
}
