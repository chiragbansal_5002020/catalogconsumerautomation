package com.paytm.page_classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.paytm.network_sniffer.ActionTracker;
import com.paytm.network_sniffer.Product;
import com.paytm.network_sniffer.TrackerResponse;
import com.paytm.reusable_methods.ResponseCodeProvider;
import com.paytm.reusable_methods.generic_methods;
import com.paytm.utils.ConfigReader;
import com.paytm.utils.DriverManager;
import com.paytm.utils.NoProductFoundException;
import com.paytm.utils.SleepUtils;
import com.paytm.utils.WebdriverUtils;




public class ConsumerGridPage extends WebdriverUtils{


	By locator_gridProductsHeadingText = By.xpath("//h1[@class='_1qXr']");
	By locator_clearFilterButtonsList = By.xpath("//div[@class='_24le _1zvo']//i[@class='iconCross']");
	By locator_attributeFiltersList = By.xpath("(//div[@class='_24le _1zvo'])[position()>1]"); 
	By locator_selectedFiltersList = By.xpath("//div[@class='_7PLv']//span[@class='_34iq']/span[1]");
	By locator_productQtyOnGridPage = By.xpath("//span[@class='_3-is']");
	By locator_minPriceRangePlaceholder = By.xpath("//input[@placeholder='Min']");
	By locator_maxPriceRangePlaceholder = By.xpath("//input[@placeholder='Max']");
	By locator_priceRangeTextNearClearAllButton = By.xpath("//span[@class='_34iq']");
	By locator_priceRangeFilterSlider = By.xpath("//div[@class='rc-slider']");
	By locator_priceRangeSliderMinHandle = By.xpath("//div[contains(@class,'rc-slider-handle-1')]");
	By locator_priceRangeSliderMaxHandle = By.xpath("//div[contains(@class,'rc-slider-handle-2')]");
	By locator_priceRangeSliderDiv = By.xpath("//div[@role='slider']");
	By locator_pricesDivList = By.xpath("//div[@class='_1kMS']");
	By locator_sortByDropDown = By.xpath("//span[contains(text(),'Sort by')]");
	By locator_sortingOptionsList = By.xpath("//div[@class='_29BM']//div");
	By locator_allProductsAnchorElements = By.xpath("//div[@class='_3RA-']//a");
	By locator_nextButton = By.xpath("//a[text()='Next']");
	By locator_productImages = By.xpath("//img[@role='presentation']");
	By locator_leftSliderPriceRangeFilter = By.xpath("//div[@class='rc-slider']//div[@role='slider'][1]");
	By locator_rightSliderPriceRangeFilter = By.xpath("//div[@class='rc-slider']//div[@role='slider'][2]");
	By locator_addToCartPlusIcon = By.xpath("//i[@class='iconPlus']/parent::div");
	By locator_cartValueDiv = By.xpath("//i[@class='iconOrder']/ancestor::div[2]//div[contains(text(),'Rs')]");
	By locator_enterPincodeLink = By.xpath("//span[text()='Enter Pincode']");
	By locator_enterPincodeTextbox = By.xpath("//input[@placeholder='Enter Pincode']");
	By locator_pincodeCheck = By.xpath("//span[text()='Check']");
	By locator_pinocdeChange = By.xpath("//span[text()='Change']");
	By locator_removeProductFromCart = By.xpath("//i[@class='iconDelete' or @class='iconTrash']");
	By locator_cartIcon = By.xpath("//i[@class='iconOrder']");
	By locator_noProductFoundMessage = By.xpath("//span[contains(text(),\"Sorry! Couldn’t find the product you were looking for!!\")]");
	By locator_cartDivForFMCGProducts = By.xpath("//div[@class='JGxa']");
	
	public enum SortingOptionsGrid{
		NEW,POPULAR,LOW_TO_HIGH_PRICE,HIGH_TO_LOW_PRICE
	}

	public SortingOptionsGrid lowToHighPrice = SortingOptionsGrid.LOW_TO_HIGH_PRICE;
	public SortingOptionsGrid highToLowPrice = SortingOptionsGrid.HIGH_TO_LOW_PRICE;
	generic_methods gm = new generic_methods();


	public String getHeaderTextOnGrid() {
		return getWebElement(locator_gridProductsHeadingText , true).getText();
	}

	public void clearAllAttributeFilters(){
		System.out.println("Clearing all filters applied on attributes of product ... ");
		List<WebElement> clearFilterButtonList = getWebElements(locator_clearFilterButtonsList);
		for(WebElement clearButton : clearFilterButtonList) {
			clearButton.click();
			SleepUtils.sleepForMilliseconds(500);
		}
		SleepUtils.sleepForMilliseconds(2000);
		System.out.println("Cleared all filters !");
	}


	public List<String> getlistOfSelectedFiltersOnGrid(){
		List<WebElement> filtersElementList = getWebElements(locator_selectedFiltersList);
		List<String> selectedFiltersList = new ArrayList<String>();
		for(WebElement currentFilter : filtersElementList) {
			selectedFiltersList.add(currentFilter.getText().trim());
		}
		return selectedFiltersList;
	}
	
	public boolean isTwoListContainsSameValue(List<String> list1, List<String> list2 ) {
		boolean result = true;
		for(String val : list1) {
			if(list2.contains(val))continue;
			else {
				result = false;
				break;
			}
		}
		
		return result;
	}
	
	public List<String> getAttributeFiltersList(){
		waitForPageToLoad();
		List<String> attributeFilterList_text = new LinkedList<String>();
		List<WebElement> attributeFilterList = getWebElements(locator_attributeFiltersList);
		String filterName = null;

		for(WebElement currentFilter : attributeFilterList) {
			filterName = getInnerTextWithJavaScript(
					currentFilter.findElement(By.xpath(".//div[@class='_1sRm']"))).trim();
			attributeFilterList_text.add(filterName);
		}
		return attributeFilterList_text;
	}

	public List<String> selectRandomFiltersOnPage(){
		waitForPageToLoad();
		List<String> filtersList = new LinkedList<String>();
		List<WebElement> attributeFilterList = getWebElements(locator_attributeFiltersList);
		String filterName = null;
		
		Random random = new Random();
		for(WebElement currentFilter : attributeFilterList) {
			filterName = getInnerTextWithJavaScript(
					currentFilter.findElement(By.xpath(".//div[@class='_1sRm']"))).trim();
			System.out.println("--------------------------------------------------------");
			System.out.println("Testing filter - "+filterName);
			if(currentFilter.findElements(By.xpath(".//i[@class='iconPlus']")).size()>0) {
				currentFilter.click();
				SleepUtils.sleepForMilliseconds(500);
			}

			List<WebElement> optionsForCurrentFilter = currentFilter.findElements(By.xpath(".//div[@class='Gswd']"));
			String filterVal;
			filterVal = getInnerTextWithJavaScript(optionsForCurrentFilter.get(
					random.nextInt(optionsForCurrentFilter.size()))
					.findElement(By.xpath(".//span[@class='labelName']"))).trim();

			filtersList.add(filterVal);
			applyAttributeFilter(filterName, new ArrayList<String>(Arrays.asList(filterVal)));
		}
		return filtersList;

	}
	
	
	public List<String> selectRandomFiltersForAnAttribute(String attributeName,int numberOfFilters) {
		waitForPageToLoad();
		List<String> filtersList = new LinkedList<String>();
		List<Integer> randomIntList = new ArrayList<Integer>();
		List<WebElement> attributeFilterList = getWebElements(locator_attributeFiltersList);
		String filterName = null;
		
		Random random = new Random();
		for(WebElement currentFilter : attributeFilterList) {
			filterName = getInnerTextWithJavaScript(
					currentFilter.findElement(By.xpath(".//div[@class='_1sRm']"))).trim();
			if(filterName.equalsIgnoreCase(attributeName)) {
				if(currentFilter.findElements(By.xpath(".//i[@class='iconPlus']")).size()>0) {
					currentFilter.click();
					SleepUtils.sleepForMilliseconds(500);
				}
				List<WebElement> optionsForCurrentFilter_element = currentFilter.findElements(By.xpath(".//div[@class='Gswd']"));
				List<String> optionsForCurrentFilter_text= new ArrayList<String>(); 
				for(WebElement option : optionsForCurrentFilter_element) {
					optionsForCurrentFilter_text.add(getInnerTextWithJavaScript(option
							.findElement(By.xpath(".//span[@class='labelName']"))).trim());
				}
				
				if(numberOfFilters>optionsForCurrentFilter_text.size()) {
					applyAttributeFilter(attributeName, optionsForCurrentFilter_text);
					filtersList = new ArrayList<String>(optionsForCurrentFilter_text);
				
				}else {
					for(int i=1;i<=numberOfFilters;i++) {
						int randomIndex;
						do {
							randomIndex = random.nextInt(optionsForCurrentFilter_text.size());
						}while(randomIntList.contains(randomIndex));
						filtersList.add(optionsForCurrentFilter_text.get(randomIndex));	
						randomIntList.add(randomIndex);
					}
					applyAttributeFilter(filterName, filtersList);
				}
				break;
			}

		}
		return filtersList;
	}

	public void applyAttributeFilter(String filterName, List<String> filterValuesList){
		waitForPageToLoad();
		//SleepUtils.waitForMidTime();
		System.out.println("Applying filter on attribute : "+filterName);
		System.out.println("Applying filters on values : "+filterValuesList);
		

		for(String filterVal : filterValuesList) {
			WebElement checkbox = getWebElement(By.xpath("//span[@class='labelName' and normalize-space()=\""+filterVal+"\"]"
					+ "/preceding-sibling::span[@class='check']"), false);
			checkbox.click();
			WebElement inputBox = getWebElement(By.xpath("(//span[@class='labelName'  and normalize-space()=\""+filterVal+"\"]"
					+ "/preceding::input[@type='checkbox'])[last()]"), false);
			if(inputBox.isSelected())
				System.out.println("Selected filter : "+filterVal);
			else
				System.out.println("Some problem occured while selecting filter : "+filterVal);
			SleepUtils.waitForShortTime();
		}
	}


	public int get_AND_ResultOfPrices(int... prices) {
		int result=Integer.MAX_VALUE;
		for(int i=0; i<prices.length;i++) 
			result = prices[i]<result?prices[i]:result;
		return result;
	}
	
	public int getProductQtyOnGridPage() throws NoProductFoundException   {
		//If no product is found on the page, throw no product found exception
		if(isNoProductFoundOnGrid())
			throw new NoProductFoundException();
		return Integer.parseInt(getWebElement(
				locator_productQtyOnGridPage,true).getText().replaceAll("-", "").replaceAll("Products","").replaceAll(",","").trim());
	}

	public void setPriceRange(int minAmount, int maxAmount){
		WebElement minPriceInputBox = getWebElement(locator_minPriceRangePlaceholder,true);
		minPriceInputBox.clear();
		minPriceInputBox.sendKeys(Integer.toString(minAmount));
		WebElement maxPriceInputBox = getWebElement(locator_maxPriceRangePlaceholder,true);
		maxPriceInputBox.clear();
		maxPriceInputBox.sendKeys(Integer.toString(maxAmount));
		SleepUtils.waitForMidTime();
		if(!isNoProductFoundOnGrid() && !isPriceRangeSetInFilter(minAmount, maxAmount)){
			System.out.println("Price range was not set using webdriver sendkeys. Passing as a query string in url.");
			String currentURL = DriverManager.getCurrentWebDriver().getCurrentUrl();
			String queryString = "price="+minAmount+"%2C"+maxAmount;
			if(!currentURL.contains("?")) {
				//No query string is there is URL, so append the query string directly.
				currentURL+="?"+queryString;
			}else {
				//Check if there is price in query string already in URL, 
				//If that is the case, then replace existing price range with new price range.
				if(currentURL.contains("price=")) {
					currentURL=currentURL.replaceAll("price=\\d+%2[cC]\\d+", queryString);
				}else {
					//If price is not present then add the price range in last of URL
					currentURL+="&"+queryString;
				}
			}
			DriverManager.getCurrentWebDriver().get(currentURL);
			waitForPageToLoad();
		}
	}

	public void validatePriceRangeTextNearClearAllButton(int minPrice, int maxPrice) {
		System.out.println("Validating minimum and maximum Price text near clear all button --- ");
		System.out.println("Minimum Price should be : "+minPrice);
		System.out.println("Maximum Price should be : "+maxPrice);
		String textOnClearAllFilter = getInnerTextWithJavaScript(getWebElement(locator_priceRangeTextNearClearAllButton ,true))
				.trim().replaceAll("Rs", "");
		int minPriceOnClearAllFilter = Integer.parseInt(textOnClearAllFilter.split("-")[0].trim());
		int maxPriceOnClearAllFilter = Integer.parseInt(textOnClearAllFilter.split("-")[1].trim());
		System.out.println("Actual values on clear all filter --- "+"\n"
				+"Actual Minimum Value : "+minPriceOnClearAllFilter+"\n"
				+"Actual Maximum Value : "+maxPriceOnClearAllFilter);
		if(minPriceOnClearAllFilter == minPrice && maxPriceOnClearAllFilter == maxPrice)
			System.out.println("Working fine !");
		else
			System.out.println("Not working fine !");

	}

	public boolean isPriceRangeFilterDisplayedOnUI() {
		return (getWebElement(locator_priceRangeFilterSlider,true)!=null?
				getWebElement(locator_priceRangeSliderMinHandle,true)!=null?
						getWebElement(locator_priceRangeSliderMaxHandle,true)!=null?
								true:false:false:false);
	}

	public int getMinimumPriceFromPriceRangeFilter() {
		return Integer.parseInt(getWebElement(locator_priceRangeSliderDiv, true)
				.getAttribute("aria-valuemin").trim().replaceAll(",", ""));
	}

	public int getMaximumPriceFromPriceRangeFilter() {
		return Integer.parseInt(getWebElement(locator_priceRangeSliderDiv, true)
				.getAttribute("aria-valuemax").trim().replaceAll(",", ""));
	}

	public boolean isAllPricesOnGridInRange(int minPrice, int maxPrice ) throws NoProductFoundException   {
		List<Integer> prices = getListOfPricesOnGrid();
		boolean arePricesWithInRange = true;
		for(Integer currentItemPrice : prices) {
			if(currentItemPrice<= maxPrice && currentItemPrice>= minPrice)
				continue;
			else {
				arePricesWithInRange = false;
				break;
			}
		}
		return arePricesWithInRange;
	}
	
	public int getPriceWherePriceFilterFailed(int minPrice,int maxPrice) throws NoProductFoundException {
		int failureResult=0;
		List<Integer> prices = getListOfPricesOnGrid();
		for(Integer currentItemPrice : prices) {
			if(currentItemPrice<= maxPrice && currentItemPrice>= minPrice)
				continue;
			else {
				failureResult = currentItemPrice;
				break;
			}
		}
		return failureResult;
	}

	public List<Integer> setRandomPriceRange() throws NoProductFoundException  {
		int minPrice = getMinimumPriceFromPriceRangeFilter();
		int maxPrice = getMaximumPriceFromPriceRangeFilter();
		int randomMinPrice,randomMaxPrice;
		int randomPriceSettingCounter = 0;
		boolean wasNoProductFoundForFilter=false;
		do {
			randomMinPrice = gm.generateRandomNumber(minPrice, (minPrice+maxPrice)/2);
			randomMaxPrice = gm.generateRandomNumber((minPrice+maxPrice)/2, maxPrice);
			System.out.println("Setting price range with random minimum and maximum price"+"\n"
					+"Minimum Random Price : "+randomMinPrice+"\n"
					+"Maximum Random Price : "+randomMaxPrice);
			setPriceRange(randomMinPrice, randomMaxPrice);
			if(isNoProductFoundOnGrid()) {
				System.out.println("Current price range didn't fetch any result. Trying with another random price filter.");
				wasNoProductFoundForFilter=true;
				DriverManager.getCurrentWebDriver().navigate().back();
				waitForPageToLoad();
				SleepUtils.waitForShortTime();
			}else
				wasNoProductFoundForFilter=false;
			randomPriceSettingCounter++;
			
		}while(wasNoProductFoundForFilter && randomPriceSettingCounter<=5);
		if(isNoProductFoundOnGrid())
			return null;
		else
			return new ArrayList<Integer>(Arrays.asList(randomMinPrice,randomMaxPrice));
	}

	public List<Integer> getListOfPricesOnGrid() throws NoProductFoundException  {
		List<WebElement> priceDivList = getWebElements(locator_pricesDivList);
		List<Integer> pricesList = new ArrayList<>();
		
		//If no product is found on the page, throw product not found exception
		if(priceDivList.size()==0)
			throw new NoProductFoundException();
		for(WebElement currentPriceDiv : priceDivList) {
			pricesList.add(Integer.parseInt(getInnerTextWithJavaScript(currentPriceDiv).replaceAll(",","")));
		}
		return pricesList;
	}

	public boolean isSortByDropDownPresentOnPage() {
		return getWebElement(locator_sortByDropDown,true)!=null?true:false;
	}

	public void clickSortByDropDownBox() {
		getWebElement(locator_sortByDropDown,true).click();
	}

	public void moveToSortByDropDownBox() {
		javascriptMouseOverOnElement( getWebElement(locator_sortByDropDown,true));
	}

	public List<String> getSortingOptionsList(){
		List<String> options = new ArrayList<>();
		clickSortByDropDownBox();
		List<WebElement> sortByElements = getWebElements(locator_sortingOptionsList);
		for(WebElement currentOption : sortByElements) {
			String currentOptionText = currentOption.getText().trim();
			options.add(currentOptionText);
		}
		return options;
	}
	
	/*public boolean isGridSorted(SortingOptionsGrid sortingOption) {
		List<Integer> pricesList = getListOfPricesOnGrid();
		List<Integer> temp = new ArrayList<Integer>(pricesList);
		boolean result = false; 
		if(sortingOption==SortingOptionsGrid.LOW_TO_HIGH_PRICE) {
			Collections.sort(temp);
			result = temp.equals(pricesList);
		}else if(sortingOption==SortingOptionsGrid.HIGH_TO_LOW_PRICE) {
			Collections.sort(temp, Collections.reverseOrder());
			result =  temp.equals(pricesList);
		}
		return result;
	}*/
	
	public boolean isGridSorted(SortingOptionsGrid sortingOption) throws NoProductFoundException   {
		return getProductPriceWhereSortingFailed(sortingOption)==0?true:false;
	}
	
	public int getProductPriceWhereSortingFailed(SortingOptionsGrid sortingOption) throws NoProductFoundException   {
		List<Integer> pricesList = getListOfPricesOnGrid();
		int failedIndex = getProductIndexWhereSortingFailed(sortingOption);
		if(failedIndex == Integer.MIN_VALUE)return 0;
		else return pricesList.get(failedIndex);
	}
	
	public int getProductIndexWhereSortingFailed(SortingOptionsGrid sortingOption) throws NoProductFoundException {
		int failedIndex=Integer.MIN_VALUE;
		List<Integer> pricesList = getListOfPricesOnGrid();
		int previousPrice;
		int tempIndex = 0;
		if(sortingOption==SortingOptionsGrid.LOW_TO_HIGH_PRICE) {
			previousPrice = Integer.MIN_VALUE;
			for(Integer currentItemPrice : pricesList) {
				if(currentItemPrice>=previousPrice) previousPrice = currentItemPrice;
				else {
					System.out.println("Sorting failed at price : "+currentItemPrice);
					failedIndex = tempIndex;
					break;
				}
				tempIndex++;
			}
		}else if(sortingOption==SortingOptionsGrid.HIGH_TO_LOW_PRICE) {
			previousPrice = Integer.MAX_VALUE;
			for(Integer currentItemPrice : pricesList) {
				if(currentItemPrice<=previousPrice) previousPrice = currentItemPrice;
				else {
					System.out.println("Sorting failed at price : "+currentItemPrice);
					failedIndex = tempIndex;
					break;
				}
				tempIndex++;
			}
		}
		return failedIndex;
	}
	
	public String getProductNameWhereSortingFailed(SortingOptionsGrid sortingOption) throws NoProductFoundException {
		List<String> productNamesList = getProductNamesListOnGrid();
		int failedIndex = getProductIndexWhereSortingFailed(sortingOption);
		if(failedIndex == Integer.MIN_VALUE) return null;
		else return productNamesList.get(failedIndex);
	}
	

	public void clickSortingOption(SortingOptionsGrid option) {

		moveToSortByDropDownBox();
		List<WebElement> sortByElements = getWebElements(locator_sortingOptionsList);
		String optionToMatch=null;
		if(option == SortingOptionsGrid.NEW) {
			optionToMatch = "New";
		}else if(option == SortingOptionsGrid.POPULAR) {
			optionToMatch = "Popular";
		}else if(option == SortingOptionsGrid.LOW_TO_HIGH_PRICE) {
			optionToMatch = "Low to High Price";
		}else if(option == SortingOptionsGrid.HIGH_TO_LOW_PRICE) {
			optionToMatch = "High to Low Price";
		}

		for(WebElement currentOption : sortByElements) {
			String currentOptionText = currentOption.getText().trim();
			if(currentOptionText.equalsIgnoreCase(optionToMatch)) {
				currentOption.click();
				waitForPageToLoad();
				SleepUtils.waitForMidTime();
				break;
			}
		}

	}
	
	public List<String> getProductNamesListOnGrid() throws NoProductFoundException  {
		//If no product is found on the page, throw no product found exception.
		if(isNoProductFoundOnGrid())
			throw new NoProductFoundException();
		List<String> productNamesList = new ArrayList<String>();
		List<WebElement> productsAnchorTagList = getWebElements(locator_allProductsAnchorElements);
		for(WebElement currentProduct : productsAnchorTagList)
			productNamesList.add(currentProduct.getAttribute("title").trim());
		return productNamesList;
	}
	
	public Map<String,Integer> getProductsNamePriceMap() throws NoProductFoundException  {
		Map<String,Integer> data = new LinkedHashMap<String,Integer>();
		List<String> productNamesList = getProductNamesListOnGrid();
		List<Integer> productPricesList = getListOfPricesOnGrid();
		for(int i=0; i<productNamesList.size(); i++)
			data.put(productNamesList.get(i), productPricesList.get(i));
		return data;
	}
	
	
	public int getTotalProductQtyDisplyedRightSideInAFilter(String filterName) {
		int productQty=0;
		List<WebElement> checkboxesList = getWebElements(By.xpath(""
				+ "//div[contains(text(),'"+filterName+"')]/ancestor::div[@class='_24le _1zvo']//input[@type='checkbox']"));
		
		List<WebElement> pricesListInFilter = getWebElements(By.xpath(""
				+ "//div[contains(text(),'"+filterName+"')]/ancestor::div[@class='_24le _1zvo']//span[@class='_2e1o']"));
		WebElement checkbox=null;
		for(int i=0; i<checkboxesList.size(); i++) {
			checkbox = checkboxesList.get(i);
			if(checkbox.isSelected()) {
				productQty = productQty+Integer.parseInt(pricesListInFilter.get(i).getText().trim());
			}
		}
		return productQty;
	}
	
	//Method to verify if any product is present or not
	public boolean isNoProductFoundOnGrid() {
		//if(getWebPageSource().contains("Sorry! Couldn’t find the product you were looking for"))
		if(isElementPresentOnPage(locator_noProductFoundMessage))
			return true;
		else
			return false;
	}
	
	public Map<String,Integer> getResponseCodeForProductsLink() throws IOException {
		Map<String,Integer> data = new ResponseCodeProvider().getResponseCodeForProductsLinkOnGrid(DriverManager.getCurrentWebDriver());
		return data;
	}
	
	public Map<String,Integer> getBrokenLinksOnPage() throws IOException{
		return new ResponseCodeProvider().getLinksWithResponseCodeExcluding(DriverManager.getCurrentWebDriver(),200);
	}
	
	public Map<String,Integer> getFilteredResponseCodeExcluding(Map<String,Integer> dataToFilter, List<Integer> responseCodesToExcludeFromMap){
		return new ResponseCodeProvider().getFilteredResultsWithExcludingResponseCode(dataToFilter, responseCodesToExcludeFromMap);
	}
	
	public Map<String,String> getFilterValueFormatStatus() {
		Map<String,String> data=new LinkedHashMap<String,String>();
		waitForPageToLoad();
		List<WebElement> attributeFilterList = getWebElements(locator_attributeFiltersList);
		
		for(WebElement currentFilter : attributeFilterList) {
			if(currentFilter.findElements(By.xpath(".//i[@class='iconPlus']")).size()>0) {
				currentFilter.click();
				SleepUtils.sleepForMilliseconds(500);
			}
			
			List<WebElement> optionsForCurrentFilter = currentFilter.findElements(By.xpath(".//div[@class='Gswd']"));
			String filterVal;
			for(WebElement option : optionsForCurrentFilter) {
				filterVal = ((String)executeJavascriptQuery("return arguments[0].textContent;", option)).trim();
				if(filterVal.isEmpty() || filterVal==null || filterVal.matches(ConfigReader.getValue("regexOptionsFormat"))) {
					data.put(filterVal,"Incorrect Format");
				}else
					data.put(filterVal,"Correct Format");
			}	
		}
		return data;
	}
	
	public Map<String,String> getIncorrectFilterValueFormatMap(){
		return (Map<String, String>) getFilterValueFormatStatus().entrySet().stream()
				.filter(x -> {
					if(x.getValue().equalsIgnoreCase("Incorrect Format"))return true;
					else return false;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	public boolean isNextPageAvailable() {
		if(getWebElement(locator_nextButton, true).isEnabled())
			return true;
		else
			return false;
	}
	
	public void goToNextPage() {
		WebElement nextPageButton = getWebElement(locator_nextButton, true);
		javascriptClickOnElement(nextPageButton);
		waitForPageToLoad();
		SleepUtils.waitForMidTime();
	}
	
	public boolean isProductPresentOnGrid() {
		if(isNoProductFoundOnGrid())
			return false;
		
		List<WebElement> productImagesList = getWebElements(locator_productImages);
		boolean result = true;
		if(productImagesList.size()==0)
			return false;
		else {
			for(WebElement currentImage : productImagesList) {
				if(! (currentImage.isDisplayed() && currentImage.isEnabled())){
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	private int[] getMinAndMaxPriceSetInPriceFilter() {
		return new int[] {
				Integer.parseInt(getWebElement(locator_leftSliderPriceRangeFilter, true)
						.getAttribute("aria-valuenow").trim()),
				Integer.parseInt(getWebElement(locator_rightSliderPriceRangeFilter,true)
						.getAttribute("aria-valuenow").trim())
		};
	}
	
	public boolean isPriceRangeSetInFilter(int expectedMinVal, int expectedMaxVal) {
		int currentMinValInFilter = getMinAndMaxPriceSetInPriceFilter()[0];
		int currentMaxValInFilter = getMinAndMaxPriceSetInPriceFilter()[1];
		System.out.println("Current min and max vlaue set in filter is : "
								+currentMinValInFilter+" , "+currentMaxValInFilter);
		
		if(currentMinValInFilter==expectedMinVal && currentMaxValInFilter==expectedMaxVal)
			return true;
		else 
			return false;
	}
	
	//Methods related to add to cart
	public void addProductToCartByPlusIcon(int productIndex) {
		getWebElement(By.xpath("(//i[@class='iconPlus']/parent::div)"+"["+productIndex+"]"),false).click();  
	}
	
	public void addProductToCartByPlusIcon(String productName) {
		WebElement  addToCartPlusIcon = (getWebElement(By.xpath(""
						+ "//a[contains(@title,'"+productName+"')]//i[@class='iconPlus']/parent::div"
								+ "[not(parent::div[@class='_1v0Y'])]"),true));
		
		//addToCartPlusIcon.click();
		javascriptClickOnElement(addToCartPlusIcon);
		waitForElementAbsence(locator_cartDivForFMCGProducts);
	}
	
	public double getCartValue() {
		if(!isElementPresentOnPage(locator_cartValueDiv)) {
			return 0;
		}
		else {
			WebElement cartDiv = getWebElement(locator_cartValueDiv,true);
			return Double.parseDouble(cartDiv.getText().replaceAll("Rs","").replaceAll(",","").trim());
		}
	}
	
	public void mouseHoverOnCartIcon() {
		mouseOverOnElement(locator_cartIcon);
	}
	
	public boolean isAddToCartPlusIconAvailableForProduct(String productName) {
		return isElementPresentOnPage(By.xpath(""
				+ "//a[contains(@title,\""+productName+"\")]//i[@class='iconPlus']"));
	}
	
	public boolean isCartEmpty() {
		if(getCartValue()==0)
			return true;
		else 
			return false;
	}
	
	public boolean removeAllProductsFromCart() {
		if(getCartValue()>0) {
			mouseHoverOnCartIcon();
			while(isElementPresentOnPage(locator_removeProductFromCart)) {
				getWebElement(locator_removeProductFromCart, true).click();
				SleepUtils.sleepForMilliseconds(2000);
			}
			
			if(getCartValue()==0)
				return true;
			else
				return false;
		}else {
			return true;
		}
		
	}
	
	public TrackerResponse enterPincodeAndGetTrackerResponse(String pincode, String filterValue) throws Exception {
		getWebElement(locator_enterPincodeLink, true).click();
		SleepUtils.sleepForMilliseconds(500);
		getWebElement(locator_enterPincodeTextbox, true).sendKeys(pincode);
		TrackerResponse pincodeTrackerResponse = 
				new ActionTracker(filterValue).trackClickOnTheElement(
						getWebElement(locator_pincodeCheck,true),10);
		return pincodeTrackerResponse;
	}
	
	public TrackerResponse changePincodeAndGetTrackerResponse(String pincode, String filterValue) throws Exception{
		getWebElement(locator_pinocdeChange,true).click();
		SleepUtils.sleepForMilliseconds(500);
		getWebElement(locator_enterPincodeTextbox, true).sendKeys(pincode);
		TrackerResponse pincodeTrackerResponse = 
				new ActionTracker(filterValue).trackClickOnTheElement(
						getWebElement(locator_pincodeCheck,true),10);
		return pincodeTrackerResponse;
	}
	
	public void changePincode(String pincode) {
		getWebElement(locator_pinocdeChange,true).click();
		SleepUtils.sleepForMilliseconds(500);
		getWebElement(locator_enterPincodeTextbox, true).sendKeys(pincode);
		getWebElement(locator_pincodeCheck,true).click();
	}
	
	public boolean isServiceabilityOrderCorrect(List<Product> productList) {
		boolean result = true;
		List<Integer> serviceabilityList = productList.stream()
												.map(e->e.getServiceability_enabled())
												.collect(Collectors.toList());
		if(serviceabilityList.stream().distinct().limit(2).count()<=1)
			result = true;
		else {
			if(serviceabilityList.indexOf(0)!= -1) {
				if(serviceabilityList.lastIndexOf(1)<serviceabilityList.indexOf(0)) {
					if(serviceabilityList.indexOf(2)!=-1) {
						if(serviceabilityList.lastIndexOf(0) < serviceabilityList.indexOf(2)) 
							result = true;
						else 
							result = false;
					}
				}else 
					result = false;
			}else if(serviceabilityList.indexOf(2)!=-1) {
				if(serviceabilityList.lastIndexOf(1) < serviceabilityList.indexOf(2))
					result = true;
				else
					result = false;
			}
		}
		return result;
	}
	
	public boolean isAllProductsServiceable(List<Product> productList) {
		List<Integer> serviceabilityList = productList.stream()
				.map(e->e.getServiceability_enabled())
				.collect(Collectors.toList());
		return serviceabilityList.parallelStream().allMatch(v -> v==1);
	}
	
}
