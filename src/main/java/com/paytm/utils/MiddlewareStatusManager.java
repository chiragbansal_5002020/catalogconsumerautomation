package com.paytm.utils;

public class MiddlewareStatusManager {
	private static boolean isMiddlewareOn=false;
	static {
		if(System.getProperty("isMiddlewareOn")!=null) {
			String middlewareSetting = System.getProperty("isMiddlewareOn").trim().toUpperCase();
			if(middlewareSetting.equals("YES") || middlewareSetting.equals("TRUE"))
				isMiddlewareOn=true;
		}
	}
	public static boolean isMiddlewareStatusOn() {
		return isMiddlewareOn;
	}
	
	public static void main(String[] args) {
		System.out.println(MiddlewareStatusManager.isMiddlewareStatusOn());
	}
}
