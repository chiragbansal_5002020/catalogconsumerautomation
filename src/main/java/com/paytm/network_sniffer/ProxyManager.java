package com.paytm.network_sniffer;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Proxy;
import org.testng.Assert;

import com.paytm.utils.LocalPortFinder;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;

public class ProxyManager {

	private static ProxyManager proxyManager;
	//private static BrowserMobProxy proxyServer;
	//private static Proxy seleniumProxy;
	private static Map<Long,BrowserMobProxy> proxyServerMap = new HashMap<>(); 
	
	private ProxyManager() {
		
	}
	
	public static ProxyManager getInstance() {
		if(proxyManager==null) {
			proxyManager = new ProxyManager();
		}
		return proxyManager;
	}
	
	public synchronized BrowserMobProxy getProxyServer() {
		BrowserMobProxy proxyServer = proxyServerMap.get(Thread.currentThread().getId());
		if(proxyServer==null) {
			proxyServer = new BrowserMobProxyServer();
			proxyServer.setTrustAllServers(true); 
			int portNumber = LocalPortFinder.findFreePort();
			proxyServer.start(portNumber);
			if(proxyServer.isStarted()) {
				System.out.println("Started Proxy Server Successfully at port number : "+portNumber);
				proxyServerMap.put(Thread.currentThread().getId(), proxyServer);
			}else 
				System.out.println("Some problem occured while starting proxy server !!");
		}
		return proxyServer;
	}
	
	
	public synchronized Proxy getSeleniumProxy() {
		//if(seleniumProxy==null) {
			Proxy seleniumProxy;
			BrowserMobProxy proxyServer  = getProxyServer();
			seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
		    try {
		    String hostIp = Inet4Address.getLocalHost().getHostAddress();
		    seleniumProxy.setHttpProxy(hostIp+":"+ proxyServer.getPort());
		    seleniumProxy.setSslProxy(hostIp+":" + proxyServer.getPort());
		    } catch (UnknownHostException e) {
		        e.printStackTrace();
		        Assert.fail("invalid Host Address");
		     }
		//}
		
		return seleniumProxy;
	}
	
	public void abortProxyServer() {
		if(proxyServerMap.get(Thread.currentThread().getId())!=null)
			proxyServerMap.get(Thread.currentThread().getId()).abort();
	}
}
