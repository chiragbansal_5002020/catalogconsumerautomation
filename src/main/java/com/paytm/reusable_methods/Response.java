package com.paytm.reusable_methods;

public class Response {
	int responseCode;
	String errorMsg="";
	boolean isError=false;
	
	public Response(int responseCode) {
		this.responseCode=responseCode;
	}
	
	public Response(int responseCode, String errorMsg) {
		this.responseCode = responseCode;
		this.errorMsg = errorMsg;
		if(errorMsg.isEmpty() || errorMsg==null)
			isError=false;
		else
			isError=true;
	}
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
		if(errorMsg.isEmpty() || errorMsg==null) isError=false;
		else isError=true;
	}
	public boolean getIsError() {
		return isError;
	}
}	
