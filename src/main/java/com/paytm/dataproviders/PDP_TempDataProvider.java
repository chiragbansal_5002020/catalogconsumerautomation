package com.paytm.dataproviders;

import org.testng.annotations.DataProvider;

public class PDP_TempDataProvider {
	
	@DataProvider(name="PDP_TemporaryDataProvider")
	public static Object[][] getDataProvider() {
		return new Object[][] {
			{"https://paytmmall.com/red-tape-men-midnight-blue-cotton-polyblend-full-sleeve-t-shirt-CMPLXAPPRED-TAPE-MENRED-2509795ABB76B7-pdp?product_id=210764881&channel=WEB&discoverability=online&src=grid&svc=2&tracker=%7C%7C%7C%7C%2Fg%2Fmen%2Fclothes-all%2Ft-shirts-glpid-5030%7C5030%7C1%7C%7C%7C%7C"},
			{"https://paytmmall.com/hubberholme-regular-track-pants-CMPLXAPPHUBBERHOLME-HUBB52032728444448-pdp?product_id=163681856&channel=WEB&discoverability=online&src=grid&svc=2&tracker=%7C%7C%7C%7C%2Fg%2Fmen%2Fclothes-all%2Fsportswear%2Ftrack-pants-glpid-5930%7C5930%7C1%7C%7C%7C%7C"},
			{"https://paytmmall.com/rg-designers-blue-cotton-kurta-pyjamas-CMPLXAPPRG-DESIGNERSRAJ-14891191CE212-pdp?product_id=30553705&channel=WEB&discoverability=online&src=grid&svc=2&tracker=%7C%7C%7C%7C%2Fg%2Fmen%2Fclothes-all-glpid-5029%7C5029%7C2%7C%7C%7C%7C"}	
			};
		}
	}	
