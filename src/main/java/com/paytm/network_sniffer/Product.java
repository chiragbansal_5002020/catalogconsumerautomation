package com.paytm.network_sniffer;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Product {
	String product_id;
	String name;
	int add_to_cart;
	double actual_price;
	double offer_price;
	int serviceability_enabled;
	int discoverability;
	
	
	public Product() {
		super();
		this.product_id = "NOT_FOUND";
		this.name = "NOT_FOUND";
		this.add_to_cart = Integer.MIN_VALUE;
		this.actual_price = Integer.MIN_VALUE;
		this.offer_price = Integer.MIN_VALUE;
		this.serviceability_enabled = Integer.MIN_VALUE;
		this.discoverability = Integer.MIN_VALUE;
	}
	public Product(String product_id, String product_name, int add_to_cart, double actual_price,
			double effective_price,int serviceability_enabled,int discoverability) {
		super();
		this.product_id = product_id;
		this.name = product_name;
		this.add_to_cart = add_to_cart;
		this.actual_price = actual_price;
		this.offer_price = effective_price;
		this.serviceability_enabled = serviceability_enabled;
		this.discoverability = discoverability;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return name;
	}
	public void setProduct_name(String product_name) {
		this.name = product_name;
	}
	public int getAdd_to_cart() {
		return add_to_cart;
	}
	public void setAdd_to_cart(int add_to_cart) {
		this.add_to_cart = add_to_cart;
	}
	public double getActual_price() {
		return actual_price;
	}
	public void setActual_price(double actual_price) {
		this.actual_price = actual_price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getEffective_price() {
		return offer_price;
	}
	public void setEffective_price(double effective_price) {
		offer_price = effective_price;
	}
	public double getOffer_price() {
		return offer_price;
	}
	public void setOffer_price(double offer_price) {
		this.offer_price = offer_price;
	}
	public int getServiceability_enabled() {
		return serviceability_enabled;
	}
	public void setServiceability_enabled(int serviceability_enabled) {
		this.serviceability_enabled = serviceability_enabled;
	}
	public int getDiscoverability() {
		return discoverability;
	}
	public void setDiscoverability(int discoverability) {
		this.discoverability = discoverability;
	}
	@Override
	public String toString() {
		return "\n=============================="
				+"\nProduct Name - "+this.name
				+"\nProduct ID - "+this.product_id
				+"\nAdd To Cart - "+this.add_to_cart
				+"\nActual Price - "+this.actual_price
				+"\nEffective Price - "+this.offer_price
				+"\nServiceability enabled - "+this.serviceability_enabled
				+"\nDiscoverability - "+this.discoverability
				+"\n============================";
	}
	
	public static void main(String[] args) {
		List<String> fields = Arrays.asList(
				Product.class.getDeclaredFields()).stream()
				.map(Field::getName)
				.collect(Collectors.toList());
		
		System.out.println(fields);
		
	}
	
}
