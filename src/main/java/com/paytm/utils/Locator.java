package com.paytm.utils;

public class Locator {
	private String value;
	private LocatorType type;
	
	public Locator(LocatorType type, String value) {
		this.type = type;
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public LocatorType getType() {
		return type;
	}
	public void setType(LocatorType type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "Locator Type -> "+type.toString()+" , "+"Locator Value -> "+value;
	}
	
}
