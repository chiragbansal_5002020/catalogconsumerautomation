package com.paytm.page_classes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.paytm.utils.DriverManager;
import com.paytm.utils.FrameworkExceptionStack;
import com.paytm.utils.SleepUtils;
import com.paytm.utils.WebdriverUtils;

/*
 * SortingOrder enum
 * 	S_M_L     -> If the sorting order of product is like   ... XS, S, M, L, XL ...
 * 	Non_S_M_L -> All sorting except S_M_L like   ....28,30,32,...  OR ...80cm, 85cm, 90cm ... etc
 */
enum SortingOrder{
	S_M_L, NON_S_M_L
}

public class PDP_Page extends WebdriverUtils{

	By locator_selectOfferDiv = By.xpath("//div[text()='Select Offer']");
	By locator_moreOffersAvailable = By.xpath(""
			+ "//span[text()='offers']/ancestor::span/preceding-sibling::span//i[@class='iconArrowRight']");
	By locator_moreSellersRightArrow = By.xpath("//span[contains(text(),'Also available by')] /following-sibling::i[@class='iconArrowRight']");
	By locator_moreSellersSpan = By.xpath("//span[contains(text(),'Also available by')] ");
	By locator_changeSellerHeader = By.xpath("//div[text()='Change Seller']");
	By locator_OtherSellerList= By.xpath("//div[@class='e_Lw']");
	By locator_authorizedSellerList = By.xpath("//div[@class='e_Lw']/following::img[contains(@src,'seller/authorized')]");
	By locator_filterIconauthorizedSeller = By.xpath("//i[@class='iconFilter']");
	By locator_brandAuthroisedSellerCheckbox = By.xpath("//span[@class='labelName' and text()='Brand authorized Seller']/preceding-sibling::span[@class='check']");
	By locator_closeIconOtherSellersDiv = By.xpath("//i[@class='iconCross']");
	By locator_loadingMoreSellers = By.xpath("//div[contains(text(),'Loading more sellers...')]");
	By locator_ohterSellerNameInSellerList = By.xpath("//div[text()='Select']/preceding::div[@class='e_Lw']");
	By locator_sortSellersByPrice = By.xpath("//span[text()='Price']/parent::div");
	By locator_sortSellersByDeliveryTime = By.xpath("//span[text()='Delivery Time']/parent::div");
	By locator_listOfPricesByOtherSellers = By.xpath("//div[contains(@class,'_3vvk')]");
	By locator_listOfDeliverDateByOtherSellers = By.xpath("//span[contains(text(),'Delivery by') and count(preceding-sibling::i)=0]");
	By locator_selectOtherSeller = By.xpath("//div[text()='Select']");
	By locator_buyButton = By.xpath("//span[contains(text(),'Buy for')]/ancestor::button");
	By locator_sizes = By.xpath("//div[text()='Select Size']/../..//a");
	By locator_exchangeOfferLink = By.xpath("//span[text()='Exchange Offer']");
	By locator_exchangeOfferHeading = By.xpath("//i[@class='iconCross' and //i[contains(@class,'iconExchange')]/following::span[text()='Exchange Offer']]");
	By locator_loadingExchangeOffers = By.xpath("//div[contains(text(),'Loading exchange offers...')]");
	By locator_selectBrandSelectionBox = By.xpath("//option[text()='Select Brand']/parent::select");
	By locator_selectModelSelectionBox = By.xpath("//option[text()='Select Model']/parent::select");
	By locator_IMEI_TextBox = By.xpath("//input[@name='Enter IMEI Number']");
	By locator_verifyIMEINumber = By.xpath("//span[text()='Verify']");
	By locator_checkboxTnC = By.xpath("//input[@id='ExchangeTnc']");
	By locator_checkboxTncClickableSpan = By.xpath("//label[@for='ExchangeTnc']/span[@class='check']");
	By locator_done = By.xpath("//div[text()='Done']");
	By locator_exchangeCashbackDiv = By.xpath("//div[text()='Exchange Cashback']/following-sibling::div");
	By locator_exchangeOfferAppliedText = By.xpath("//*[text()='Exchange offer Applied']");
	By locator_exchangeOfferAppliedRupees = By.xpath("//*[text()='Exchange offer Applied']/following-sibling::span");
	By locator_exchangingProductName = By.xpath("//span[text()='Exchanging']/following-sibling::span");
	By locator_attributeDiv = By.xpath("//div[@class='FqsW']");
	By locator_attributeListClosed = By.xpath("//div[@class='_28Ki']");
	By locator_attributeListOpened = By.xpath("//div[@class='_28Ki _1Qt3']");
	By locator_similarProductsHeader = By.xpath("//*[text()='Similar Products']");
	By locator_similarProductsLink = By.xpath("//h2[text()='Similar Products']/ancestor::div[2]/following-sibling::div//a");
	By locator_similarProductsImages = By.xpath("//h2[text()='Similar Products']/ancestor::div[2]/following-sibling::div//img");
	By locator_similarProductsMore_RightArrow = By.xpath("//div[@class='_1JVU _3pq_']");
	By locator_goUpInPageArrow = By.xpath("//div[@class='_2GMD']");
	By locator_sizeChartLink = By.xpath("//span[text()='Size Chart']");
	By locator_sizeChartDiv = By.xpath("//div[text()='Size Chart']");
	By locator_sizeChartCm = By.xpath("//div[text()='cm']");
	By locator_sizeChartInch = By.xpath("//div[text()='inch']");
	By locator_sizesInSizeChart = By.xpath("//div[@class='_2nSj' and position()!=1]/div[1]");
	By locator_sizeChartIframe = By.xpath("//iframe[contains(@src,'sizechart')]");
	By locator_badgeBrandAuthorized = By.xpath("//span[text()='Brand Certified Seller']");
	
	public final SortingOrder order_small_medium_large = SortingOrder.S_M_L;
	public final SortingOrder order_non_small_medium_large = SortingOrder.NON_S_M_L;

	public boolean isOffersAvailableOnPage() {
		return WebdriverUtils.isElementPresentOnPage(locator_selectOfferDiv);
	}

	public boolean isAllOffersAvailableOnPage(List<String> offersList) {

		if(getListOfOffersNotAvailableOnPage(offersList).size()==0)
			return true;
		else
			return false;
	}

	public List<String> getListOfOffersNotAvailableOnPage(List<String> expectedOffersList){
		WebdriverUtils.waitForPageToLoad();
		List<String> offersNotAvailableOnPage = new ArrayList<String>();
		if(WebdriverUtils.isElementPresentOnPage(locator_moreOffersAvailable,true)) {
			WebdriverUtils.getWebElement(locator_moreOffersAvailable, false).click();
			SleepUtils.sleepForMilliseconds(500);
		}
		for(String offer : expectedOffersList) {
			if(!WebdriverUtils.isElementPresentOnPage(By.xpath(""
					+ "//div[text()='Select Offer']/parent::div//span[text()='"+offer+"']"),
					true)) {
				offersNotAvailableOnPage.add(offer);
			}
		}
		return offersNotAvailableOnPage;
	}

	public List<String> getListOfAdditionalOffersNotAvailableOnPage(List<String> additionalOffersList){
		WebdriverUtils.waitForPageToLoad();
		List<String> additionalOffersNotAvailableOnPage = new ArrayList<String>();
		for(String additionalOffer : additionalOffersList) {
			if(!WebdriverUtils.isElementPresentOnPage(By.xpath(""
					+ "//span[text()='T&C Apply']/preceding-sibling::span[contains("
					+ "translate(text(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'),'"+additionalOffer.toUpperCase()+"')]"),
					true))
				additionalOffersNotAvailableOnPage.add(additionalOffer);
		}
		return additionalOffersNotAvailableOnPage;
	}

	public boolean isAllAdditionalOffersAvailableOnPage(List<String> additionalOffersList) {
		if(getListOfAdditionalOffersNotAvailableOnPage(additionalOffersList).size()==0)
			return true;
		else
			return false;
	}

	public boolean isMoreSellersAvialable() {
		return WebdriverUtils.isElementPresentOnPage(locator_moreSellersRightArrow,true);
	}

	public boolean isAllOtherSellersAvaialbleOnPage(List<String> listOfOtherSellers) {
		if(getListOfSellersNotAvailableOnPage(listOfOtherSellers).size()==0)
			return true;
		else
			return false;
	}

	public List<String> getListOfSellersNotAvailableOnPage(List<String> listOfOtherSellers){
		listOfOtherSellers.replaceAll(String::toUpperCase);
		if(!WebdriverUtils.isElementPresentOnPage(locator_changeSellerHeader)) {
			WebdriverUtils.getWebElement(locator_moreSellersRightArrow, true).click();
			SleepUtils.sleepForMilliseconds(500);
		}
		List<WebElement> otherSellersDivList = WebdriverUtils.getWebElements(locator_OtherSellerList);
		List<String> listOfOtherSellersNotPresentOnUI = new ArrayList<String>();
		for(WebElement sellerDiv : otherSellersDivList) {
			if(!listOfOtherSellers.contains(sellerDiv.getText().trim().toUpperCase())) {
				listOfOtherSellersNotPresentOnUI.add(sellerDiv.getText().trim());
			}
		}
		//if(WebdriverUtils.isElementPresentOnPage(locator))
		return listOfOtherSellersNotPresentOnUI;
	}

	public int getNumberOfOtherSellers() {
		if(!isMoreSellersAvialable())
			return 0;
		else {
			String moreSellersText = getInnerTextWithJavaScript(getWebElement(locator_moreSellersSpan, true));
			Pattern p = Pattern.compile("-?\\d+");
			Matcher m = p.matcher(moreSellersText);
			m.find();
			return Integer.parseInt(m.group());
		}
	}

	public boolean verifyOtherSellersDisplayedOnUI(int expectedNumberOfSellers) {
		if(expectedNumberOfSellers==0) {
			if(!isMoreSellersAvialable())
				return true;
			else 
				return false;
		}

		openOtherSellersListIfNotAlreadyOpen();

		List<WebElement> otherSellersDivList = WebdriverUtils.getWebElements(locator_OtherSellerList);
		if(otherSellersDivList.size()==expectedNumberOfSellers) {
			return true;
		}else
			return false;
	}

	public String selectAndGetOtherSeller() {
		openOtherSellersListIfNotAlreadyOpen();
		String otherSellerBeingSelected = getWebElement(locator_ohterSellerNameInSellerList,true).getText();
		getWebElement(locator_selectOtherSeller, true).click();
		SleepUtils.sleepForMilliseconds(500);
		return otherSellerBeingSelected;
	}

	public boolean isCurrentSelectedSeller(String sellerNameToCheck) {
		return isElementPresentOnPage(By.xpath("//a[@title=\""+sellerNameToCheck+"\"]"),true);
	}

	public boolean isBuyButtonEnabled() {
		return (isElementPresentOnPage(locator_buyButton, true)?
				getWebElement(locator_buyButton,false).isEnabled()?true:false:false);
	}

	public void openOtherSellersListIfNotAlreadyOpen() {
		if(!WebdriverUtils.isElementPresentOnPage(locator_changeSellerHeader,false)) {
			WebElement moreSellersLink = WebdriverUtils.getWebElement(locator_moreSellersRightArrow, true);
			scrollToElement(moreSellersLink);
			waitForElementToBeClickable(moreSellersLink);
			javascriptClickOnElement(moreSellersLink);
			SleepUtils.sleepForMilliseconds(500);
		}
		waitForElementAbsence(locator_loadingMoreSellers);
	}

	public List<String> getSizeList() {
		List<WebElement> sizesElementList = getWebElements(locator_sizes);
		List<String> sizesList = new ArrayList<String>();
		for(WebElement sizeElement : sizesElementList) {
			sizesList.add(sizeElement.getText().trim());
		}
		return sizesList;
	}

	private static List<Double> processSizeList(List<String> sizes, SortingOrder order){
		sizes.replaceAll(String::toUpperCase);
		
		List<Double> convertedSizeList = new ArrayList<>();
		
		if(order==SortingOrder.S_M_L) {
			for(String size: sizes) {
				size = size.replaceAll(" +","");
				int sizeValue=0;

				switch(size.charAt(size.length()-1)){
				case 'L':
					sizeValue+=1;
					break;
				case 'M':
					sizeValue+=0;
					break;
				case 'S':
					sizeValue+=-1;
					break;
				default:
					System.out.println("Invalid character in size- "+size.charAt(size.length()-1));
				}
				
				if(size.length()>1) {
					int counterForX = 0;
					size = size.substring(0, size.length()-1);
					if(Character.isDigit(size.charAt(0))){
						counterForX+=Integer.parseInt(size.charAt(0)+"")-1;
						size=size.substring(1);
					}
					counterForX+=size.length();
					sizeValue+=(counterForX*sizeValue);
				}
				convertedSizeList.add((double) sizeValue);
			}
			
		}else if(order == SortingOrder.NON_S_M_L) {
			Pattern p = Pattern.compile("(-?[0-9]+(?:[,.][0-9]+)?)");
			for(String size : sizes) {
				size=size.replaceAll(" +", "");
				Matcher m = p.matcher(size);
				if(m.find())
					convertedSizeList.add(Double.parseDouble(m.group()));
			}
		}
		
		return convertedSizeList;
	}

	public boolean isListSorted(List<Double> list) {
		List<Double> tmp = new ArrayList<>(list);
		Collections.sort(tmp);
		return tmp.equals(list);
	}
	
	public boolean isSortOrderSizesCorrect() {
		List<String> sizes = getSizeList();
		return isSortOrderSizesCorrect(sizes);
	}
	
	public boolean isSortOrderSizesCorrect(List<String> sizes) {
		SortingOrder expectedSortingOrder = getSortingOrderForSizeOfProduct(sizes);
		if(sizes.size()==1) 
			return true;
		List<Double> convertedSizesList = processSizeList(sizes,expectedSortingOrder);
		return isListSorted(convertedSizesList);
	}
	
	public boolean isExchangeOfferDisplayed() {
		return isElementPresentOnPage(locator_exchangeOfferLink,true);
	}
	
	public void openExchangeOfferDiv() {
		if(!isElementPresentOnPage(locator_exchangeOfferHeading)) {
			javascriptClickOnElement(getWebElement(locator_exchangeOfferLink, true));
			SleepUtils.sleepForMilliseconds(1000);
		}
		waitForElementAbsence(locator_loadingExchangeOffers);
	}
	
	public int selectExchangeProductAndGetExchangeCashbackValue(String brand, String model, String IMEI_Number) {
		int exchangeCashback;
		selectItemFromSelectBox(locator_selectBrandSelectionBox, brand);
		selectItemFromSelectBox(locator_selectModelSelectionBox,model);
		waitForElementToBeClickable(locator_IMEI_TextBox);
		getWebElement(locator_IMEI_TextBox, true).sendKeys(IMEI_Number);
		waitForElementToBeClickable(locator_verifyIMEINumber);
		getWebElement(locator_verifyIMEINumber,true).click();
		if(!getWebElement(locator_checkboxTnC,false).isSelected())
			getWebElement(locator_checkboxTncClickableSpan,true).click();
		SleepUtils.sleepForMilliseconds(2000);
		exchangeCashback = Integer.parseInt(getInnerTextWithJavaScript(
				getWebElement(locator_exchangeCashbackDiv,true)).trim());
		SleepUtils.sleepForMilliseconds(1000);
		javascriptClickOnElement(locator_done);
		SleepUtils.sleepForMilliseconds(500);
		return exchangeCashback;
	}
	
	public boolean isExchangeOfferApplied() {
		return isElementPresentOnPage(locator_exchangeOfferAppliedText,true);
	}
	
	public int getExchangeOfferAppliedValue() {
		return Integer.parseInt(getInnerTextWithJavaScript(
				getWebElement(locator_exchangeOfferAppliedRupees,true)).trim().replaceAll(",",""));
	}
	
	public String getNameOfProductBeingExchanged() {
		return getWebElement(locator_exchangingProductName, true).getText().trim();
	}
	
	public Map<String,String> getAttributesMap(){
		Map<String,String> map = new LinkedHashMap<String, String>();
		
		List<WebElement> attributeDivArrowDownList = getWebElements(locator_attributeListClosed);
		for(WebElement attributeDivArrowDownElement : attributeDivArrowDownList) {
			if(attributeDivArrowDownElement.isDisplayed()) {
				attributeDivArrowDownElement.click();
			}
		}
		List<WebElement> attributeDivList = getWebElements(locator_attributeDiv);
		for(WebElement attributeDiv : attributeDivList) {
			if(attributeDiv.isDisplayed()) {
				map.put(attributeDiv.findElement(By.xpath("./div[1]")).getText().trim(), 
						attributeDiv.findElement(By.xpath("./div[2]")).getText().replaceAll("\n"," ").trim());
			}
		}
		return map;
	}
	
	public boolean isSimilarProductsDisplayed() {
		System.out.println("I reached here");
		boolean result = false;
		if(isElementPresentOnPage(locator_similarProductsHeader,true)) {
			scrollToElement(locator_similarProductsHeader, false);
			SleepUtils.sleepForMilliseconds(1000);
			try{
				isElementPresentOnPage(locator_similarProductsImages,true);
				result = true;
				}catch(Exception e) {result =  false;}
		}
		return result;
	}
	
	public List<String> getListOfSimilarProductsLink(){
		if(isElementPresentOnPage(locator_similarProductsMore_RightArrow,true)) {
			executeJavascriptQuery("arguments[0].scrollIntoView(false);", getWebElement(locator_similarProductsMore_RightArrow,false));
			SleepUtils.sleepForMilliseconds(500);
			if(isElementPresentOnPage(locator_goUpInPageArrow)) {
				//Sometimes this go to up section web element masks the similar products right arrow icon. 
				hide_goToTopArrowLink(getWebElement(locator_goUpInPageArrow,false));
			}
			SleepUtils.sleepForMilliseconds(500);
			while(isElementPresentOnPage(locator_similarProductsMore_RightArrow, true)) {
				getWebElement(locator_similarProductsMore_RightArrow,false).click();
			}
		}
		return getWebElements(locator_similarProductsLink).stream()
				.map(e->{return e.getAttribute("href").trim();})
				.collect(Collectors.toList());
	}
	
	private void hide_goToTopArrowLink(WebElement element) {
		executeJavascriptQuery("arguments[0].setAttribute('style','display:none;');",element);
	}
	
	private void show_goToTopArrowLink(WebElement element) {
		executeJavascriptQuery("arguments[0].setAttribute('style','display:block;');",element);
	}
	
	public boolean isSizeChartLinkDisplayed() {
		return isElementPresentOnPage(locator_sizeChartLink,true);
	}
	
	public void openSizeChart() {
		getWebElement(locator_sizeChartLink, true).click();
	}
	
	public boolean isSizeChartOpened() {
		return isElementPresentOnPage(locator_sizeChartDiv, true);
	}
	
	public boolean isSizeInCMDisplayedInSizeChart() {
		DriverManager.getCurrentWebDriver().switchTo().frame(getWebElement(locator_sizeChartIframe,false));
		boolean result =  isElementPresentOnPage(locator_sizeChartCm,true);
		DriverManager.getCurrentWebDriver().switchTo().defaultContent();
		return result;
	}
	
	public boolean isSizeInInchDisplayedInSizeChart() {
		DriverManager.getCurrentWebDriver().switchTo().frame(getWebElement(locator_sizeChartIframe,false));
		boolean result =  isElementPresentOnPage(locator_sizeChartInch, true);
		DriverManager.getCurrentWebDriver().switchTo().defaultContent();
		return result;
	}
	
	public List<String> getListOfSizesInSizeChart(){
		DriverManager.getCurrentWebDriver().switchTo().frame(getWebElement(locator_sizeChartIframe,false));
		List<String> result =  getWebElements(locator_sizesInSizeChart).stream()
				.map(e-> {
					return e.getText().trim();
				})
				.collect(Collectors.toList());
		DriverManager.getCurrentWebDriver().switchTo().defaultContent();
		return result;
	}
	
	public SortingOrder getSortingOrderForSizeOfProduct(List<String> listSizes) {
		return listSizes.stream().filter(e->{
			String ch = e.charAt(e.length()-1)+"";
			if(ch.equalsIgnoreCase("s") || ch.equalsIgnoreCase("m") || ch.equalsIgnoreCase("l")) {
				if(e.length()>1) {
					int index = e.length()-2;
					while(index>0 && e.charAt(index)==' ') {
						index++;
					}
					if(e.charAt(index)=='x' || e.charAt(index)=='X' || e.charAt(index)==' ')
						return true;
					else
						return false;
				}else
					return true;
			}
			else return false;
			}).count() == listSizes.size()?SortingOrder.S_M_L:SortingOrder.NON_S_M_L;
	}
	
	public void applyOffer(String offerName) {
		expandOffersIfMultipleOffersPresent();
		getWebElement(By.xpath("//span[contains(translate("
				+ "text(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'),'"+offerName.toUpperCase()+"')]"), true).click();
	}
	
	public boolean isOfferApplied(String offerName) {
		expandOffersIfMultipleOffersPresent();
		return isElementPresentOnPage(By.xpath("//span[contains(translate("
				  + "text(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'),'"+offerName.toUpperCase()+"')]"
				  + "/following-sibling::span[contains(text(),'Applied!')]"));
	}
	
	public void expandOffersIfMultipleOffersPresent() {
		if(isElementPresentOnPage(locator_moreOffersAvailable)) {
			getWebElement(locator_moreOffersAvailable, false).click();
			SleepUtils.sleepForMilliseconds(1000);
		}
	}
	
	public int getNumberOfOtherSellersOnChangeSellerPanel() {
		return getWebElements(locator_OtherSellerList).size();
	}
	
	public int getNumberOfauthorizedSellers() {
		return getWebElements(locator_authorizedSellerList).size();
	}
	
	public void applyauthorizedSellerFilterOnChangeSellerPanel() {
		getWebElement(locator_filterIconauthorizedSeller, true).click();
		getWebElement(locator_brandAuthroisedSellerCheckbox,true).click();
		SleepUtils.sleepForMilliseconds(1000);
	}
	
	public void applySortingByPriceOnChangeSellerPanel() {
		getWebElement(locator_sortSellersByPrice,true).click();
	}
	
	public List<Integer> getOtherSellersPriceList(){
		return getWebElements(locator_listOfPricesByOtherSellers).stream()
				.map(p->{return Integer.parseInt(
						p.getText().trim().replaceAll(",","").replaceAll(" +",""));
						})
				.collect(Collectors.toList());
	}
	
	public void applySortingByDeliveryDateInChangeSellerPanel() {
		getWebElement(locator_sortSellersByDeliveryTime,true).click();
	}
	
	public List<String> getOtherSellersDeliverDateList(){
		return getWebElements(locator_listOfDeliverDateByOtherSellers).stream()
				.map(p->{return p.getText().trim();})
				.collect(Collectors.toList());
	}
	
	public boolean isDeliverDateListSorted(List<String> deliveryDateList) {
		List<Date> deliveryDateDateObjectsList = deliveryDateList.stream()
											.map(s->{ return getJavaDateObjectFromDeliveryDateString(s);})
											.collect(Collectors.toList());
		List<Date> tempList = new ArrayList<Date>(deliveryDateDateObjectsList);
		Collections.sort(tempList);
		return deliveryDateDateObjectsList.equals(tempList);
	}
	
	private static Date getJavaDateObjectFromDeliveryDateString(String text) {
		text=text.replaceAll("Delivery[ ]*by", "").trim();
		if(text.equalsIgnoreCase("tomorrow")) {
			return new Date(new Date().getTime() + (24*60*60*1000));
		}
		
		Pattern p = Pattern.compile("([0-9]+).*");
		Matcher m = p.matcher(text);
		m.matches();
		int day = Integer.parseInt(m.group(1));
		String month = text.substring(text.length()-3);
		System.out.println("Day : "+day+"   ,month : "+month);
		Date d = null;
		try {
			d =  new SimpleDateFormat("dd-MMM-yyyy").parse(day+"-"+month+"-"+
																Calendar.getInstance().get(Calendar.YEAR));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d;
	}
	
	private boolean isBrandAuthorizedBadgeAvailable() {
		return isElementPresentOnPage(locator_badgeBrandAuthorized,true);
	}
	
	private boolean isExchangeBadgeAvailable() {
		return isElementPresentOnPage(locator_exchangeOfferLink,true);
	}
	
	public boolean isBadgeAvailable(String badgeName) {
		badgeName = badgeName.toUpperCase();
		if(badgeName.contains("BRAND") || badgeName.contains("AUTHORISE") || badgeName.contains("AUTHORIZE"))
			return isBrandAuthorizedBadgeAvailable();
		if(badgeName.contains("EXCHANGE") || badgeName.contains("OFFER"))
			return isExchangeBadgeAvailable();
		else
			FrameworkExceptionStack.getCurrentStack().add(new Exception(badgeName+" not handled in script"));
		return false;
	}
	
	public static void main(String[] args) throws ParseException {
		List<String> sizes = Arrays.asList("xXS","x  xx     Xs","S","M","L","XXXXL","3 XL","5XL");
		List<String> sizes2= Arrays.asList("1.20mtr", "1.34mtr  "," 3.22  Meter", "0.45m","1.2M");
		List<String> sizes3 = Arrays.asList("28\"","40\"","23\"","56\"");
		List<String> sizes4 = Arrays.asList("34","36","38","40");
		List<String> sizes5 = Arrays.asList("s");
		System.out.println(processSizeList(sizes,SortingOrder.S_M_L));
		System.out.println(processSizeList(sizes2, SortingOrder.NON_S_M_L));
		System.out.println(processSizeList(sizes3, SortingOrder.NON_S_M_L));
		System.out.println(processSizeList(sizes4, SortingOrder.NON_S_M_L));
	}
}
