package com.paytm.utils;

public enum LocatorType {
	XPATH,CSS_SELECTOR,NAME,LINKTEXT,PARTIAL_LINKTEXT,ID,TAG_NAME,CLASS_NAME
}
