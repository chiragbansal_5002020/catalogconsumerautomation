package com.paytm.test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.testng.ISuiteListener;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.paytm.dataproviders.PDP_TestDataPacket;
import com.paytm.page_classes.PDP_Page;
import com.paytm.reusable_methods.ResponseCodeProvider;
import com.paytm.reusable_methods.generic_methods;
import com.paytm.utils.DriverManager;
import com.paytm.utils.Reporter;
import com.paytm.utils.SleepUtils;
import com.paytm.utils.URL_Launcher;

public class PDP_TestCases extends Setup{

	public Reporter htmlReporter = new Reporter().getInstance(this.getClass(),"PDP_validations.html");
	PDP_Page pdp;
	generic_methods gm;
	
	@BeforeTest(alwaysRun=true)
	public void startReport() {	
		pdp = new PDP_Page();
		gm = new generic_methods();
	}
	
	@Test
	public void test1() {
		htmlReporter.logInfo("dummy pdp test 1");
		SleepUtils.sleepForMilliseconds(4500);
		htmlReporter.logPass("test passed");
	}
	
	@Test
	public void test2() {
		htmlReporter.logInfo("dummy pdp test 2");
		SleepUtils.sleepForMilliseconds(3300);
		htmlReporter.logPass("test passed");
	}
	
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class,
			priority=1
		 )
	public void verifyOffersOnPDP(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;
		htmlReporter.logInfo("Testing for Offers on product- "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		if(data.getOffersAvailable().size()>0) {
			htmlReporter.logInfo("Offers expected on PDP : "+
						gm.getRepresentableStringFromList(data.getOffersAvailable()));
			if(pdp.isAllOffersAvailableOnPage(data.getOffersAvailable())) {
				htmlReporter.logPass("All offers present on the PDP");
			}else {
				htmlReporter.logFailWithoutCapturingScreenshot("All offers not present on the PDP");
				htmlReporter.logInfo("Offers not present on UI - "+
						gm.getRepresentableStringFromList(pdp.getListOfOffersNotAvailableOnPage(data.getOffersAvailable())));
			}
			
			htmlReporter.logInfo("Applying each offer ... ");
			for(String offer : data.getOffersAvailable() ) {
				htmlReporter.logInfo("Applying offer - "+offer);
				pdp.applyOffer(offer);
				if(pdp.isOfferApplied(offer))
					htmlReporter.logPass("Offer applied successfully!");
				else {
					htmlReporter.logFail("Couldn't apply the offer!");
				}
			}
			
		}else {
			htmlReporter.logInfo("No Offer is expected on PDP");
			if(pdp.isOffersAvailableOnPage()) {
				htmlReporter.logPass("As expected, no offer present on UI");
			}else {
				htmlReporter.logFail("Offers present on UI");
			}
		}
		
		htmlReporter.captureScreenshot();
	}
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class,
			priority=1
		 )
	public void verifyAdditionalOffersOnPDP(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;
		htmlReporter.logInfo("Testing for Additional Offers on product- "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		List<String> additionalOffers = data.getAdditionalOffers();
		if(additionalOffers.size()>0) {
			htmlReporter.logInfo("Additional Offers expected on PDP : "+
								gm.getRepresentableStringFromList(additionalOffers));
			if(pdp.isAllAdditionalOffersAvailableOnPage(additionalOffers)) {
				htmlReporter.logPass("All additional offers present on the PDP");
			}else {
				htmlReporter.logFailWithoutCapturingScreenshot("All additional offers not present on the PDP");
				htmlReporter.logInfo("Additional Offers not present on UI - "+
						gm.getRepresentableStringFromList(pdp.getListOfAdditionalOffersNotAvailableOnPage(additionalOffers)));
			}
		}else {
			htmlReporter.logInfo("No Additional Offer is expected on PDP");
			if(pdp.isOffersAvailableOnPage()) {
				htmlReporter.logPass("As expected, no additional offer present on UI");
			}else {
				htmlReporter.logFail("Additional Offers present on UI");
			}
		}
		
		htmlReporter.captureScreenshot();
	}
	
	
	/*@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class,
			priority=1
		 )
	public void verifyMoreSellersOnPage_old(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;    
		htmlReporter.logInfo("Testing for Other Sellers for product- "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		List<String> otherSellers = data.getMoreSellerAvailable();
		if(otherSellers.size()>0) {
			htmlReporter.logInfo("Expected other sellers on the PDP- "
					+gm.getRepresentableStringFromList(otherSellers));
			if(pdp.isMoreSellersAvialable()) {
				if(pdp.isAllOtherSellersAvaialbleOnPage(otherSellers)) {
					htmlReporter.logPass("All the other sellers are available");
				}else {
					htmlReporter.logFail("Not all other selllers avaiable on UI");
					htmlReporter.logInfo("Sellers not available: "
								+gm.getRepresentableStringFromList(pdp.getListOfSellersNotAvailableOnPage(otherSellers)));
				}
			}else {
				htmlReporter.logInfo("No other sellers is displayed on UI");
			}
		}else {
			htmlReporter.logInfo("No other seller is expected on PDP");
			if(!pdp.isMoreSellersAvialable()) {
				htmlReporter.logPass("No other seller is available on page as expected !");
			}else {
				htmlReporter.logFail("Other sellers are available on page!");
			}
		}
		
		htmlReporter.captureScreenshot();
	}*/
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class,
			priority=1
		 )
	public void verifyMoreSellersOnPage(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;    
		htmlReporter.logInfo("Testing for Other Sellers for product- "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		int numberOfOtherSellersExpectedOnUI = pdp.getNumberOfOtherSellers();
		htmlReporter.logInfo("Expected number of other sellers on page : "+numberOfOtherSellersExpectedOnUI);
		if(numberOfOtherSellersExpectedOnUI>0) {
			pdp.openOtherSellersListIfNotAlreadyOpen();
			htmlReporter.captureScreenshot();
		}
		if(pdp.verifyOtherSellersDisplayedOnUI(numberOfOtherSellersExpectedOnUI)) {
			htmlReporter.logPass(numberOfOtherSellersExpectedOnUI==0?"No other seller available for product"
					:numberOfOtherSellersExpectedOnUI+" other seller available for product");
			if(numberOfOtherSellersExpectedOnUI>0) {
				
				//Test Sort By Price -- Starts
				htmlReporter.logInfo("Cheking for Price Sorting in Change Seller panel");
				List<Integer> priceListBeforeSorting = pdp.getOtherSellersPriceList();
				htmlReporter.logInfo("Price List before applying sorting: "+priceListBeforeSorting.toString());
				htmlReporter.logInfo("Applying soritng by price");
				pdp.applySortingByPriceOnChangeSellerPanel();
				List<Integer> priceListAfterSorting = pdp.getOtherSellersPriceList();
				htmlReporter.logInfo("Price list after applying sorting: "+priceListAfterSorting.toString());
				if(pdp.isListSorted(priceListAfterSorting.stream().map(Integer::doubleValue).collect(Collectors.toList()))) {
					htmlReporter.logPass("Price sorting working fine");
				}else {
					htmlReporter.logFail("Price sorting not working!");
				}
				//Test Sort By Price -- Ends
				
				//Test Sort By Delivery Time Starts
				htmlReporter.logInfo("Checking for Delivery Time Sorting in Change Seller panel");
				List<String> deliveryTimeListBeforeSorting = pdp.getOtherSellersDeliverDateList();
				htmlReporter.logInfo("Delivery Time list before applying sorting: "
								+gm.getRepresentableStringFromList(deliveryTimeListBeforeSorting));
				htmlReporter.logInfo("Applying sorting by Delivery Time ... ");
				pdp.applySortingByDeliveryDateInChangeSellerPanel();
				List<String> deliveryTimeListAfterSorting = pdp.getOtherSellersDeliverDateList();
				htmlReporter.logInfo("Delivery Time list After applying sorting: "
								+gm.getRepresentableStringFromList(deliveryTimeListAfterSorting));
				if(pdp.isDeliverDateListSorted(deliveryTimeListAfterSorting)) {
					htmlReporter.logPass("Sorting with delivery time working fine.");
				}else {
					htmlReporter.logFail("Sorting with delivery time not working!");
				}
				//Test Sort By Delivery Time Ends
				
				//Test Brand Authorized Sellers Filter -- Starts
				htmlReporter.logInfo("Checking for Brand authorized Filter");
				int numBrandauthorizedSellersOnList = pdp.getNumberOfauthorizedSellers();
				htmlReporter.logInfo("Number of brand authorized sellers before applying filter: "+numBrandauthorizedSellersOnList);
				if(numBrandauthorizedSellersOnList>0) {
					htmlReporter.logInfo("Appllying filter ... ");
					pdp.applyauthorizedSellerFilterOnChangeSellerPanel();
					int numberOfSellersAfterApplyingFilter = pdp.getNumberOfOtherSellersOnChangeSellerPanel();
					htmlReporter.logInfo("After applying filter, number of sellers on list: "+numberOfSellersAfterApplyingFilter);
					if(numBrandauthorizedSellersOnList == numberOfSellersAfterApplyingFilter) {
						htmlReporter.logPass("Brand authorized filter is working fine.");
					}else {
						htmlReporter.logFail("Brand authorized filter is not working.");
					}
				}else {
					htmlReporter.logInfo("Brand authorized filter can't be tested.");
				}
				//Test Brand Authorized Sellers Filter -- Ends
				
				htmlReporter.logInfo("checking if user is able to change to other seller !");
				String changingToSeller = pdp.selectAndGetOtherSeller();
				htmlReporter.logInfo("Selecting other seller - "+changingToSeller);
				if(pdp.isCurrentSelectedSeller(changingToSeller)) {
					htmlReporter.logPass("Successfully changed to other seller - "+changingToSeller);
					htmlReporter.logInfo("Checking for Buy button availability for seller");
					if(pdp.isBuyButtonEnabled()) {
						htmlReporter.logPass("Buy now button is enabled!");
					}else
						htmlReporter.logFail("Buy now button not enabled!");
				}else {
					htmlReporter.logFail("Couldn't change to seller - "+changingToSeller);
				}
			}
		}else {
			htmlReporter.logFailWithoutCapturingScreenshot("Expected and actual number of sellers not matched!");
		}
		htmlReporter.captureScreenshot();
	}
	
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class)
	public void testSortOrderOfSizes(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;    
		htmlReporter.logInfo("Verifing sort order of sizes on PDP- "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		List<String> sizeList = pdp.getSizeList();
		htmlReporter.logInfo("Sizes order present on UI- "+ sizeList);
		if(pdp.isSortOrderSizesCorrect(sizeList)) {
			htmlReporter.logPass("Sorting order of size is correct on UI");
		}else {
			htmlReporter.logFail("Sorting Order of Size is incorrect on UI");
		}
		htmlReporter.logInfo("Verfying size chart");
		if(pdp.isSizeChartLinkDisplayed()) {
			htmlReporter.logPass("Size Chart Link displayed for the product");
			pdp.openSizeChart();
			SleepUtils.sleepForMilliseconds(2000);
			htmlReporter.captureScreenshot();
			if(pdp.isSizeChartOpened()) {
				htmlReporter.logPass("Size Chart opened successfully!");
				if(pdp.isSizeInCMDisplayedInSizeChart()) {
					htmlReporter.logPass("Size in CM is displayed on size chart");
				}else {
					htmlReporter.logFail("Size in CM is not displayed on size chart");
				}
				
				if(pdp.isSizeInInchDisplayedInSizeChart()) {
					htmlReporter.logPass("Size in Inch displayed on size chart");
				}else {
					htmlReporter.logFail("Size in Inch not displayed on size chart");
				}
				
				htmlReporter.logInfo("Checking for sorting order of size in Size Chart");
				List<String> sizeListSizeChart = pdp.getListOfSizesInSizeChart();
				htmlReporter.logInfo("Order of size displayed in size chart : "+"\n"+
							gm.getRepresentableStringFromList(sizeListSizeChart));
				if(pdp.isSortOrderSizesCorrect(sizeListSizeChart)) {
					htmlReporter.logPass("Sorting is correct in size chart");
				}else {
					htmlReporter.logFail("Sorting is not correct in size chart.");
				}
			}else {
				htmlReporter.logFail("Size Chart not opened!");
			}
		}else {
			htmlReporter.logFail("Size Chart link not displayed for the product");
		}
		htmlReporter.captureScreenshot();
	}
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class)
	public void testExchangeOffer(Object object) {
		new DriverManager().restartBrowser();
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;    
		htmlReporter.logInfo("Testing for exchange offers on product- "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		if(pdp.isExchangeOfferDisplayed()) {
			htmlReporter.logPass("Exchange Offer displayed for product!");
			String exchangeProductInformation = data.getExchangeOfferAvailable();
			String[] parts = exchangeProductInformation.split("@");
			String brand = parts[0].trim();
			String model = parts[1].trim();
			String imeiNumber = parts[2].trim();
			htmlReporter.logInfo("Select Parameters for exchange product- "+"\n"
									+"Brand - "+brand+"\n"
									+"model - "+model+"\n"
									+"imeiNumber - "+imeiNumber);
			pdp.openExchangeOfferDiv();
			htmlReporter.captureScreenshot();
			int cashbackCalculated = pdp.selectExchangeProductAndGetExchangeCashbackValue(brand, model, imeiNumber);
			htmlReporter.logInfo("Cashback calculated after selecting product : "+cashbackCalculated);
			int actualCashbackDisplayedOnPDP = pdp.getExchangeOfferAppliedValue();
			htmlReporter.logInfo("Actual cashback displayed on PDP: "+actualCashbackDisplayedOnPDP);
			if(cashbackCalculated == actualCashbackDisplayedOnPDP) {
				htmlReporter.logPass("Cashback Displayed matches with cashbackCalculated");
			}else {
				htmlReporter.logFailWithoutCapturingScreenshot("Actual Cashback displayed differs from calculated Cashback");
			}
			
			String productNameBeingExchangedDisplayedOnPDP = pdp.getNameOfProductBeingExchanged();
			htmlReporter.logInfo("Product Name being exchanged displayed on PDP : "+ productNameBeingExchangedDisplayedOnPDP);
			if(productNameBeingExchangedDisplayedOnPDP.equalsIgnoreCase(model)) {
				htmlReporter.logPass("Name of product being exchanged is displayed correctly!");
			}else {
				htmlReporter.logFailWithoutCapturingScreenshot("Name of product being exchanged is not displayed correctly!");
			}
			
		}else {
			htmlReporter.logFailWithoutCapturingScreenshot("Exchange Offer not displayed for product!");
		}
		htmlReporter.captureScreenshot();
	}
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class)
	public void testMultipleAttributesOfProduct(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;
		htmlReporter.logInfo("Testing for multiple attributes of product- "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		Map<String,String> attributesMapFromTestData = data.getMultipleAttributesMap();
		htmlReporter.logInfo("PDP should display following attributes for product- "+
					gm.getRepresentableStringFromMap(attributesMapFromTestData));
		Map<String,String> attributesMapDisplayedOnPage = pdp.getAttributesMap();
		htmlReporter.logInfo("Actual attributes displayed on product page - "+
					gm.getRepresentableStringFromMap(attributesMapDisplayedOnPage));
		
		if(attributesMapDisplayedOnPage.equals(attributesMapFromTestData)) {
			htmlReporter.logPass("All expected attributes displayed on product page!");
			htmlReporter.captureScreenshot();
		}else {
			htmlReporter.logFailWithoutCapturingScreenshot("Expected and Actual attributes differ!");
			htmlReporter.logFailWithoutCapturingScreenshot("Differnce in attributes: "+"\n"
					+gm.getRepresentableStringFromMap(gm.getDifferenceOfTwoMaps(
							attributesMapFromTestData, attributesMapDisplayedOnPage)));
		}
		
	}
	
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class)
	public void testSimilarProducts(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;
		htmlReporter.logInfo("Testing for similar products!!");
		htmlReporter.logInfo("testing for "+data.getProductNameInURL());
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		if(pdp.isSimilarProductsDisplayed()) {
			htmlReporter.logPass("Similar products displayed for product!");
			List<String> listSimilarProductsLink = pdp.getListOfSimilarProductsLink();
			htmlReporter.logInfo("Links for similar products \n"+gm.getRepresentableStringFromList(listSimilarProductsLink));
			htmlReporter.logInfo("Checking status of each link .... ");
			ResponseCodeProvider rcProvider =  new ResponseCodeProvider();
			Map<String,Integer> responseCodeMap =rcProvider.getResponseCodeForLinks(listSimilarProductsLink);
			htmlReporter.logInfo(gm.getRepresentableStringFromMap(responseCodeMap));
			Map<String,Integer> filteredMap = rcProvider.getFilteredResultsWithExcludingResponseCode(
					responseCodeMap, Arrays.asList(200));
			if(filteredMap.size()==0) {
				htmlReporter.logPass("All links are with response - OK !");
				htmlReporter.captureScreenshot();
			}else {
				htmlReporter.logFailWithoutCapturingScreenshot("All links not with response - OK!");
				htmlReporter.logFailWithoutCapturingScreenshot("Broken links - "+
						gm.getRepresentableStringFromMap(filteredMap));
				
			}
		}else {
			htmlReporter.logFailWithoutCapturingScreenshot("Similar products not displayed for product!");
		}
	}
	
	@Test(dataProvider="PDPTestDataProvider",
			dataProviderClass=com.paytm.dataproviders.PDP_DataProvider.class)
	public void testBadgesAvailability(Object object) {
		PDP_TestDataPacket data = (PDP_TestDataPacket)object;
		System.out.println("testing for "+data.getProductNameInURL());
		htmlReporter.logInfo("Testing for Badges available for product !!");
		DriverManager.getCurrentWebDriver().get(URL_Launcher.getBaseURL()+"/"+data.getProductNameInURL());
		SleepUtils.waitForShortTime();
		List<String> badgesList = data.getBadges();
		htmlReporter.logInfo("Expected Badges for product: "+badgesList.toString());
		htmlReporter.logInfo("Checking for all badges one by one");
		for(String badgeName : badgesList) {
			htmlReporter.logInfo("Checking for badge availability : "+badgeName);
			if(pdp.isBadgeAvailable(badgeName))
				htmlReporter.logPass(badgeName+" badge available for product");
			else
				htmlReporter.logFailWithoutCapturingScreenshot(badgeName+" badge not availabe for product");
		}
		htmlReporter.captureScreenshot();
	}
	
	@Test
	public void dummytest() {
		DriverManager.getCurrentWebDriver().get("https://www.google.com");
		htmlReporter.captureScreenshot();
		htmlReporter.logPass("Just a dummy test case for checking jenkins configurations.");
	}
	
	@AfterTest(alwaysRun=true)
	public void endReport() {
		
	}
}


