package com.paytm.utils;

public class DiscoverabilityMapping {
	public static final int OFFLINE_VAL=2;
	public static final int ONLINE_VAL=1;
	public static final int B2B_VAL=3;
	public static final int STOREFRONT_VAL=4;
}
