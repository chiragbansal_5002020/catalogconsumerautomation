package com.paytm.utils;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;

public class TestMethodListener implements IInvokedMethodListener {

    public TestMethodListener() {
    }

    @Override
    public void beforeInvocation(final IInvokedMethod method, 
            final ITestResult testResult) {
    }

    @Override
    public synchronized void afterInvocation(final IInvokedMethod method, 
            final ITestResult testResult) {
    	if(method.isTestMethod()) {
    		ITestContext tc = Reporter.getCurrentTestResult().getTestContext();;
    		if (testResult.getStatus() == ITestResult.SUCCESS) {
        		System.out.println("Checking for any framework exceptions");
        		FrameworkExceptionStack expStack = FrameworkExceptionStack.map.get(Thread.currentThread().getId());
                if(expStack.getAllExceptionsList().size()!=0) {
                    System.out.println("Found framework exceptions and so marking this test as fail!");
                    Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
                    Reporter.getCurrentTestResult().setThrowable(new Exception("Test Failed"));
                }else {
                	System.out.println("Not found any framework exception");
                } 
            }
    		tc.getSkippedTests().removeResult(Reporter.getCurrentTestResult().getMethod());
    	}
    	
    }
}

