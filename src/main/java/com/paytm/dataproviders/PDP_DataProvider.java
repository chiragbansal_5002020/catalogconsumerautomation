package com.paytm.dataproviders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

import com.paytm.reusable_methods.generic_methods;
import com.paytm.utils.ConfigReader;

public class PDP_DataProvider {
	private static List<PDP_TestDataPacket> dataPacketsList = null;
	
	
	@DataProvider(name="PDPTestDataProvider",
			parallel = false)
	public synchronized static Object[][] getDataProvider(Method testMethod) throws IOException, SecurityException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		
		fillDataPacketListIfEmpty();
		
		Object[][] dataToReturn=null;
		if(testMethod.getName().equalsIgnoreCase("testExchangeOffer")) {
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
					    if(!p.getExchangeOfferAvailable().isEmpty()) return true;
						else return false;
					})
					.collect(Collectors.toList())
					);
		}else if(testMethod.getName().equalsIgnoreCase("testMultipleAttributesOfProduct")){
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
						if(p.getMultipleAttributesMap().size()!=0)return true;
						else return false;
					})
					.collect(Collectors.toList())
					);
		}else if(testMethod.getName().equalsIgnoreCase("testSimilarProducts")) {
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
						return p.isSimilarProductsAvailable();
					})
					.collect(Collectors.toList()));
		}else if(testMethod.getName().equalsIgnoreCase("verifyOffersOnPDP")) {
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
						if(p.getOffersAvailable().size()>0)
						return true;
						else return false;
					})
					.collect(Collectors.toList()));
		}else if(testMethod.getName().equalsIgnoreCase("verifyAdditionalOffersOnPDP")) {
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
						if(p.getAdditionalOffers().size()>0)
						return true;
						else return false;
					})
					.collect(Collectors.toList()));
		}else if(testMethod.getName().equalsIgnoreCase("verifyMoreSellersOnPage")) {
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
						return p.isMoreSellerAvailable();
					})
					.collect(Collectors.toList()));
		}else if(testMethod.getName().equalsIgnoreCase("testSortOrderOfSizes")) {
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
						return p.isSortingOrderAvailable();
					})
					.collect(Collectors.toList()));
		}else if(testMethod.getName().equalsIgnoreCase("testBadgesAvailability")) {
			dataToReturn = convertListTo2DArray(dataPacketsList.stream()
					.filter(p->{
						return p.getBadges().size()>0;
					})
					.collect(Collectors.toList()));
		}
		else {
			dataToReturn  = convertListTo2DArray(dataPacketsList);
		}
		return dataToReturn;
	}
	
	private static Object[][] convertListTo2DArray(List<PDP_TestDataPacket> list){
		Object[][] array2D = new Object[list.size()][1];
		for(int i=0; i<list.size(); i++) {
			array2D[i][0] = list.get(i);
		}
		return array2D;
	}
	
	private static void fillDataPacketListIfEmpty () throws IOException, SecurityException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if(dataPacketsList==null) {
			dataPacketsList = new ArrayList<>();
			String pdpExcelFilePath = ConfigReader.getValue("pdpTestDataFilePath");
			File excelFile = new File(pdpExcelFilePath);
			System.out.println("Searching for PDP Test Data at following path  : "+pdpExcelFilePath);
			if(!excelFile.exists()) {
				System.out.println("Test data not exist for PDP test cases at path : "+
						pdpExcelFilePath+" !Exiting the program ... !!");
				System.exit(0);
			}else {
				Map<String,Map<String,String>> dataSheetsMap = new HashMap<>();
				Workbook workbook=null;
				InputStream iStream = new FileInputStream(excelFile);
				String fileName = pdpExcelFilePath.substring(pdpExcelFilePath.lastIndexOf("/"));
				if(fileName.endsWith(".xlsx")) {
					workbook = new XSSFWorkbook(iStream);
				}else if(fileName.endsWith(".xls")) {
					workbook = new HSSFWorkbook(iStream);
				}else {
					System.out.println("Test Data is not in \".xlsx\" OR \".xls\" format. Exiting the program ");
					System.exit(0);
				}
				Sheet masterSheet = workbook.getSheet("MasterSheet");
				int masterSheetIndex = workbook.getSheetIndex(masterSheet);
				for(int sheetIndex=0; sheetIndex<workbook.getNumberOfSheets(); sheetIndex++) {
					if(sheetIndex!=masterSheetIndex) {
						Sheet currentSheet = workbook.getSheetAt(sheetIndex);

						Map<String,String> indexValueMap = new LinkedHashMap<>();
						Row headerRow = currentSheet.getRow(currentSheet.getFirstRowNum());
						int IndexColumnNumber=Integer.MIN_VALUE;
						int ValueColumnNumber=Integer.MIN_VALUE;
						for(int colNum = headerRow.getFirstCellNum(); colNum<headerRow.getLastCellNum(); colNum++) {
							if(headerRow.getCell(colNum).getStringCellValue().equalsIgnoreCase("Index"))
								IndexColumnNumber = colNum;
							else if(headerRow.getCell(colNum).getStringCellValue().equalsIgnoreCase("Value"))
								ValueColumnNumber=colNum;
						}
						if(IndexColumnNumber==Integer.MIN_VALUE || ValueColumnNumber==Integer.MIN_VALUE) {
							System.out.println("Index column OR Value column not found for sheet- "+currentSheet.getSheetName());
							System.out.println("Skipping current sheet for test data and Proceeding with next sheet ..!");
						}else {
							int totalRows = currentSheet.getLastRowNum()-currentSheet.getFirstRowNum();
							for(int rowNum=currentSheet.getFirstRowNum()+1; rowNum<=totalRows; rowNum++) {
								Row currRow = currentSheet.getRow(rowNum);
								if(!isRowEmpty(currRow))
									indexValueMap.put(currRow.getCell(IndexColumnNumber).getStringCellValue(), 
											currRow.getCell(ValueColumnNumber).getStringCellValue());
							}
							dataSheetsMap.put(currentSheet.getSheetName(), indexValueMap);
						}
					}
				}

				//Printing index-value map
				for(String str : dataSheetsMap.keySet()) {
					System.out.println("=====================================");
					System.out.println("Sheetname - "+str);
					Map<String,String> ivMap = (LinkedHashMap<String,String>)dataSheetsMap.get(str);
					for(String index : ivMap.keySet()) {
						System.out.println(index+"  ->  "+ivMap.get(index));
					}

				}

				//Printing index-value map ends

				Row masterSheetHeaderRow = masterSheet.getRow(masterSheet.getFirstRowNum());
				Map<String,Integer> colNameColNumberMap = new LinkedHashMap<>();
				Field[] fieldsArray = Class.forName("com.paytm.dataproviders.PDP_TestDataPacket").getDeclaredFields();
				List<String> columnNames = new ArrayList<>();
				for(int i=0; i<fieldsArray.length; i++) {
					columnNames.add(fieldsArray[i].getName());
				}
				for(int colNum = masterSheetHeaderRow.getFirstCellNum(); colNum<masterSheetHeaderRow.getLastCellNum(); colNum++) {
					String colName = masterSheetHeaderRow.getCell(colNum).getStringCellValue();
					if(columnNames.contains(colName)) {
						colNameColNumberMap.put(colName, colNum);
						columnNames.remove(colName);
					}
				}
				if(columnNames.size()!=0) {
					System.out.println("Following columns not found in master sheet- "+columnNames);
				}

				//int totalTestDataRowInMasterSheet = masterSheet.getLastRowNum()-masterSheet.getFirstRowNum()-1;
				for(int rowNum=masterSheet.getFirstRowNum()+1; rowNum<=masterSheet.getLastRowNum();rowNum++) {
					Row currRow = masterSheet.getRow(rowNum);
					if(!isRowEmpty(currRow)) {
						PDP_TestDataPacket dataPacket = new PDP_TestDataPacket();
						Field[] fields = dataPacket.getClass().getDeclaredFields();
						List<Method> methods = Arrays.asList(dataPacket.getClass().getDeclaredMethods());
						for(int i=0; i<fields.length; i++) {
							Cell currCell = currRow.getCell(colNameColNumberMap.get(fields[i].getName()));
							if(currCell!=null) {
								for(Method m : methods) {
									if(m.getName().startsWith("set") && m.getName().substring(3).equalsIgnoreCase(fields[i].getName())) {
										Class<?> type = fields[i].getType();
										String cellValue = currRow.getCell(colNameColNumberMap.
												get(fields[i].getName())).getStringCellValue().trim();
										
										if(type==Map.class) {
											Map<String,String> map = new LinkedHashMap<String,String>();
											if(cellValue.contains("#")) {
												cellValue = dataSheetsMap.get(cellValue.split("#")[0].trim())
														.get(cellValue.split("#")[1].trim());
											}
											/*String[] linesSeperatedBynewlineChar = cellValue.split("\n");
											for(String line: linesSeperatedBynewlineChar) {
												if(!line.isEmpty() && line.split("->").length!=2) {
													System.out.println("There is some problem with line- "+line+" . Either Separator -> is not present b/w key and value or empty line");
												}else {
													map.put(line.split("->")[0].trim(), line.split("->")[1].trim());
												}
											}*/
											map = new generic_methods().getMapFromSimpleJsonObject(cellValue);
											m.invoke(dataPacket, map);
										}
										if(type == List.class){
											if(cellValue.contains(",")) {
												List<String> list = new ArrayList<String>();
												String[] vals = cellValue.split(",");
												for(String value : vals) {
													if(value.contains("#")) {
														value = dataSheetsMap.get(value.split("#")[0].trim())
																.get(value.split("#")[1].trim());
													}
													list.add(value);
												}
												m.invoke(dataPacket, list);
											}else {
												if(cellValue.contains("#")) {
													cellValue = dataSheetsMap.get(cellValue.split("#")[0].trim())
															.get(cellValue.split("#")[1].trim());
												}
												m.invoke(dataPacket, Arrays.asList(cellValue));
											}
										}else if(type == boolean.class) {
											if(cellValue.equalsIgnoreCase("Yes") || cellValue.equalsIgnoreCase("True"))
												m.invoke(dataPacket, true);
											else if(cellValue.equalsIgnoreCase("No") || cellValue.equalsIgnoreCase("False"))
												m.invoke(dataPacket, false);
										}else if(type == int.class) {
											if(cellValue.contains("#")) {
												cellValue = dataSheetsMap.get(cellValue.split("#")[0].trim())
														.get(cellValue.split("#")[1].trim());           
											}
											m.invoke(dataPacket, Integer.parseInt(cellValue.trim()));
										}else if(type == double.class) {
											if(cellValue.contains("#")) {
												cellValue = dataSheetsMap.get(cellValue.split("#")[0].trim())
														.get(cellValue.split("#")[1].trim());           
											}
											m.invoke(dataPacket, Double.parseDouble(cellValue.trim()));
										}
										else if(type == String.class) {
											if(cellValue.contains("#")) {
												cellValue = dataSheetsMap.get(cellValue.split("#")[0].trim())
														.get(cellValue.split("#")[1].trim());
											}
											if(!cellValue.isEmpty())
												m.invoke(dataPacket, cellValue);
										}
										break;
									}
								}
							}
							
						}
						dataPacketsList.add(dataPacket);
					}
				}
			}
		}
	}

	private static boolean isRowEmpty(Row row) {
		if(row==null)return true;
		for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK && 
					!cell.getStringCellValue().trim().isEmpty())
				return false;
		}
		return true;
	}

	private static void testSimilarProducts() {
		
	}
public static void main(String[] args) throws IOException, SecurityException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
	fillDataPacketListIfEmpty();
	PDP_DataProvider p = new PDP_DataProvider();
	Method m = p.getClass().getDeclaredMethod("testSimilarProducts");
	Object[][] data = getDataProvider(m);
	for(int i=0; i<data.length; i++) {
		System.out.println(data[i][0]);
	}
}
}
