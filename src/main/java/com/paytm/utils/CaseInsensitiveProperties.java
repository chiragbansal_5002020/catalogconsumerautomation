package com.paytm.utils;

import java.util.Properties;

@SuppressWarnings("serial")
public class CaseInsensitiveProperties extends Properties{
	
	@Override
	 public Object put(Object key, Object value) {
        String lowercase = ((String) key).toLowerCase();
        return super.put(lowercase, value);
    }
  
	@Override
    public String getProperty(String key) {
        String lowercase = key.toLowerCase();
        return super.getProperty(lowercase);
    }
  
	@Override
    public String getProperty(String key, String defaultValue) {                              
        String lowercase = key.toLowerCase();
        return super.getProperty(lowercase, defaultValue);
    }
}
