package com.paytm.utils;

public class GlobalWebDriverSettings {
	public static Browser browserType;
	public static boolean isProxyOn;
	public static boolean isHeadlessModeOn;
	public static OptionsOrProfile optionsOrProfile;
	public static String optionsOrProfileFilePath;
	public static Browser getBrowserType() {
		return browserType;
	}
	public static void setBrowserType(Browser browserType) {
		GlobalWebDriverSettings.browserType = browserType;
	}
	public static boolean isProxyOn() {
		return isProxyOn;
	}
	
	public static boolean isHeadlessModeOn() {
		return isHeadlessModeOn;
	}
	public static void setHeadlessModeOn(boolean isHeadlessModeOn) {
		GlobalWebDriverSettings.isHeadlessModeOn = isHeadlessModeOn;
	}
	public static void setProxyOn(boolean isProxyOn) {
		GlobalWebDriverSettings.isProxyOn = isProxyOn;
	}
	public static OptionsOrProfile getOptionsOrProfile() {
		return optionsOrProfile;
	}
	public static void setOptionsOrProfile(OptionsOrProfile optionsOrProfile) {
		GlobalWebDriverSettings.optionsOrProfile = optionsOrProfile;
	}
	public static String getOptionsOrProfileFilePath() {
		return optionsOrProfileFilePath;
	}
	public static void setOptionsOrProfileFilePath(String optionsOrProfileFilePath) {
		GlobalWebDriverSettings.optionsOrProfileFilePath = optionsOrProfileFilePath;
	}

	
}
