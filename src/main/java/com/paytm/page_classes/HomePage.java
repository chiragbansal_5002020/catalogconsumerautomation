package com.paytm.page_classes;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.paytm.utils.SleepUtils;
import com.paytm.utils.WebdriverUtils;

public class HomePage extends WebdriverUtils{
	
	
	By searchBoxLocator = By.xpath("//input[@type='search']");
	
	public boolean isOnHomePage(WebDriver driver) {
		if(driver.getTitle().equalsIgnoreCase("Online Shopping Site in India | Shop for Mobiles, Electronics, Fashion, Grocery & more @ Paytmmall.com"))
			return true;
		else
			return false;
	}
	
	public boolean isSearchBoxPresentOnUI(WebDriver driver) {
		try {
			getWebElement(searchBoxLocator, true);
			return true;
		}catch(ElementNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void searchForProduct(WebDriver driver, String productName) throws InterruptedException {
		WebElement searchBoxElement = getWebElement(searchBoxLocator, true);
		searchBoxElement.sendKeys(productName);
		searchBoxElement.sendKeys(Keys.ENTER);
		waitForPageToLoad();
		SleepUtils.waitForShortTime();
	}
}
