package com.paytm.page_classes;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.paytm.utils.SleepUtils;
import com.paytm.utils.WebdriverUtils;

public class BasePage extends WebdriverUtils{
	
	
	By locator_shopByCategoryDiv = By.xpath("//span[text()='Shop By Category']/parent::div");
	By locator_subCategoryScrollableDiv = By.xpath("//div[@class='_9k9d']");
	By locator_gridPageProductsHeading = By.xpath("//h1[@class='_1qXr']");
	
	public void loadPaytmMallUrl(WebDriver driver){
		driver.get("https://www.paytmmall.com");
		waitForPageToLoad();
		SleepUtils.waitForShortTime();
	}
	
	public boolean shopByCategory(WebDriver driver, String category,String subCategory, String expectedHeading) {
		System.out.println("Slecting following : "+category+" -> "+subCategory);
		
		getWebElement(locator_shopByCategoryDiv, true).click();
		
		SleepUtils.sleepForMilliseconds(500);
		
		category = category.replaceAll("'", "\'");
		javascriptMouseOverOnElement(getWebElement(By.xpath(""
				+ "//div[@class='Ot6H']//a[translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')=\""+category.toLowerCase()+"\"]"),true));

		WebElement subcategoryElement = getWebElement(By.xpath(""
				+ "//div[@class='_3SFl']//a[translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')=\""+subCategory.toLowerCase()+"\"]")
				,true);

		try {
			subcategoryElement.click();
		}catch(Exception ex) {
			WebElement subCategoryScrollableDiv = getWebElement(locator_subCategoryScrollableDiv,true);
			executeJavascriptQuery("arguments[0].scrollTop = arguments[0].scrollHeight;", subCategoryScrollableDiv);
			subcategoryElement.click();
		}

		waitForPageToLoad();
		System.out.println("Expected Heading on page : "+expectedHeading);
		WebElement headingOnPage = getWebElement(By.xpath(""
				+ "//div[@class='_1gX7']//h1[translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='"+expectedHeading.toLowerCase()+"']"),true);
		
		System.out.println("Actual Heading on page : "+getWebElement(locator_gridPageProductsHeading,true).getText());
		if(headingOnPage!=null) {
			
			return true;
		}
		else
			return false;
	}
}
