package com.paytm.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.paytm.network_sniffer.ProxyManager;
import com.paytm.network_sniffer.ProxyWebDriverManager;

public class DriverManager {

	private  Browser browser;
	private  WebDriver driver;
	private static Map<Long,WebDriver> driverMap = new HashMap<>();

	public DriverManager() {
		String browserType;
		browserType = ConfigReader.getValue("browser");
		browserType = browserType.toLowerCase();

		switch(browserType) {
		case "chrome":
			browser = Browser.CHROME;
			break;
		case "ie":
			browser = Browser.IE;
			break;
		case "firefox":
			browser = Browser.FIREFOX;
		default:
			browser = Browser.CHROME;
			break;
		}
	}

	public static WebDriver getWebDriver(Long threadId) {
		return driverMap.get(threadId);
	}

	public static WebDriver getCurrentWebDriver() {
		return driverMap.get(Thread.currentThread().getId());
	}

	public  WebDriver getCurrentDriver() {
		return driver;
	}

	public  void killWebDriver() {
		if(driver!=null) {
			driver.quit();
		}
	}

	public void setWebDriverFromGlobalWebDriverSettings() {
		if(GlobalWebDriverSettings.getOptionsOrProfile()==null)
			new DriverManager().setWebDriver(GlobalWebDriverSettings.getBrowserType(),
					GlobalWebDriverSettings.isProxyOn(), GlobalWebDriverSettings.isHeadlessModeOn());
		else if(!GlobalWebDriverSettings.getOptionsOrProfileFilePath().isEmpty())
			new DriverManager().setWebDriver(GlobalWebDriverSettings.getBrowserType(),
					GlobalWebDriverSettings.getOptionsOrProfile(), GlobalWebDriverSettings.getOptionsOrProfileFilePath());

	}

	public  void setWebDriverDefinedInConfig() {
		if(browser==Browser.CHROME) {
			setChromeDriver(false,false);
		}else if(browser==Browser.FIREFOX) {
			setFirefoxDriver(false,false);
		}else {
			setInternetExplorerDriver();
		}
	}

	public  void setWebDriver(Browser browser,boolean isProxyOn,boolean isHeadless) {
		if(browser==Browser.CHROME) {
			setChromeDriver(isProxyOn,isHeadless);
		}else if(browser==Browser.FIREFOX) {
			setFirefoxDriver(isProxyOn,isHeadless);
		}else if(browser==Browser.IE){
			setInternetExplorerDriver();
		}
	}

	@SuppressWarnings("deprecation")
	public  void setChromeDriverWithChromeOptions(String pathForChromeOptionsFile) {
		File file = new File(pathForChromeOptionsFile);
		if(!file.exists()) {
			System.out.println("Chrome Profile file not found at following location : "+pathForChromeOptionsFile
					+"\n"+"Aborting execution ... !");
			System.exit(0);
		}
		System.setProperty("webdriver.chrome.driver", ConfigReader.getValue("drivers")+"/chromedriver");
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();  
		LoggingPreferences logPrefs = new LoggingPreferences();

		HashMap<String, Object> chromePrefs = getPropertyFileAsMap(file);  
		options.setExperimentalOption("prefs", chromePrefs);  
		cap.setCapability(ChromeOptions.CAPABILITY, options);  

		logPrefs.enable(LogType.BROWSER, Level.SEVERE);
		cap.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		driver = new ChromeDriver(cap);
		driverMap.put(Thread.currentThread().getId(), driver);
	}

	public  void setChromeDriverWithAlreadyCreatedProfile(String pathForChromeProfile) {
		System.setProperty("webdriver.chrome.driver", ConfigReader.getValue("drivers")+"/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--user-data-dir="+pathForChromeProfile);
		options.addArguments("--start-maximized");
		try {
			driver = new ExtendedChromeDriver(options);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Extended chrome driver didn't work!\n"+"Proceeding with normal Chrome Driver");
		}
		driverMap.put(Thread.currentThread().getId(), driver);
	}

	public  void setFirefoxDriverWithAlreadyCreatedProfile(String pathForFirefoxProfile) {
		System.setProperty("webdriver.gecko.driver", ConfigReader.getValue("drivers")+"/geckodriver");
		FirefoxProfile ffProfile = new FirefoxProfile(
				new File(pathForFirefoxProfile));
		FirefoxOptions ffOptions = new FirefoxOptions();
		ffOptions.setProfile(ffProfile);
		driver = new FirefoxDriver(ffOptions);
		driverMap.put(Thread.currentThread().getId(), driver);
	}

	public  void setFirefoxDriverWithFirefoxOptions(String pathForFirefoxOptionsFile) {
		System.setProperty("webdriver.gecko.driver", ConfigReader.getValue("drivers")+"/geckodriver");
		FirefoxOptions ffOptions = new FirefoxOptions();
		Map<String,Object> optionsMap = getPropertyFileAsMap(new File(pathForFirefoxOptionsFile));
		for(String key : optionsMap.keySet()) {
			ffOptions.addPreference(key, (String) optionsMap.get(key));
		}
		driver = new FirefoxDriver(ffOptions);
		driverMap.put(Thread.currentThread().getId(), driver);
	}

	public  void setInternetExplorerDriver() {
		System.setProperty("webdriver.ie.driver", ConfigReader.getValue("drivers")+"InternetExplorer");
		driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		driverMap.put(Thread.currentThread().getId(), driver);
	}

	public  void setChromeDriver(boolean isProxyOn, boolean isHeadless) {
		System.setProperty("webdriver.chrome.driver", ConfigReader.getValue("drivers")+"/chromedriver");
		ChromeOptions options = new ChromeOptions();
		//options.addArguments("--start-maximized");
		if(isProxyOn)
			options.setCapability(CapabilityType.PROXY, ProxyManager.getInstance().getSeleniumProxy());
		if(isHeadless) {
			options.addArguments("--headless");
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-gpu");
		}

		try {
			driver = new ExtendedChromeDriver(options);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Extended chrome driver didn't work !\n"+"Proceeding with normal chrome driver");
			driver = new ChromeDriver(options);
		}

		if(isProxyOn)
			ProxyWebDriverManager.setWebDriver(driver);

		driverMap.put(Thread.currentThread().getId(), driver);
		driver.manage().window().maximize();

	}

	public  void setFirefoxDriver(boolean isProxyOn, boolean isHeadless) {
		System.setProperty("webdriver.gecko.driver", ConfigReader.getValue("drivers")+"/geckodriver");
		FirefoxOptions options = new FirefoxOptions();
		if(isHeadless)
			options.addArguments("--headless");
		
		if(isProxyOn) {
			options.setCapability(CapabilityType.PROXY, ProxyManager.getInstance().getSeleniumProxy());
			driver = new FirefoxDriver(options);
			ProxyWebDriverManager.setWebDriver(driver);
		}else {
			driver = new FirefoxDriver(options);
		}
		
		driver.manage().window().maximize();
		driverMap.put(Thread.currentThread().getId(), driver);
	}


	public  void setWebDriver(Browser browser, OptionsOrProfile oORp, String filePath) {
		if(browser==Browser.CHROME) {
			if(oORp == OptionsOrProfile.OPTIONS) {
				setChromeDriverWithChromeOptions(filePath);
			}else if(oORp == OptionsOrProfile.PROFILE) {
				setChromeDriverWithAlreadyCreatedProfile(filePath);
			}
		}else if(browser == Browser.FIREFOX) {
			if(oORp == OptionsOrProfile.OPTIONS) {
				setFirefoxDriverWithFirefoxOptions(filePath);
			}else {
				setFirefoxDriverWithAlreadyCreatedProfile(filePath);
			}
		}
	}

	private  HashMap<String,Object> getPropertyFileAsMap(File propertyFile){
		Properties property = new Properties();
		HashMap<String,Object> map = new HashMap<String,Object>();
		try {
			property.load(new FileReader(propertyFile));
			Set<Object> keys = property.keySet();
			for(Object key : keys) {
				map.put(key.toString(), property.getProperty((String) key));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public void restartBrowser() {
		if(DriverManager.getCurrentWebDriver()!=null) {
			DriverManager.getCurrentWebDriver().quit();
		}
		setWebDriverFromGlobalWebDriverSettings();
	}

}
