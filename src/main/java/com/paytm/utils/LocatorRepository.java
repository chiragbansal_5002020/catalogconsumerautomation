package com.paytm.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LocatorRepository {
	
	static LocatorRepository lrObject = null;
	public LocatorRepository() {
		
	}
	
	public static LocatorRepository getInstance() {
		if(lrObject==null) {
			lrObject = new LocatorRepository();
		}
		return lrObject;		
	}
	
	private  static String locatorXmlPath = ConfigReader.getValue("locators");
	private static HashMap<String,Locator> locatorsMap;
	static {
		File locatorsFile = new File(locatorXmlPath);
		if(!locatorsFile.exists()) {
			System.out.println("Locators file not found at path : "+locatorXmlPath+"\n"
					+"Aborting execution");
		}
		
		locatorsMap = new HashMap<String,Locator>();
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document xmlDoc = docBuilder.parse(locatorsFile);
			xmlDoc.normalizeDocument();
			
			NodeList locatorsNode = xmlDoc.getElementsByTagName("locators").item(0).getChildNodes();
			for(int i=0; i<locatorsNode.getLength(); i++) {
				Node childNode = locatorsNode.item(i);
				if(childNode.getNodeType()==Element.ELEMENT_NODE) {
					Element childElement = (Element)childNode;
					//locatorsMap.put(childElement.getAttribute("name"), 
					//		new Locator(childElement.getAttributeNode("type")));
					String locatorName = childElement.getAttribute("name");
					String locatorType = childElement.getElementsByTagName("type").item(0).getTextContent();
					String locatorValue = childElement.getElementsByTagName("value").item(0).getTextContent();
					LocatorType type = null;
					switch(locatorType) {
					case "xpath":
						type = LocatorType.XPATH;
						break;
					case "cssSelector":
						type = LocatorType.CSS_SELECTOR;
						break;
					case "id":
						type = LocatorType.ID;
						break;
					case "name":
						type = LocatorType.NAME;
						break;
					case "tagName":
						type = LocatorType.TAG_NAME;
						break;
					case "linkText":
						type = LocatorType.LINKTEXT;
						break;
					case "partialLinkText":
						type = LocatorType.PARTIAL_LINKTEXT;
						break;
					case "className":
						type = LocatorType.CLASS_NAME;
						break;
					default :
						type = null;
					}
					locatorsMap.put(locatorName, new Locator(type,locatorValue));
				}
			}
					
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public By getLocator(String locatorName) {
		Locator loc = locatorsMap.get(locatorName);
		if(loc==null) {
			System.out.println("Locator ("+locatorName+") not found !");
		}
		if(loc.getValue().trim().isEmpty()) {
			System.out.println("Locator ("+locatorName+") is empty ! ");
		}
		By locatorBy = null;
		
		switch(loc.getType()) {
		case CLASS_NAME:
			locatorBy = By.className(loc.getValue());
			break;
		case CSS_SELECTOR:
			locatorBy = By.cssSelector(loc.getValue());
			break;
		case ID:
			locatorBy = By.id(loc.getValue());
			break;
		case LINKTEXT:
			locatorBy = By.linkText(loc.getValue());
			break;
		case NAME:
			locatorBy = By.name(loc.getValue());
			break;
		case PARTIAL_LINKTEXT:
			locatorBy = By.partialLinkText(loc.getValue());
			break;
		case TAG_NAME:
			locatorBy = By.tagName(loc.getValue());
			break;
		case XPATH:
			locatorBy = By.xpath(loc.getValue());
			break;
		default:
			locatorBy = null;
			break;
		}
		return locatorBy;
	}
	
	public static void main(String[] args) {
		System.out.println(new LocatorRepository().getLocator("HomePage.loginButton"));
		System.out.println(new LocatorRepository().getLocator("LoginForm.username"));
	}
}
