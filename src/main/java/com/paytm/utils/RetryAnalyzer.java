package com.paytm.utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;


public class RetryAnalyzer implements IRetryAnalyzer {

	int counter = 0;
	int retryLimit = Integer.parseInt(ConfigReader.getValue("failedTestCaseRetryCount"));
	
	@Override
	public boolean retry(ITestResult result) {

		if(counter < retryLimit)
		{	
			System.out.println("Re-executing test case ..... And removing skipped test from testng report! ");
			counter++;
			return true;
		}
		return false;
	}
}