package com.paytm.reusable_methods;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.paytm.utils.FrameworkExceptionStack;

public class ResponseCodeProvider {
	
	
	public int getResponseCodeForRequest(String uri) throws IOException  {
		URL url = new URL(uri);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(10000);
		connection.setReadTimeout(10000);
		connection.connect();
		return connection.getResponseCode();
	}
	
	public Map<String,Integer> getLinksWithResponseCodeIncluding(WebDriver driver, int responseCode) throws IOException{
		return (Map<String, Integer>) getResponseCodeForAllLinksOnPage(driver).entrySet().stream()
				.filter(x -> {
					if(x.getValue().intValue()==responseCode)return true;
					else return false;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	
	public Map<String,Integer> getLinksWithResponseCodeIncluding(WebDriver driver, List<Integer> responseCodeList) throws IOException{
		return (Map<String, Integer>) getResponseCodeForAllLinksOnPage(driver).entrySet().stream()
				.filter(x -> {
					if(responseCodeList.contains(x.getValue().intValue()))return true;
					else return false;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	
	public Map<String,Integer> getLinksWithResponseCodeExcluding(WebDriver driver, int responseCode) throws IOException{
		return (Map<String, Integer>) getResponseCodeForAllLinksOnPage(driver).entrySet().stream()
				.filter(x -> {
					if(x.getValue().intValue()!=responseCode)return true;
					else return false;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	
	public Map<String,Integer> getLinksWithResponseCodeExcluding(WebDriver driver, List<Integer> responseCodeList) throws IOException{
		return (Map<String, Integer>) getResponseCodeForAllLinksOnPage(driver).entrySet().stream()
				.filter(x -> {
					if(responseCodeList.contains(x.getValue().intValue()))return false;
					else return true;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	public Map<String,Integer> getFilteredResultsWithExcludingResponseCode(Map<String,Integer> dataToFilter, List<Integer> responseCodeList){
		return (Map<String, Integer>) dataToFilter.entrySet().stream()
				.filter(x -> {
					if(responseCodeList.contains(x.getValue().intValue()))return false;
					else return true;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	public Map<String,Integer> getResponseCodeForProductsLinkOnGrid(WebDriver driver) throws IOException{
		List<WebElement> allProductsLinksList = driver.findElements(By.xpath("//div[@class='_3RA-']//a"));
		return getResponseCodeForAnchorTags(allProductsLinksList);
	}
	
	
	public Map<String,Integer> getResponseCodeForAllLinksOnPage(WebDriver driver) throws IOException{
		List<WebElement> allLinksOnPageList = driver.findElements(By.xpath("//a"));
		return getResponseCodeForAnchorTags(allLinksOnPageList);
	}
	
	public Map<String,Integer> getResponseCodeForLinks(List<String> urlList){
		return getResponseCodeForUrlList(urlList);
	}
	
	private Map<String,Integer> getResponseCodeForAnchorTags (List<WebElement> anchorTagsList) {
		List<String> urlList = new ArrayList<String>();
		for(WebElement currentLink : anchorTagsList)
			urlList.add(currentLink.getAttribute("href").trim());
		return getResponseCodeForUrlList(urlList);
	}
	
	private Map<String,Integer> getResponseCodeForUrlList(List<String> urlList){
		int responseCode = Integer.MIN_VALUE;
		Map<String,Integer> responseCodesMap = new LinkedHashMap<String,Integer>();
		for(String uri : urlList) {
			System.out.print("Link : "+uri);
			if(isValidUrl(uri)) {
				try {
					responseCode = getResponseCodeForRequest(uri);
					System.out.print(" ---> "+responseCode+"\n");
				}catch(Exception e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
					FrameworkExceptionStack.getCurrentStack().add(e);
					responseCode = Integer.MIN_VALUE;
				}
				responseCodesMap.put(uri,responseCode);
			}else {
				System.out.print("  -- Invalid !\n");
			}
		}
		return responseCodesMap;
	}
	
	private boolean isValidUrl(String url) {
		boolean result=false;
		try {
			URL uri = new URL(url);
			result =  true;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			result = false;
		}
		return result;
	}
	
	private List<String> getValidUrlList(List<String> allUrlsList){
		List<String> validUrlList = new ArrayList<String>();
		for(String url : allUrlsList) {
			try {
				if(isValidUrl(url))
					validUrlList.add(url);
			}catch(Exception e) {}
		}
		return validUrlList;
	}
}
