package com.paytm.test;

import java.util.TimeZone;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.paytm.utils.Browser;
import com.paytm.utils.DriverManager;
import com.paytm.utils.GlobalWebDriverSettings;
import com.paytm.utils.OptionsOrProfile;
import com.paytm.utils.Reporter;

@Listeners({com.paytm.utils.FrameworkTestListener.class,com.paytm.utils.TestMethodListener.class})
public class Setup {

	@BeforeSuite(alwaysRun=true)
	@Parameters({"isBrowserProxyOn","isHeadlessModeOn","browser","isProfileOrOptions","pathForDriverProfileOrOptionsFile"})
	public void setup(String isBrowserProxyOn, String isHeadlessModeOn,String browser,@Optional("") String isProfileOrOptions,
			@Optional("") String pathForDriverProfileOrOptionsFile) {
		
		/*Adding a shutdown hook to flush the extents reports (so that report could be printed 
		 * in case of abnormal termination 
		*/
		 
		System.out.println("Adding shutdown hook to system.");
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
		        public void run() {
		        	Reporter.finishAllReports() ;
		        }
		  }));
		
		 
		/* Saving browser configuration in a class so that it could be used later to instantiate
		 * new web-driver instance 
		 */
		 
		boolean isProxyOn = false;
		boolean isHeadlessOn=false;
		Browser typeOfBrowser = null;
		OptionsOrProfile typeOfFileProvided = null;
		
		if(isBrowserProxyOn.equalsIgnoreCase("YES") || isBrowserProxyOn.equalsIgnoreCase("TRUE"))
			isProxyOn = true;
		else if(isBrowserProxyOn.equalsIgnoreCase("NO") || isBrowserProxyOn.equalsIgnoreCase("FALSE"))
			isProxyOn = false;
		
		if(isHeadlessModeOn.equalsIgnoreCase("YES") || isHeadlessModeOn.equalsIgnoreCase("TRUE"))
			isHeadlessOn=true;
		
		if(browser.equalsIgnoreCase("CHROME")) {
			typeOfBrowser = Browser.CHROME;
		}else if(browser.equalsIgnoreCase("FIREFOX")) {
			typeOfBrowser = Browser.FIREFOX;
		}else if(browser.equalsIgnoreCase("IE") || browser.equalsIgnoreCase("Internet Explorer")
				||browser.equalsIgnoreCase("InternetExplorer")) {
			typeOfBrowser = Browser.IE;
		}
		
		if(!isProfileOrOptions.isEmpty()) {
			if(isProfileOrOptions.equalsIgnoreCase("Options") || isProfileOrOptions.equalsIgnoreCase("Option")) {
				typeOfFileProvided = OptionsOrProfile.OPTIONS;
			}else if(isProfileOrOptions.equalsIgnoreCase("Profile")) {
				typeOfFileProvided = OptionsOrProfile.PROFILE;
			}
		}else {
			typeOfFileProvided=null;
		}
		
		/*if(typeOfFileProvided==null)
			new DriverManager().setWebDriver(typeOfBrowser,isProxyOn);
		else if(!pathForDriverProfileOrOptionsFile.isEmpty())
			new DriverManager().setWebDriver(typeOfBrowser, typeOfFileProvided, pathForDriverProfileOrOptionsFile);*/
		
		GlobalWebDriverSettings.setBrowserType(typeOfBrowser);
		GlobalWebDriverSettings.setOptionsOrProfile(typeOfFileProvided);
		GlobalWebDriverSettings.setProxyOn(isProxyOn);
		GlobalWebDriverSettings.setHeadlessModeOn(isHeadlessOn);
		GlobalWebDriverSettings.setOptionsOrProfileFilePath(pathForDriverProfileOrOptionsFile);
		
	}
	
	@BeforeMethod
	public void initialize() {
		
	}
	
	@AfterSuite(alwaysRun=true)
	public void finish() {
		//DriverManager.killWebDriver();
		/*System.out.println("Finishing all reports .. !");*/
		//Reporter.finishAllReports();
	}
}
