package com.paytm.network_sniffer;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.paytm.reusable_methods.generic_methods;

public class Tester {
	
	public static void main(String[] args) throws Exception {
		

		generic_methods gm = new generic_methods();
		//NetworkRequestFilter filter = new NetworkRequestFilter("catalog.paytm");
		ProxyWebDriverManager.setWebDriver(ProxyWebDriverManager.firefox);
		WebDriver driver = ProxyWebDriverManager.getCurrentWebDriver();
		
		ActionTracker tracker = new ActionTracker("catalog.paytm");
		driver.get("https://paytmmall.com/Best-Selling-Refrigerators-online-Appliances-llpid-176193");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()='Enter Pincode']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@placeholder='Enter Pincode']")).sendKeys("201301");
		Thread.sleep(1000);
		TrackerResponse pincodeTracker = 
				tracker.trackClickOnTheElement(
						driver.findElement(By.xpath("//span[text()='Check']")),5);
		
		printProductList(pincodeTracker.getProductsFromGridLayoutJSON("catalog.paytm"));
		
		TrackerResponse acr1 = 
		tracker.trackUrlLaunch("https://paytmmall.com/edible-oils-glpid-101420",10);
		
		//System.out.println(gm.getRepresentableStringFromMap(acr1.getAllRequestResponseMap()));
		//System.out.println(gm.getRepresentableStringFromMap(acr1.getRemoteIPAddressForFilteredRequest("catalog.paytm")));
		
		printProductList(acr1.getProductsFromGridLayoutJSON("catalog.paytm"));
		
		/*TrackerResponse acr2 = 
		tracker.trackClickOnTheElement(
				driver.findElement(By.xpath("//span[contains(text(),'Kyocera')]/preceding-sibling::span")),
				ActionTracker.WEB_DRIVER_CLICK);
		
		System.out.println(acr2.getFilteredRequestResponseMap());
		System.out.println(acr2.isAllResponseOk());
		
		TrackerResponse acr3 = 
		tracker.trackClickOnTheElement(
				driver.findElement(By.xpath("//span[contains(text(),'Laser')]/preceding-sibling::span")),
				ActionTracker.WEB_DRIVER_CLICK);
		
		System.out.println(acr3.getFilteredRequestResponseMap("facebook"));*/

		
		
		//ProxyWebDriverManager.close();
	}
	
	public static void printProductList(List<Product> list) {
		for(Product product: list) {
			System.out.println(product);
		}
	}
}
