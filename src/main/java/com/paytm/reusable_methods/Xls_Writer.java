package com.paytm.reusable_methods;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.paytm.utils.Timer;

public class Xls_Writer {

	private void writeDataToExcelWorkbook(String filePath, String sheetName, List<String> headers,Object[][] data, boolean isReplaceExistingFile) {
		File excelFile = null;
		Workbook workbook = null;
		Sheet worksheet = null;
		FileOutputStream oStream = null;

		if(!isReplaceExistingFile) {
			excelFile = getNewFile(filePath);
		}else {
			excelFile = new File(filePath);
		}

		if(filePath.endsWith(".xls")) {
			workbook =  new HSSFWorkbook();
		}else {
			workbook = new XSSFWorkbook();
		}

		try {
			worksheet = workbook.createSheet(sheetName);
			if(headers!=null) {
				Row headerRow = worksheet.createRow(0);
				int headerColIndex=0;
				for(String header : headers) {
					headerRow.createCell(headerColIndex++).setCellValue(header);
				}
			}

			Row row = null;
			int rowIndex=(headers==null?0:1),colIndex=0;
			for(int i=0 ; i<data.length; i++) {
				row = worksheet.createRow(rowIndex++);
				colIndex = 0;
				for(int j=0; j<data[i].length; j++) {
					if(data[i][j] instanceof String)
						row.createCell(colIndex++).setCellValue(data[i][j].toString());
					else if(data[i][j] instanceof Integer)
						row.createCell(colIndex++).setCellValue((Integer)data[i][j]);
					else if(data[i][j] instanceof Double)
						row.createCell(colIndex++).setCellValue((Double)data[i][j]);
					else if(data[i][j] instanceof Date)
						row.createCell(colIndex++).setCellValue((Date)data[i][j]);
					else if(data[i][j] instanceof Boolean)
						row.createCell(colIndex++).setCellValue((Boolean)data[i][j]);
				}
			}

		} catch (Exception e) {
			System.out.println("Some error occured while writing to file at path : "+filePath);
			e.printStackTrace();
		}finally {
			if(workbook!=null) {
				try {
					oStream = new FileOutputStream(excelFile);
				} catch (FileNotFoundException e) {
					System.out.println("Error occured while getting output stream for file : "+filePath);
					e.printStackTrace();
				}
				try {
					workbook.write(oStream);
					oStream.close();
				} catch (IOException e) {
					System.out.println("Error occured while writing to output stream / "
							+ "closing output stream for excel file");
					e.printStackTrace();
				}

			}

		}

	}
	
	public void dumpResponseCodeMapInExcel(String filePath, Map<String,Integer> responseCodeMap, boolean isReplaceExisting) {
		
		Object[][] data = new Object[responseCodeMap.keySet().size()][2];
		int rowIndex=0;
		for(String link : responseCodeMap.keySet()) {
			data[rowIndex][0] = link;
			data[rowIndex][1] = responseCodeMap.get(link);
			rowIndex++;
		}
		
		writeDataToExcelWorkbook(filePath, "Sheet1",
				new ArrayList<String>(Arrays.asList("URL","Response Code")), 
				data, isReplaceExisting);
	
	}

	private File getNewFile(String filePath) {
		File file = new File(filePath);
		if(file.exists() && !file.isDirectory()) {
			String extension = filePath.substring(filePath.lastIndexOf(".")+1);
			filePath = filePath.substring(0, filePath.lastIndexOf("."+extension));
			filePath+="_"+Timer.getCurrentTimeStamp()+"."+extension;
			file = new File(filePath);
		}
		return file;
	}
}
