package com.paytm.reusable_methods;

import java.util.HashMap;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class generateSSOToken 
{
	public String generateToken()
	{
		String accessToken="";
		
		String authorizeURL="https://persona-staging.paytm.com/oauth2/authorize";
		String tokenURL="https://persona-staging.paytm.com/oauth2/token";
		
		RequestSpecification requestspec=RestAssured.given();
		HashMap<String,String> requestauthorizeParams=new HashMap<String,String>();
		HashMap<String,String> requestTokenParams=new HashMap<String,String>();
		Response  response;
		ResponseBody message;
		JsonPath jsonPathEvaluate;
		int code;
		
		//Authorize API Parameters
		requestspec.header("Content-Type","application/json");
		
		requestauthorizeParams.put("response_type", "code");
		requestauthorizeParams.put("client_id" , "seller-web-login");
		requestauthorizeParams.put("notredirect","true");
		requestauthorizeParams.put("username","rashi.agarwal@paytmmall.com");
		requestauthorizeParams.put("password","paytm@123");
		
		requestspec.body(requestauthorizeParams);
		
		response=requestspec.post(authorizeURL);
		code=response.getStatusCode();
		System.out.println("Code:::"+code);
		
		message =response.getBody();
		System.out.println(message.asString());
		
		jsonPathEvaluate=response.jsonPath();
		String generatedCode="";
		generatedCode=jsonPathEvaluate.get("code").toString();
		System.out.println("Generated Code:::"+generatedCode);
		
		Assert.assertEquals(200,code);
		
		//Token API Parameters
		requestTokenParams.put("client_id","seller-web-login");
		requestTokenParams.put("client_secret","ba74c6b5e9d354cad71c8f18ee30e5fd5879af2d");
		requestTokenParams.put("state","");
		requestTokenParams.put("grant_type","authorization_code");
		requestTokenParams.put("code",generatedCode);
		
		requestspec.body(requestTokenParams);
		
		response=requestspec.post(tokenURL);
		code=response.getStatusCode();
		System.out.println("Code:::"+code);
		
		message =response.getBody();
		System.out.println(message.asString());
		
		jsonPathEvaluate=response.jsonPath();
		accessToken=jsonPathEvaluate.get("access_token").toString();
		System.out.println("Access Token:::"+accessToken);
		
		return accessToken;
		
	}

}
