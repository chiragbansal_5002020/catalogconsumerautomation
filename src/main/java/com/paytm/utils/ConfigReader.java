package com.paytm.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ConfigReader {
	private static final String settingsFilePath = System.getProperty("user.dir")+"/src/main/resources/settings.properties";
	private static CaseInsensitiveProperties settings;
	static {
		File settingsFile = new File(settingsFilePath);
		if(!settingsFile.exists()) {
			System.out.println("settings.properties file not found at location : "+settingsFilePath+"\n"+
					"Aborting Execution ... !");
			System.exit(0);
		}
		settings = new CaseInsensitiveProperties();
		try {
			settings.load(new FileInputStream(settingsFile));
			String environmentSettingsFilePath = getValue("envSettingFilePath");
			settingsFile = new File(environmentSettingsFilePath);
			if(!settingsFile.exists()) {
				System.out.println("Environment settings file not found at location : "+environmentSettingsFilePath+"\n"+
							"Aborting execution ... !");
				System.exit(0);
			}
			settings = new CaseInsensitiveProperties();
			settings.load(new FileInputStream(settingsFile));
		} catch (FileNotFoundException e) {
			System.out.println("settings.properties file not found at location - "+settingsFilePath+"\n"+
					"Aboring Execution ... !");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Some error occured while reading from settings.properties file !"+"\n"+
					"Aboring Execution ... !");
			e.printStackTrace();
			
		}
	}
	
	public static String getValue(String key) {
		String value =  settings.getProperty(key);
		if(value==null) {
			System.out.println("Value not found for the setting - "+key+"\n"+
					"It may impact the execution, so aborting .. !");
			System.exit(0);
		}
		
		return value;
	}
	
}
