package com.paytm.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Timer {
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy_hh-mm-ss_a");
	public static String getCurrentTimeStamp(){
		return sdf.format(new Date());
	}
}
