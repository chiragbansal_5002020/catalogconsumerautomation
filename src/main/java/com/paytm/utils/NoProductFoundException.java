package com.paytm.utils;

@SuppressWarnings("serial")
public class NoProductFoundException extends Exception{
	String URL;
	
	public NoProductFoundException() {
		super("No product found at the url : "+DriverManager.getWebDriver(Thread.currentThread().getId()).getCurrentUrl());
		URL = DriverManager.getWebDriver(Thread.currentThread().getId()).getCurrentUrl();
	}
	
	@Override
	public String toString() {
		return "No product found at url "+URL; 
	}
}
