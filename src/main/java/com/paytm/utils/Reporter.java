package com.paytm.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.paytm.reusable_methods.generic_methods;

enum TestStatus{
	IN_PROGRESS, COMPLETED
}

public class Reporter {

	private  ExtentReports extent;
	private  String report;
	private  String reportDir;
	
	public static Map<Long, ExtentTest> testsMap = new HashMap<>();
	public static Map<Class,Reporter> reportsMap = new HashMap<>();
	private static List<String> reportsInProgress = new ArrayList<>();
	private Map<String,TestStatus> tcStatusMap = new HashMap<String,TestStatus>();
	
	public Reporter() {
		
	}
	
	public Reporter getInstance(Class className, String reportName) {
		if(reportsMap.get(className)==null) {
			reportsMap.put(className, this);
			createReport(reportName);
		}
		return reportsMap.get(className);	
	}
	
	public String getReportDir() {
			return reportDir;
	}
	
	public static ExtentTest getExtentTest(Long threadId) {
		return testsMap.get(threadId);
	}
	
	public  ExtentReports getCurrentExtentReportObject() {
		return extent;
	}

	public  ExtentTest getCurrentTest() {
		return getExtentTest(Thread.currentThread().getId());
	}
	
	public void addScreenshotInTest(String screenshotFilePath) {
		try {
			getExtentTest(Thread.currentThread().getId()).info("Screenshot", MediaEntityBuilder.createScreenCaptureFromPath(screenshotFilePath).build());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public  void logFail(String message) {
		System.out.println(message);
		getExtentTest(Thread.currentThread().getId()).log(Status.FAIL, "<pre><font color='red'>"+message+"</font></pre>");
		addScreenshotInTest(generic_methods.capture_screenshot(
				DriverManager.getCurrentWebDriver(),getReportDir(),"FAILURE"));
		
		FrameworkExceptionStack.map.get(Thread.currentThread().getId()).add(new Exception(message));
	}
	
	public  void logFailWithoutCapturingScreenshot(String message) {
		System.out.println(message);
		getExtentTest(Thread.currentThread().getId()).log(Status.FAIL, "<pre><font color='red'>"+message+"</font></pre>");
		FrameworkExceptionStack.map.get(Thread.currentThread().getId()).add(new Exception(message));
	}
	
	public  void logPass(String message) {
		System.out.println(message);
		getExtentTest(Thread.currentThread().getId()).log(Status.PASS, "<pre><font color='green'>"+ message +"</font></pre>");
	}
	
	public  void logInfo(String message) {
		System.out.println(message);
		getExtentTest(Thread.currentThread().getId()).log(Status.INFO,"<pre>" +message+ "</pre>");
	}
	
	public  void captureScreenshot() {
		addScreenshotInTest(generic_methods.capture_screenshot(
				DriverManager.getCurrentWebDriver(),getReportDir(),"Screenshot - "));
	}
	
	public synchronized void createReport(String reportName) {
			System.out.println("Trying to create report with name : "+reportName);
			if(!reportsInProgress.contains(reportName)) {
				reportsInProgress.add(reportName);
				if(!reportName.contains(".")) {
					reportName = reportName+".html";
				}else {
					String reportExtension = reportName.substring(reportName.lastIndexOf(".")+1);
					if(!(reportExtension.equalsIgnoreCase("html"))) {
						System.out.println("Report name is with extension other than html"+"\n"+
								"Saving the report with .html extension");
						reportName = reportName.substring(0,reportName.lastIndexOf("."))+".html";
					}
				}
				
				System.out.println("Starting a new report with name : "+reportName);
				report = reportName;
				reportDir = ConfigReader.getValue("htmlReportsDirectory")+"/Report_"+reportName.replaceAll(".html","")+"_"+Timer.getCurrentTimeStamp();
				extent = new ExtentReports();
				ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportDir+"/"+reportName);
				htmlReporter.loadConfig(ConfigReader.getValue("extentConfigPath"));
				htmlReporter.config().setCSS("textarea{height: 50rem;} "
						+ "nav .brand-logo{padding-left: 0px}"
						+ "nav .brand-logo > img{width: 100%; margin-left: 0px}");
				htmlReporter.config().setJS("document.getElementsByTagName('img')[0].src='logo.jpg';");
				extent.attachReporter(htmlReporter);
				extent.setSystemInfo("Host Name", "Consumer");
				extent.setSystemInfo("Environment", "Automation Testing");
				extent.setSystemInfo("User Name", "Chirag Bansal");
			//	extent.setSystemInfo("Browser Name",((RemoteWebDriver)DriverManager.getWebDriver(Thread.currentThread().getId())).getCapabilities().getBrowserName());
			//	extent.setSystemInfo("Browser Version",((RemoteWebDriver)DriverManager.getWebDriver(Thread.currentThread().getId())).getCapabilities().getVersion());
				
				String shouldCreateLogFile = ConfigReader.getValue("shouldCreateLogFile");
				if(shouldCreateLogFile.equalsIgnoreCase("YES") || shouldCreateLogFile.equalsIgnoreCase("TRUE")){
					new Logger_ConsoleToFile().startLoggerOnFile(reportDir+"/"+"log.txt");
				}
			}else {
				System.out.println("Can't create new report with name : "+reportName+" "
						+ "because report with same name is already in progress. ");
				
			}
	}

	public synchronized void createTest(String testcaseName) {
		
		if(!tcStatusMap.containsKey(testcaseName) || isTestCompleted(testcaseName)) {
			System.out.println("Creating test in report with name : "+testcaseName);
			System.out.println("Extent is : "+extent);
			ExtentTest test = extent.createTest(testcaseName);
			testsMap.put(Thread.currentThread().getId(), test);
			
			// Marking test case status as in progress
			tcStatusMap.put(testcaseName, TestStatus.IN_PROGRESS);
			System.out.println("Test created in extents report with name : "+testcaseName);
		}else {
				System.out.println("Can't create new test with name : "+testcaseName
						+" because test case with same name is already in progress");
		}
	}

	public synchronized void finishCurrentTest() {
		ExtentTest currentTest = getExtentTest(Thread.currentThread().getId());
		if(currentTest!=null) {
			System.out.println("Finishing test case in report - "+ currentTest.getModel().getName());
			tcStatusMap.put(currentTest.getModel().getName(),TestStatus.COMPLETED);
			System.out.println("Current test case marked completed and finished in report");
			
		}
	}

	public  void finishCurrentTestReport() {
		System.out.println("Report finisher started on report name : "+this.report);
		System.out.println("Cheking if there is any test case in progress .. !");
		if(!isAllTestCasesCompleted()) {
			System.out.println("Tests are in progress and so can't finish the reporter yet!!");
		}else {
			System.out.println("Finishing test case report : "+report);
			/*if(getExtentTest(Thread.currentThread().getId())!=null)
				finishCurrentTest();*/
			extent.flush();
			copyLogoToDisplayInReport(getReportDir());
		}
		
	}
	
	public  synchronized void RemoveCurrentTestFromReport() {
		String testcaseName = getExtentTest(Thread.currentThread().getId()).getModel().getName();
		System.out.println("Removing testcase from report - "+testcaseName);
		if(getExtentTest(Thread.currentThread().getId())!=null) {
			extent.removeTest(getExtentTest(Thread.currentThread().getId()));
			//tcStatusMap.put(testcaseName,TestStatus.COMPLETED);
			tcStatusMap.remove(testcaseName);
		}
	}
	
	private void copyLogoToDisplayInReport(String directory) {
		try {
			FileUtils.copyFile(new File("./src/main/resources/logo/logo.jpg"), 
					new File(directory+"/"+"logo.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean isAllTestCasesCompleted() {
		for(String tcName : tcStatusMap.keySet()) {
			if(tcStatusMap.get(tcName)==TestStatus.IN_PROGRESS) {
				System.out.println("Test Case : "+tcName+" is in progress.");
				return false;
			}
		}
		return true;
	}
	
	private boolean isTestCompleted(String testcaseName) {
		for(String tcName: tcStatusMap.keySet()) {
			if(tcStatusMap.get(tcName) == TestStatus.COMPLETED)
				return true;
			else 
				return false;
		}
		return true;
	}
	
	public static void finishAllReports() {
		for(Class className : reportsMap.keySet()) {
			System.out.println("Finishing report on : "+className.getName());
			Reporter reporter = reportsMap.get(className);
			reporter.extent.flush();
			reporter.copyLogoToDisplayInReport(reporter.reportDir);
		}
	}
}
