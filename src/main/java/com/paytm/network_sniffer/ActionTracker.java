package com.paytm.network_sniffer;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.lightbody.bmp.core.har.HarEntry;

enum ClickType{
	WEB_DRIVER_CLICK, JAVASCRIPT_CLICK
}
public class ActionTracker {
	public static final ClickType WEB_DRIVER_CLICK = ClickType.WEB_DRIVER_CLICK;
	public static final ClickType JAVASCRIPT_CLICK = ClickType.JAVASCRIPT_CLICK;
	private String defaultFilterValue;
	
	public ActionTracker(String defaultFilterValue) {
		this.defaultFilterValue = defaultFilterValue;
	}
	
	public TrackerResponse trackClickOnTheElement(WebElement element, int... waitInSeconds) throws Exception {
		initializeTrackerOnAction();
		new WebDriverWait(ProxyWebDriverManager.getCurrentWebDriver(), 10)
		.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
		finalizeTrackerOnAction(waitInSeconds);  
		return new TrackerResponse(defaultFilterValue,  
				ProxyManager.getInstance().getProxyServer().getHar().getLog());
	}
	
	public TrackerResponse trackClickOnTheElement(WebElement element, ClickType clickType, int... waitInSeconds) throws Exception {
		initializeTrackerOnAction();
		new WebDriverWait(ProxyWebDriverManager.getCurrentWebDriver(), 10)
		.until(ExpectedConditions.elementToBeClickable(element));
		
		if(clickType == WEB_DRIVER_CLICK)
			element.click();
		else if(clickType == JAVASCRIPT_CLICK) {
			((JavascriptExecutor)ProxyWebDriverManager.getCurrentWebDriver())
			.executeScript("arguments[0].click();",element);
		}
		finalizeTrackerOnAction(waitInSeconds);
		return new TrackerResponse(defaultFilterValue, 
				 ProxyManager.getInstance().getProxyServer().getHar().getLog());
	}
	
	public TrackerResponse trackEnterKeyOnTheElement(WebElement element,int... waitInSeconds) throws Exception {
		initializeTrackerOnAction();
		element.sendKeys(Keys.ENTER);
		finalizeTrackerOnAction(waitInSeconds);
		return new TrackerResponse(defaultFilterValue, 
				 ProxyManager.getInstance().getProxyServer().getHar().getLog());
	}
	
	public TrackerResponse trackUrlLaunch(String url,int... waitInSeconds) throws Exception {
		initializeTrackerOnAction();
		ProxyWebDriverManager.getCurrentWebDriver().get(url);
		finalizeTrackerOnAction(waitInSeconds);
		return new TrackerResponse(defaultFilterValue,
				 ProxyManager.getInstance().getProxyServer().getHar().getLog());
	}
	
	private static void initializeTrackerOnAction() throws Exception {
		ProxyManager.getInstance().getProxyServer().newHar();
	}
	
	private static void finalizeTrackerOnAction(int... waitInSeconds) throws Exception {
		if(waitInSeconds.length>0)
			Thread.sleep(waitInSeconds[0]*1000);
		waitUntilAllResponseArrived();
	}
	
	@SuppressWarnings("deprecation")
	private static void waitUntilAllResponseArrived() {
		new WebDriverWait(ProxyWebDriverManager.getCurrentWebDriver(),20)
		.pollingEvery(2,TimeUnit.SECONDS)
		.until(new ExpectedCondition<Boolean>() {

			@Override
			public Boolean apply(WebDriver input) {
				Boolean result=true;
				List<HarEntry> entries = ProxyManager.getInstance().getProxyServer().getHar().getLog().getEntries();
				for(HarEntry entry : entries)
					if(entry.getResponse().getStatus()==0) {
						result =  false;
						break;
					}
				return result;
			}
			}
		);
	}
}
