package com.paytm.utils;

public class SleepUtils {
	private static int shortTimeInMillis=1000;
	private static int midTimeInMillis=2000;
	private static int longTimeInMillis=3000;
	
	static {
		String[] times = ConfigReader.getValue("sleepTimer").split(",");
		boolean isConfigEnteryInCorrectFormat = true;
		if(times.length!=3) {
			isConfigEnteryInCorrectFormat=false;
		}else {
			try {
				shortTimeInMillis = Integer.parseInt(times[0]);
				midTimeInMillis = Integer.parseInt(times[1]);
				longTimeInMillis = Integer.parseInt(times[2]);
			}catch(Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				isConfigEnteryInCorrectFormat=false;
			}
		}
		
		if(!isConfigEnteryInCorrectFormat) {
			System.out.println("Sleep time not provided in config in the format : ShortWait,MidWait,LongWait");
			System.out.println("For eg. sleepTimer=2000,3000,5000");
			System.out.println("Setting default sleep time for now. 1000,2000,3000");
		}
	}
	
	public static void sleepForMilliseconds(int timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
	public static void waitForShortTime(){
		sleepForMilliseconds(shortTimeInMillis);
	}
	
	public static void waitForMidTime() {
		sleepForMilliseconds(midTimeInMillis);
	}
	
	public static void waitForLongTime() {
		sleepForMilliseconds(longTimeInMillis);
	}
}
