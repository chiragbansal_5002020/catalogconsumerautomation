package com.paytm.utils;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Tester {
	
	static WebDriver driver;
	public static void main(String... args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver");
		DesiredCapabilities caps = DesiredCapabilities.chrome();
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.DRIVER, Level.ALL);
		//logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		
		 driver = new ChromeDriver(caps);
		driver.get("https://www.paytmmall.com/printers-glpid-28996");
		 
		Thread.sleep(10000);
		
		//displayLogs(LogType.PERFORMANCE);
		//displayLogs(LogType.BROWSER);
		//Notworking -- displayLogs(LogType.CLIENT);
		displayLogs(LogType.DRIVER);
		//displayLogs(LogType.PROFILER);
		//displayLogs(LogType.SERVER);
		
	
		System.out.println("************* Closing the Browser ********************");
		driver.close();
		 
		driver.quit();
	}
	
	
	public static void displayLogs(String logType) {
		System.out.println("\n\n\n========================== Displaying logs "+logType+"  ===================");
		List<LogEntry> entries = driver.manage().logs().get(logType).getAll();
		System.out.println(entries.size() + " " + logType + " log entries found");
		for (LogEntry entry : entries) {
		    System.out.println(entry.getMessage());
			//System.out.println(
		     //       new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
		}
	}
}
