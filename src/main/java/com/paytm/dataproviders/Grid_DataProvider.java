package com.paytm.dataproviders;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.testng.annotations.DataProvider;

import com.paytm.utils.ConfigReader;

public class Grid_DataProvider {
	@DataProvider(name="GridTestDataProvider",
			parallel = false)
	public static Object[][] getDataProvider(Method method){
		Object[][] data=null;
		File testDataFile = new File(ConfigReader.getValue("gridTestDataFilePath"));
		if(!testDataFile.exists()) {
			return null;
		}
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(testDataFile));
		} catch (FileNotFoundException e) {
			System.out.println("Grid Test Data file not found at path : "+testDataFile.getAbsolutePath());
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			System.out.println("Some problem occured while reading grid test data file at path : "+testDataFile.getAbsolutePath());
			e.printStackTrace();
			return null;
		}

		List<String> testDataIds = new ArrayList<String>();
		String ids = null;
		String idsArray[] = null;
		
		if(method.getName().equalsIgnoreCase("addToCartTest")) {
			ids = props.getProperty("addToCartTest");		
		}else if(method.getName().equalsIgnoreCase("discoverabilityTest")) {
			ids = props.getProperty("discoverabilityTest");
		}else if(method.getName().equalsIgnoreCase("serviceabilityTest")) {
			ids = props.getProperty("serviceabilityTest");
			idsArray = ids!=null?ids.trim().split(","):new String[0];
		}else {
			String glpids = props.getProperty("glpid")!=null?props.getProperty("glpid"):"";
			String llpids = props.getProperty("llpid")!=null?props.getProperty("llpid"):"";
			ids = glpids+","+llpids;
		}
		
		idsArray = ids!=null?ids.trim().split(","):new String[0];
		for(String id: idsArray) {
			if(!id.trim().isEmpty())
				testDataIds.add(id.trim());
		}

		data = new Object[testDataIds.size()][1];
		int counter=0;
		for(String id : testDataIds) {
			data[counter++][0] = id;
		}



		return data;
	}
	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, ClassNotFoundException {
		Method m = Class.forName("com.paytm.dataproviders.Grid_DataProvider").getDeclaredMethod("serviceabilityTest");
		Object[][] data = getDataProvider(m);
		for(int i=0; i<data.length; i++)
			System.out.println(data[i][0]);
	}
}
