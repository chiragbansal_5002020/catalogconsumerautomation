package com.paytm.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebdriverUtils {
	
	
	public static void waitForElement(By by) {
		new WebDriverWait(DriverManager.getCurrentWebDriver(), 30).until(ExpectedConditions.visibilityOfElementLocated(by));
	}
	
	public static void waitForElementAbsence(By by) {
		new WebDriverWait(DriverManager.getCurrentWebDriver(),30).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				try {
			          if (driver.findElement(by).isDisplayed())
			          	return false;
			          else
			        	  return true;
			        } catch (NoSuchElementException e) {
			          return true;
			        } catch (StaleElementReferenceException e) {
			          return true;
			        }
			}
			
		});
	}
	
	public static boolean isElementPresentOnPage(By by, boolean shouldWaitForWebElement) {
		System.out.println("Checking for element on page : "+by);
		WebDriver driver = DriverManager.getCurrentWebDriver();
		if(shouldWaitForWebElement) {
			try {
				new WebDriverWait(DriverManager.getCurrentWebDriver(), 5).until(ExpectedConditions.presenceOfElementLocated(by));
			}catch(Exception e) {System.out.println("Waited for 5 seconds and element not present on UI.");}
		}
		if(DriverManager.getCurrentWebDriver().findElements(by).size()>0) return true;
		else {
			while(driver.findElements(By.xpath("//iframe")).size()>0) {
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
				if(driver.findElements(by).size()>0) return true;
			}
			driver.switchTo().defaultContent();
			return false;
		}
	}
	
	public static boolean isElementPresentOnPage(By by) {
		return isElementPresentOnPage(by,false);
	}
	
	public static void waitForPageToLoad() {
		WebDriver driver = DriverManager.getCurrentWebDriver();
		JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
		new WebDriverWait(driver,40).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return jsExecutor.executeScript("return document.readyState").toString().equals("complete");
			}
		});
	}
	
	public static WebElement getWebElement(By by, boolean shouldWaitUntilPresent) {
		WebDriver driver = DriverManager.getCurrentWebDriver();
		WebElement element = null;
		if(shouldWaitUntilPresent)
			waitForElement(by);
		element = driver.findElement(by);
		System.out.println("Web Element is : "+element);
		return element;
	}
	
	public static List<WebElement> getWebElements(By by) {
		return DriverManager.getCurrentWebDriver().findElements(by);
	}
	
	public static String getWebPageSource() {
		return DriverManager.getCurrentWebDriver().getPageSource();
	}
	public static void moveToElementAndClick(By by) {
		Actions action = new Actions(DriverManager.getCurrentWebDriver());
		action
		.moveToElement(getWebElement(by, true))
		.click().build().perform();
		waitForPageToLoad();
	}
	
	public static void waitForElementToBeClickable(By locator) {
		System.out.println("waiting upto 30 seconds for element to be clickable- "+locator);
		new WebDriverWait(DriverManager.getCurrentWebDriver(),30).until(ExpectedConditions.elementToBeClickable(locator));
	} 
	
	public static void waitForElementToBeClickable(WebElement element) {
		System.out.println("waiting upto 30 seconds for element to be clickable- "+element);
		new WebDriverWait(DriverManager.getCurrentWebDriver(),30).until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public static void javascriptClickOnElement(By by) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor)DriverManager.getCurrentWebDriver();
		waitForElementToBeClickable(by);
		jsExecutor.executeScript("arguments[0].click();", getWebElement(by, true));
	}
	
	public static void javascriptClickOnElement(WebElement element) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor)DriverManager.getCurrentWebDriver();
		waitForElementToBeClickable(element);
		jsExecutor.executeScript("arguments[0].click();",element);
	}
	
	public static void mouseOverOnElement(By by) {
		WebElement element = getWebElement(by, true);
		Actions action = new Actions(DriverManager.getCurrentWebDriver());
		action.moveToElement(element).perform();
	}
	
	protected void javascriptMouseOverOnElement(WebElement element) {
	    String query = "var fireOnThis = arguments[0];"
	                + "var evObj = document.createEvent('MouseEvents');"
	                + "evObj.initEvent( 'mouseover', true, true );"
	                + "fireOnThis.dispatchEvent(evObj);";
	    ((JavascriptExecutor) DriverManager.getCurrentWebDriver()).executeScript(query, element);
	}
	
	public static String getInnerTextWithJavaScript(WebElement element) {
		return (String)((JavascriptExecutor)DriverManager.getCurrentWebDriver()).executeScript("return arguments[0].innerText;", element);
	}
	
	public static Object executeJavascriptQuery(String query, Object... args) {
		JavascriptExecutor jsExecutor = ((JavascriptExecutor)DriverManager.getCurrentWebDriver());
		return jsExecutor.executeScript(query,args);
	}
	
	public static void selectItemFromSelectBox(By locator_selectBox, String item) {
		Select selectBox = new Select(getWebElement(locator_selectBox, true));
		waitForElementToBeClickable(locator_selectBox);
		selectBox.selectByVisibleText(item);
	}
	
	public static void scrollToElement(By by, boolean shouldWaitForElement) {
		executeJavascriptQuery("arguments[0].scrollIntoView(true);", getWebElement(by, shouldWaitForElement));	
	}
	
	public static void scrollToElement(WebElement element) {
		executeJavascriptQuery("arguments[0].scrollIntoView(true);", element);
	}
	
	public static void scrollUptoElement(WebElement element) {
		executeJavascriptQuery("arguments[0].scrollIntoView(false);", element);
	}
	
}
