package com.paytm.reusable_methods;

import java.text.SimpleDateFormat;
import java.util.Date;

public class common_constants
{
	private static String classPathtemp = "";
	static
	{
		classPathtemp = System.getProperty("user.dir") + "/src";
	}
	public static String browser = "chrome";
	public static String siteURL = "https://seller-dev.paytm.com"; 
	
	//login credentials
	public static String seller_username="rashi.agarwal@paytmmall.com";
	public static String seller_password="paytm@123";
	
	public static final String classPath = classPathtemp;
	public static String dataFolderName = "Test Data";
	public static String serverName = "staging";
	
	static Date date = new Date();
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy_hh-mm-ss_a");
	public static String currentTimeStamp = sdf.format(date);
	
}
