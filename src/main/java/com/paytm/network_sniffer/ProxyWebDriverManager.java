package com.paytm.network_sniffer;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;

import com.paytm.utils.ConfigReader;

enum BrowserType{
	CHROME,IE,FIREFOX
}

public class ProxyWebDriverManager {
	public static Map<Long,WebDriver> driversMap = new HashMap<>();
	public static final BrowserType chrome = BrowserType.CHROME;
	public static final BrowserType ie = BrowserType.IE;
	public static final BrowserType firefox = BrowserType.FIREFOX;
	
	public static void setWebDriver(WebDriver d) {
		driversMap.put(Thread.currentThread().getId(), d);
	}
	
	public static void setWebDriver(BrowserType type) {
			if(type==BrowserType.CHROME)
				initializeChromeDriver();
			else if(type == BrowserType.FIREFOX)
				initializeFirefoxDriver();
			else if(type == BrowserType.IE)
				initializeInternetExplorerDriver();
		
	}
	
	public static WebDriver getCurrentWebDriver() {
		return driversMap.get(Thread.currentThread().getId());
	}
	
	private static void initializeChromeDriver() {
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", ConfigReader.getValue("drivers")+"/chromedriver");
		ChromeOptions options = new ChromeOptions();
	    options.setCapability(CapabilityType.PROXY, ProxyManager.getInstance().getSeleniumProxy());
	    driver = new ChromeDriver(options);
	    driver.manage().window().maximize();
	    driversMap.put(Thread.currentThread().getId(), driver);
	}
	
	private static void initializeFirefoxDriver() {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", ConfigReader.getValue("drivers")+"/geckodriver");
		FirefoxOptions options = new FirefoxOptions();
		options.setCapability(CapabilityType.PROXY, ProxyManager.getInstance().getSeleniumProxy());
		driver = new FirefoxDriver(options);
		driver.manage().window().maximize();
		driversMap.put(Thread.currentThread().getId(), driver);

	}
	
	private static void initializeInternetExplorerDriver() {
		
	}
	
	public static void close() {
		WebDriver driver = getCurrentWebDriver();
		if(driver!=null) {
			if(ProxyManager.getInstance().getProxyServer()!=null)
				ProxyManager.getInstance().getProxyServer().abort();
		}
			driver.quit();
	}
	
}
