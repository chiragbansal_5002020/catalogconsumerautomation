package com.paytm.network_sniffer;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarLog;
import net.lightbody.bmp.core.har.HarRequest;

public class TrackerResponse {
	private Map<String,Integer> requestResponseMap;
	private String defaultFilterValue;
	private Map<String,String> requestUrlToRemoteAddrMap;
	private HarLog harLog;

	public TrackerResponse(String defaultFilterValue,HarLog harLog) {
		this.harLog = harLog;
		this.defaultFilterValue = defaultFilterValue;
		requestResponseMap = new LinkedHashMap<String,Integer>();
		requestUrlToRemoteAddrMap = new LinkedHashMap<String,String>();
		for(HarEntry entry : harLog.getEntries()) {
			requestResponseMap.put(entry.getRequest().getUrl(), entry.getResponse().getStatus());
			requestUrlToRemoteAddrMap.put(entry.getRequest().getUrl(), 
					entry.getServerIPAddress());
		}
	}

	private String getJSONResponseFromURL(String url) throws MalformedURLException, IOException {
		URLConnection con = new URL(url).openConnection();
		if(con.getContentType().contains("json")) {
			InputStream in = con.getInputStream();
			String encoding = con.getContentEncoding();
			encoding = encoding == null ? "UTF-8" : encoding;
			String body = IOUtils.toString(in, encoding);
			return body;
		}else
			return null;
	}

	private List<HarEntry> getFilteredHarEntry(String filterValue) {
		return harLog.getEntries().stream()
				.filter(e->{
					if(e.getRequest().getUrl().contains(filterValue)) return true;
					else return false;
				}).collect(Collectors.toList());
	}

	public int getAddToCartValueForCategory(String filterValueForApiRequest) throws MalformedURLException, IOException {
		List<HarEntry> entries = getFilteredHarEntry(filterValueForApiRequest);
		for(HarEntry entry : entries) {
			HarRequest request = entry.getRequest();
			System.out.println("request url - "+request.getUrl());
			String jsonResponse = getJSONResponseFromURL(request.getUrl());
			if(jsonResponse == null) {
				System.out.println("Requested url didn't provide json response.");
				return Integer.MIN_VALUE;
			}else {
				if(jsonResponse.startsWith("[") && jsonResponse.endsWith("]"))
					jsonResponse = jsonResponse.substring(1,jsonResponse.length()-1);
				JSONObject obj = new JSONObject(jsonResponse);
				if(obj.has("add_to_cart")) {
					return obj.getInt("add_to_cart");
				}else {
					return Integer.MIN_VALUE;
				}

			}
		}
		return Integer.MIN_VALUE;
	}

	public List<Product> getProductsFromGridLayoutJSON(String filterValurForApiRequest) throws MalformedURLException, IOException{
		List<Product> productLayoutData = new ArrayList<>();
		List<HarEntry> entries = getFilteredHarEntry(filterValurForApiRequest);
		for(HarEntry entry : entries) {
			HarRequest request = entry.getRequest();
			System.out.println("request url - "+request.getUrl());
			String jsonResponse = getJSONResponseFromURL(request.getUrl());
			if(jsonResponse == null) {
				System.out.println("Requested url didn't provide json response.");
				return null;
			}else {
				if(jsonResponse.startsWith("[") && jsonResponse.endsWith("]"))
					jsonResponse = jsonResponse.substring(1,jsonResponse.length()-1);
				JSONObject obj = new JSONObject(jsonResponse);
				if(obj.has("grid_layout")) {
					JSONArray gridLayoutArray = obj.getJSONArray("grid_layout");
					JSONObject jsonProductObject = null;
					for (int i = 0; i < gridLayoutArray.length(); i++)
					{
						jsonProductObject = gridLayoutArray.getJSONObject(i);
						productLayoutData.add(getProductFromJsonProudctObject(jsonProductObject, 
								Arrays.asList(
										Product.class.getDeclaredFields()).stream()
								.map(Field::getName)
								.collect(Collectors.toList())));
					}
					break;
				}	
			}	
		}
		return productLayoutData;
	}

	private Product getProductFromJsonProudctObject(JSONObject jsonProductObject, List<String> requiredKeys) {
		Product product = new Product();
		for(String currentKey : requiredKeys) {
			Field field=null;
			try {
				field = product.getClass().getDeclaredField(currentKey);
			} catch (NoSuchFieldException | SecurityException e1) {
				e1.printStackTrace();
			}

			if(jsonProductObject.has(currentKey)) {
				String data = jsonProductObject.get(currentKey).toString();
				try {

					if(field.getType()==java.lang.String.class) {
						field.set(product, data);
					}else if(field.getType()==int.class) {
						field.set(product, Integer.parseInt(data));
					}else if(field.getType()==double.class) {
						field.set(product,Double.parseDouble(data));
					}

				}catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}

			}else {
				System.out.println("Key not found in JSON : "+currentKey);
				if(currentKey.equalsIgnoreCase("serviceability_enabled")) {
					System.out.println("Trying to extract serviceablility from product url ...");
					if(jsonProductObject.has("newurl")) {
						String data = jsonProductObject.getString("newurl");
						//System.out.println("svc="+data.charAt(data.indexOf("svc=")+4));
						try {
							field.set(product,Integer.parseInt(""+data.charAt(data.indexOf("svc=")+4)));
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}

					}
				}
			}
		}
		return product;
	}

	public Map<String,Integer> getAllRequestResponseMap(){
		return requestResponseMap;
	}

	public Map<String,Integer> getFilteredRequestResponseMap (String filterByContainsString){
		return requestResponseMap
				.entrySet().parallelStream()
				.filter(x->{
					return x.getKey().contains(filterByContainsString)?true:false;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	public Map<String,Integer> getFilteredRequestResponseMap(){
		return requestResponseMap
				.entrySet().parallelStream()
				.filter(x->{
					return x.getKey().contains(defaultFilterValue)?true:false;
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	public boolean isAllResponseOk() {
		return true;
	}

	public boolean isAllResponseOkWithFilteredRequest(String filterValue) {
		return requestResponseMap.entrySet().stream()
				.filter(e-> {
					if(e.getKey().contains(filterValue)) return true;
					else return false;})
				.anyMatch(e->e.getValue()!=200?false:true);
	}

	public boolean isAllResponseOkWithFilteredRequests() {
		return requestResponseMap.entrySet().stream()
				.filter(e-> {
					if(e.getKey().contains(defaultFilterValue)) return true;
					else return false;})
				.anyMatch(e->e.getValue()!=200?false:true);
	}

	public Map<String,String> getRemoteIPAddressForFilteredRequest(String filterValue){
		return requestUrlToRemoteAddrMap.entrySet().stream()
				.filter(e->{
					if(e.getKey().contains(filterValue)) return true;
					else return false;
				}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

}
