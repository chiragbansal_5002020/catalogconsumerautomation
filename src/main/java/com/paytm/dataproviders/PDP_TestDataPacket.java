package com.paytm.dataproviders;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/*
 * 	An object will be created for every row in PDP Test Data Excel MasterSheet.
 * 	There must be a column with same name as the instance variable name
 * 	eg. ProductNameInURL is defined as String and with same name a column must be in mastersheet
 * 	Generate Getters and Setters methods for every instance variable.
 * 
 */
public class PDP_TestDataPacket {
	private String ProductNameInURL;
	private String ProductType;
	boolean MoreSellerAvailable;
	private boolean SimilarProductsAvailable;
	private boolean SortingOrderAvailable;
	private List<String> AdditionalOffers;
	private List<String> OffersAvailable;
	private String ExchangeOfferAvailable;
	private String ReturnPolicy;
	private Map<String,String> MultipleAttributesMap;
	private List<String> Badges;
	
	public PDP_TestDataPacket() {
		this.ProductNameInURL="";
		this.ProductType="";
		this.AdditionalOffers = new ArrayList<>();
		this.OffersAvailable = new ArrayList<>();
		this.ExchangeOfferAvailable="";
		this.ReturnPolicy="";
		this.MultipleAttributesMap=new LinkedHashMap<>();
		this.Badges = new ArrayList<>();
	}
	public String getProductNameInURL() {
		return ProductNameInURL;
	}
	public void setProductNameInURL(String productNameInURL) {
		ProductNameInURL = productNameInURL;
	}
	public String getProductType() {
		return ProductType;
	}
	public void setProductType(String productType) {
		ProductType = productType;
	}
	public boolean isMoreSellerAvailable() {
		return MoreSellerAvailable;
	}
	public void setMoreSellerAvailable(boolean moreSellerAvailable) {
		MoreSellerAvailable = moreSellerAvailable;
	}
	public boolean isSimilarProductsAvailable() {
		return SimilarProductsAvailable;
	}
	public void setSimilarProductsAvailable(boolean similarProductsAvailable) {
		SimilarProductsAvailable = similarProductsAvailable;
	}
	public boolean isSortingOrderAvailable() {
		return SortingOrderAvailable;
	}
	public void setSortingOrderAvailable(boolean sortingOrderAvailable) {
		SortingOrderAvailable = sortingOrderAvailable;
	}
	public List<String> getAdditionalOffers() {
		return AdditionalOffers;
	}
	public void setAdditionalOffers(List<String> additionalOffers) {
		AdditionalOffers = additionalOffers;
	}
	public List<String> getOffersAvailable() {
		return OffersAvailable;
	}
	public void setOffersAvailable(List<String> offersAvailable) {
		OffersAvailable = offersAvailable;
	}
	
	public String getExchangeOfferAvailable() {
		return ExchangeOfferAvailable;
	}
	public void setExchangeOfferAvailable(String exchangeOfferAvailable) {
		ExchangeOfferAvailable = exchangeOfferAvailable;
	}
	public String getReturnPolicy() {
		return ReturnPolicy;
	}
	public void setReturnPolicy(String returnPolicy) {
		ReturnPolicy = returnPolicy;
	}
	
	public Map<String, String> getMultipleAttributesMap() {
		return MultipleAttributesMap;
	}
	public void setMultipleAttributesMap(Map<String, String> multipleAttributesMap) {
		MultipleAttributesMap = multipleAttributesMap;
	}
	
	
	public List<String> getBadges() {
		return Badges;
	}
	public void setBadges(List<String> badges) {
		Badges = badges;
	}
	
	@Override
	public String toString() {
		String result = "========================================\n"+"Object PDP_TestDataPacket contains \n";
		Field[] fields = this.getClass().getDeclaredFields();
		Method[] methods = this.getClass().getDeclaredMethods();
		for(int i=0; i<fields.length; i++) {
			try {
				for(int j=0; j<methods.length;j++) {
					if((methods[j].getName().startsWith("get") || methods[j].getName().startsWith("is") || 
							methods[j].getName().startsWith("has")) &&
							(methods[j].getName().substring(3).equalsIgnoreCase(fields[i].getName()) || 
									methods[j].getName().substring(2).equalsIgnoreCase(fields[i].getName()))) {
						result+=fields[i].getName()+" -> "+ methods[j].invoke(this).toString()+"\n";
						break;
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return result+"\n========================================";
	}
	
	
	public static void main(String[] args) {
		PDP_TestDataPacket packet = new PDP_TestDataPacket();
		packet.setProductNameInURL("abc");
		System.out.println(packet);
	}
}
